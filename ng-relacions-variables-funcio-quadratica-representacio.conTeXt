\startsection[reference=seccio:funcio-quadratica-representacio, title={Representació de funcions quadràtiques}]

\startsubject[title={Preguntes}]

\startsubsection[title={Representació gràfica de funcions quadràtiques}]

\startexercici Representeu gràficament les funcions següents:
\startitemize[a,columns,three,packed]
\item $y = (x-2)^2 +3$
\item $y= -2(x-3)^2 +5$
\item $y=-(x+2)^2 -4$
\item $y= -4x^2-8x+12$
\item $y = -10x^2 - 20x$
\item $y = 2x^2 -4x -1$
\stopitemize
\stopexercici

\startexercici Representeu les funcions següents:
\startitemize[a,columns,packed,three]
\item $y = x^2+2$
\item $y = (x-2)^2$
\item $y = 2x^2-x+2$
\item $y = 3(x-1)^2$
\item $y = -x^2+x-10$
\item $y = (x+3)^2 + 1$
\item $y = 2x^2-8x$
\item $y = (x-1)(x+2) + 3$
\item $y = -2(x-3)^2 -10$
\stopitemize
En cada cas, trobeu el vèrtex de la paràbola.
\stopexercici


\startexercici Representeu:
\startitemize[a,columns,packed]
\item $y=2x^2 +12x +16$
\item $y=x^2 -2x + 1$
\item $y=-x^2 - 2x -4$
\item $y=x^2 -2x$
\item $y=-x^2 +2x -2$
\item $y=-3x^2$
\item $y=2x^2 -8$
\item $y = -(x+10)^2 -10$
\stopitemize
\stopexercici

\stopsubsection

\startsubsection[title={Càlcul del vèrtex, orientació i punts de tall amb els eixos}]

\startexercici Trobeu la curvatura, el vèrtex i els punts de talls amb els eixos d'aquelles funcions que donin lloc a paràboles:
\startitemize[a,packed,columns]
\item $y = 2x^2 +2x -12$
\item $y = -x^2 -3x - 2$
\item $y = 3x^2 +9x$
\item $y = -3x^2 +9$
\item $y = 3x^2 + 9$
\item $y = -x^2 -2x -4$
\item $y = -x^2 -3x -2$
\item $y = -x^2$
\item $y = -x^2 +2$
\item $y = -2x^2 +7$
\item $y = -x^2 +25$
\item $y = -x^2 -25$
\item $y = x^2 -2x +3$
\item $y = (x-2)^2 + 3(x-3)$
\item $y = 3(x-3)^2 -3x^2$
\stopitemize
\stopexercici

\startexercici Trobeu el vèrtex de la paràbola que té com a fórmula $y=-x^2 +4$
\stopexercici

\startexercici Trobeu el vèrtex i els punts de tall amb els eixos de les paràboles:
\startitemize[a, columns, packed]
\item $y = (x+2)^2 +2$
\item $y = (x-2)^2 + 2$
\item $y = 4 (x-2)^2 -3$
\item $y = -2 (x+3)^2 +5$
\item $y = -5 (x-3)^2 -5$
\item $y = -2 (x-1)^2$
\stopitemize
\stopexercici

\startexercici Trobeu el vèrtex i els punts de tall amb els eixos de les paràboles:
\startitemize[a, columns, packed]
\item $y = -(x+2)^2$
\item $y = (x-1)(x-2)$
\item $y = (x-2)^2 -1$
\item $y = (x-1)^2 -1$
\item $y = 2(x-1)(x+3)$
\item $y = 2 (x-1)(x-2) -2$
\stopitemize
\stopexercici

\stopsubsection

\stopsubject


\page[yes]
\startsubject[title={Solucions}]
\stopsubject

\stopsection