\startmode[espa2a]
\section[seccio:relacions-magnituds]{Relacions entre magnituds}

\startexercici Digues quina relació hi ha entre les magnituds següents:
\startmyitemize[packed]
\item kg de farina i número d'ous per fer una coca de xocolata
\item Número de persones i temps que tarden a cavar un pou
\item La velocitat a la que caminem i les hores que tardem en arribar al nostre destí
\item Els doblers que guanyam i el nombre de fills que tenim
\stopmyitemize
\stopexercici


\startexercici[exer:exercici-2-relacio] En cada cas, digues quina relació hi ha entre les dues magnituds:
\startmyitemize[packed]
\item El nombre d'astronautes a una nau espacial i els kg de menjar que mengen
\item El nombre d'obrers a una obra i el temps que tarden a acabar-la
\item El temps que passa i la longitud d'una estalactita
\item Les hores de temps lliure que tenim i les hores que miram la televisió
\item La longitud del costat d'un triangle equilàter i el seu perímetre
\item Hores que tenim encesa la televisió i la despesa d'energia que provoca
\item El número de sabata d'una persona i la seva edat
\item El volum d'una bolla i el temps que tarda per arribar a enterra si la deixem anar
\item El perímetre d'un pentàgon regular i la longitud del seu costat
\stopmyitemize
\stopexercici


\startexercici[exer:exercici-3-relacio] En cada cas, digues quina relació hi ha entre les dues magnituds:
\startmyitemize[packed]
\item La longitud del costat d'un cub i l'aire que conté
\item El perímetre d'un quadrat i el seu costat
\item Els doblers que guanyam i els doblers que gastam en el mes
\item El temps que tardem en anar del punt $A$ al punt $B$ i la distància entre $A$ i $B$
\stopmyitemize
\stopexercici

\startexercici Entre aquestes magnituds, digues quines tenen una relació de proporcionalitat directa, de proporcionalitat inversa o bé no tenen cap relació:

\startmyitemize[packed]
\item Litres d'aigua i quilograms de farina per fer la massa del pa
\item L'alçada d'una persona i la seva edat
\item El tamany de la roda d'un cotxe i la velocitat a la que va
\item El radi d'una circumferència i la seva longitud
\item L'àrea d'un quadrat i la longitud el seu costat
\item L'àrea d'un quadrat i la longitud de la seva diagonal
\item L'àrea d'un quadrat i el seu perímetre
\stopmyitemize
\stopexercici

\startexercici Digues quina relació hi ha entre les magnituds següents (\quotation{relació de proporcionalitat directa}, \quotation{relació de proporcionalitat inversa} o \quotation{no hi ha relació}):
\startmyitemize[packed]
\item El nombre de quilograms comprats de taronges i els euros que ens costen
\item La altura d'un individu i la longitud del seu dit polsa
\item Hores de feina i sou apercebut, en un treballador que treballa per hores
\item Els doblers que demanam al banc i els interessos que tornam
\item El nombre de columnes d'un edifici i el pes que suporta cadascuna
\stopmyitemize
\stopexercici

page[yes]

\starttextrule{Solucions de \about[seccio:relacions-magnituds]}
\startitemize[n,packed]
\item Exercici~\in[exer:exercici-2-relacio]: \startitemize[a,text] \item D, \item I, \item D, \item No té relació, \item D, \item D, \item No té relació, \item No té relació, \item D.\stopitemize
\item Exercici~\in[exer:exercici-3-relacio]: \startitemize[a,text] \item Relació cúbica, \item Directa, \item Hi ha relació però no és directa, \item Directa \stopitemize
\stopitemize
\stoptextrule

\stopmode