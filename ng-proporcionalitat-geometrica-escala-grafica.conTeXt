\startsection[reference=seccio:proporcionalitat-geometrica-escala-grafica, title={Proporcionalitat geomètrica: escala gràfica}]

\startsubject[title={Preguntes}]

\startexercici[reference=exer:d-Londres-NY] Donat aquest plànol (Figura~\in[fig:planol-Lon-NY]), trobeu quina distància hi ha entre Londres i Nova York?

\startplacefigure[location=force, reference=fig:planol-Lon-NY, title={Mapa del món}]
  \bTABLE[frame=off,align={middle,lohi}]
    \bTR
      \bTD
        \goto{\externalfigure[figs/proporcionalitat-geometrica-figs-Lon-NY2.png][scale=800]}[url(https://goo.gl/maps/8eQGB)]
      \eTD
    \eTR
   \eTABLE
\stopplacefigure
\stopexercici

\page[yes]
\startexercici[reference=exer:comparació-Algèria-Aràbia-Saudí] Quin país és més gran: Algèria o Aràbia Saudí? (figura~\in[fig:planol-Arabia-Saudi-Algeria])

\startplacefigure[location=force, reference=fig:planol-Arabia-Saudi-Algeria, title={Aràbia Saudí vs Algèria}]
  \bTABLE[frame=off,align={middle,lohi}]
    \bTR
      \bTD
        \goto{\externalfigure[figs/proporcionalitat-geometrica-figs-Alg-ArSaud.png][scale=820]}[url(https://goo.gl/maps/AlXqb)]
      \eTD
     \eTR
  \eTABLE
\stopplacefigure
\stopexercici

\startexercici[reference=exer:Australia-vs-Mallorca] Quina superfície és major: la de Mallorca o la d'Austràlia? Quant més gran és? (figura~\in[fig:planol-Australia-Mallorca]).

\startplacefigure[location={here}, reference=fig:planol-Australia-Mallorca, title={Austràlia vs Mallorca}]
\bTABLE[frame=off,align={middle,lohi},width=fit, split=yes]
\bTR
  \bTD
\goto{\externalfigure[figs/proporcionalitat-geometrica-figs-Mallorca-OSM.png][scale=640]}[url(http://osm.org/go/xUK7g8)]
  \eTD
\eTR
\bTR
  \bTD
\goto{\externalfigure[figs/proporcionalitat-geometrica-figs-Australia-OSM.png][scale=600]}[url(http://osm.org/go/s277)]
  \eTD
\eTR
\eTABLE
\stopplacefigure
\stopexercici

Potser estaria bé que en aquest punt véssiu l'apunt de teoria sobre l'escala gràfica (secció~\in[seccio:teoria-escala-grafica]).

\startexercici[reference=exer:Son-Bieló-carrers] A partir de la figura següent (figura~\in[fig:plànol-Son-Bieló-OSM]):

\startplacefigure[location=here, reference=fig:plànol-Son-Bieló-OSM, title={Plànol de Son Bieló (entre s'Estanyol de Migjorn i sa Ràpita)}]
\bTABLE[frame=off,align=middle]
%\setupTABLE[column][2,4,6][width=1cm]
 \bTR[align=middle]
   \bTD
    {\goto{\externalfigure[figs/proporcionalitat-geometrica-figs-Son-Bieló-OSM.png][scale=600]}[url(http://osm.org/go/xR1XcBBCg-?layers=H)]}
   \eTD
 \eTR
\eTABLE
\stopplacefigure

\startitemize[a,packed]
\item Trieu dos {\em carrers} del poble que contenguin, com a mínim, a 3 illetes de cases
\item Trobeu la seva {\em longitud} (en metres)
\item Quin és el carrer més gran del poble? Què medeix?
\stopitemize
\stopexercici

\startexercici[reference=exer:recorregut-A-B] Observeu el mapa (figura~\in[fig:planol-Alaro-Binissalem]):

\startplacefigure[location=here, reference=fig:planol-Alaro-Binissalem, title={Plànol de Alaró-Binissalem}]
\bTABLE[frame=off,align=middle]
  \bTR[align=middle]
    \bTD
      {\goto{\externalfigure[figs/proporcionalitat-geometrica-figs-recorregut-Alaró-Binissalem-openroute.png][scale=600]}[url(http://osrm.at/941)]}
    \eTD
  \eTR
\eTABLE
\stopplacefigure

\startitemize[a]
\item Quina distància aproximada hi ha entre Alaró i Binissalem?
\item Què gastaríeu de benzina amb una moto que consumeix 5 litres cada 100 quilòmetres?
\item Què tardaríeu en fer cada destí? Podeu suposar que sempre anau a la mateixa velocitat
\stopitemize
\stopexercici

\startexercici[reference=exer:agència-viatges] Volem anar de viatge. Anam a l'agència de viatges i ens diuen que hi ha una oferta (figura~\in[fig:oferta]):

\startplacefigure[location=here, reference=fig:oferta, title={Oferta}]
\startframedtext
\startalignment[middle]
{\ssb Viatge per quilòmetres}

\blank[big]
Mínim 4000 km

Màxim 8000 km

1 € = 10 km

Obligatori fer el trajecte Madrid-París
\stopalignment
\stopframedtext
\stopplacefigure

\startplacefigure[location=here, reference=fig:planol-europa, title={Plànol d'Europa}]
\bTABLE[frame=off,align=middle]
  \bTR[align=middle]
    \bTD
     {\goto{\externalfigure[figs/proporcionalitat-geometrica-figs-Europa-OSM.png][scale=670]}[url(http://osrm.at/942)]}
    \eTD
   \eTR
\eTABLE
\stopplacefigure

\startitemize[packed]
\item Trieu un viatge dins aquests paràmetres
\item Què vos costarà el viatge?
\stopitemize

Podeu fer servir el plànol d'Europa (figura~\in[fig:planol-europa]).
\stopexercici

\startexercici[reference=exer:lluna, title={Mare Serenitatis}] El {\em mar de la serenitat} (figura~\in[fig:lluna-petita]) és un antic mar de la Lluna \goto{[1]}[url(http://en.wikipedia.org/wiki/Mare_Serenitatis)] (figura~\in[fig:lluna]). Forma part de la {\em cara} de l'\goto{home de la Lluna}[url(http://en.wikipedia.org/wiki/Man_in_the_Moon)]. Podeu calcular quina superfície té?

\startplacefigure[location=here, reference=fig:lluna-petita, title={Plànol de la mar {\sl Mare Serenitatis}}]
\bTABLE[frame=off,align=middle]
 \bTR[align=middle]
   \bTD
     \externalfigure[figs/proporcionalitat-geometrica-figs-Lluna-petita.png][scale=600]
   \eTD
 \eTR
\eTABLE
\stopplacefigure

\startplacefigure[location=here, reference=fig:lluna, title={Plànol de la Lluna}]
\bTABLE[frame=off,align=middle]
 \bTR[align=middle]
  \bTD
    \externalfigure[figs/proporcionalitat-geometrica-figs-Lluna-gran.png][scale=600]
  \eTD
 \eTR
\eTABLE
\stopplacefigure


El continent europeu té una superfície de $10.180.000 \unit{square kilo meter}$. Compareu les superfícies de la {\sl Mare Serenitatis} amb Europa.
\stopexercici

\startexercici[reference=exer:comparació-Islàndia-EUA] Estimeu quantes vegades és més gran Estats Units d'Amèrica (figura~\in[fig:EUA-quadriculada]) respecte d'Islàndia (figura~\in[fig:Islàndia-quadriculada]) a partir d'aquests plànols.

\startplacefigure[location=here, reference=fig:Islàndia-quadriculada, title={Plànol d'Islàndia amb l'espai quadriculat}]
\bTABLE[frame=off,align=middle]
\bTR[align=middle]
\bTD
% adaptat de http://tex.stackexchange.com/questions/9559/drawing-on-an-image-with-tikz i http://www.texample.net/tikz/examples/connecting-text-and-graphics/
{\starttikzpicture
    \node[anchor=south west,inner sep=0] at (0,0) {\externalfigure[figs/proporcionalitat-geometrica-figs-Islàndia-OSM.png]};
    \draw[color=orange!40,step=1cm,very thin] (0,0) grid (14,10);
\stoptikzpicture}
\eTD
\eTR
\eTABLE
\stopplacefigure

\startplacefigure[location=here, reference=fig:EUA-quadriculada, title={Plànol d'EUA amb l'espai quadriculat}]
\bTABLE[frame=off,align=middle]
\bTR[align=middle]
\bTD
% adaptat de http://tex.stackexchange.com/questions/9559/drawing-on-an-image-with-tikz i http://www.texample.net/tikz/examples/connecting-text-and-graphics/
{\starttikzpicture
    \node[anchor=south west,inner sep=0] at (0,0) {\externalfigure[figs/proporcionalitat-geometrica-figs-EUA.png][scale=800]};
    \draw[color=orange!40,step=1cm,very thin] (0,0) grid (15,8);
\stoptikzpicture}
\eTD
\eTR
\eTABLE
\stopplacefigure
\stopexercici

\startsubsubject[title={Problemes d'estimacions}]

\startexercici En un bar es recapten entre 200 i 1000 euros de propina a la setmana. D'altra banda, s'estima que hi ha entre 20 i 50 clients a la setmana.
\startitemize[a,packed]
\item Es pot saber què ha gastat cada client?
\item Quin és els euros que com a màxim s'ha gastat un client?
\item Quin és el cas extrem en el que els clients han gastat el mínim?
\stopitemize
\stopexercici

\startexercici En Bartomeu sap que compra entre 6 i 10 ampolles de xampú a l'any i que les botelles de xampú han costat entre 1,5 i 3 €. Què s'ha gastat com a màxim en Bartomeu enguany? I com a mínim?
\stopexercici

\startexercici Per fer una coca, dues persones volem juntar el nombre d'ous que tenen. Però no s'enrecorden de quants en tenen i no ho poden mirar perquè no són a ca seva. Quants d'ous podran juntar si s'enrecorden que tenen entre 10 i 12 ous i entre 20 i 25 ous? Quants d'ous té de més el que més ous té?
\stopexercici
\stopsubsubject
\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exer:comparació-Algèria-Aràbia-Saudí]}
\startitemize[n]

\item En primer lloc, {\em quadrangularitzem} els països:

\startplacefigure[location=here, reference=fig:planol-Arabia-Saudi-Algeria-solució, title={Després de quadrangularitzar els països (cortesia de Judith Banda - 2014).}]
\bTABLE[frame=off,align={middle,lohi}]
\bTR
\bTD
 \externalfigure[figs/proporcionalitat-geometrica-figs-Solucio-Judith-Banda-2014-04-10.jpg][scale=1000]
\eTD
\eTR
\eTABLE
\stopplacefigure

\item I comptem el nombre de quadrats:

\startplacefigure[location={here,none}, reference=fig:planol-Arabia-Saudi-Algeria-recompte, title={Recompte de quadres}]
\bTABLE[setups={table5:header, table5:frame, table5:style}]
\bTR
\bTD País \eTD \bTD Quadres interiors \eTD \bTD Quadres exteriors \eTD \bTD Quadres totals \eTD
\eTR
\bTR
\bTD Algèria \eTD \bTD 8 \eTD \bTD 17 \eTD \bTD 25 \eTD 
\eTR
\bTR
\bTD Aràbia Saudí \eTD \bTD 4 \eTD \bTD 17 \eTD \bTD 21 \eTD 
\eTR
\eTABLE
\stopplacefigure

Els quadrats interiors ens proporcionen l'àrea mínima del país, mentre que el nombre de quadrats totals representen l'àrea màxima del país.

\item Amb una regla de tres, amb l'escala, tenim que cada quadrat té una àrea {\em real} de $400 \unit{kilo meter} \times 400 \unit{kilo meter} = 16.000 \unit{square kilo meter}$.

\item Per tant, l'àrea màxima i mínima de cada país és:

\startplacefigure[location={here,none}, reference=fig:planol-Arabia-Saudi-Algeria-recompte-area, title={Recompte de quadres}]
\bTABLE[setups={table5:header, table5:frame, table5:style}]
\bTR
\bTD País \eTD \bTD Àrea mínima \eTD \bTD Àrea màxima \eTD
\eTR
\bTR
\bTD Algèria \eTD \bTD $8 \cdot 16.000$ \eTD \bTD $25 \cdot 16.000$  \eTD 
\eTR
\bTR
\bTD Aràbia Saudí \eTD \bTD $4 \cdot 16.000$ \eTD \bTD $21 \cdot 16.000$ \eTD 
\eTR
\eTABLE
\stopplacefigure

\stopitemize


\sym{\in[exer:Australia-vs-Mallorca]} 

En primer lloc, quadrangularitzem el mapa de Mallorca i marquem en diferents colors els quadres que tenen només una part a Mallorca i els que estan continguts completament dins Mallorca (figura~\in[fig:planol-Mallorca-solució]), és dir, els quadres interiors i exteriors.

\startplacefigure[location=here, reference=fig:planol-Mallorca-solució, title={Mallorca quadrangularitzada}]
\bTABLE[frame=off,align={middle,lohi},width=fit]
\bTR
  \bTD
{% https://mailman.ntg.nl/archives/list/ntg-context@ntg.nl/thread/VDKPKRJSWX32WUSAYBJH5W2BD75EWMAV/
     \starttikzpicture
       \node[anchor=south west,inner sep=0] at (0,0) {\externalfigure[figs/proporcionalitat-geometrica-figs-Mallorca-OSM.png][scale=640]};
       \draw[help lines, xstep=1,ystep=1] (0,0) grid (17,12);
       % quadres parcials
       \foreach \p in {(9,0), (10,0), (9,1), (10,1), (11,1), (6,2), (7,2), (8,2), (12,2), (2,3), (3,3), (5,3), (6,3), (12,3), (1,4), (2,4), (3,4), (5,4), (6,4), (12,4), (13,4), (0,5), (1,5), (4,5), (5,5), (13,5), (14, 5), (1,6), (2,6), (14,6), (2,7), (3,7), (4,7), (14,7), (15,7), (4,8), (5,8), (11,8), (12,8), (13,8), (14,8), (5,9), (6,9), (10,9), (11,9), (6,10), (7,10), (8,10), (10,10), (11,10), (8,11), (9,11), (10,11), (11,11)}
       {
         \startscope[shift={(\p)}]
         \draw[color=red!60, thick] (0, 0) -- (1, 0) -- (1,1) -- (0, 1) -- cycle;
         \draw[color=red!60, thick] (0,0) -- (1,1);
         \draw[color=red!60, thick] (0,1) -- (1,0);       
         \stopscope
       }
       % quadres totals
       \foreach \p in {(9,2), (10,2), (7,3), (8,3), (9,3), (10,3), (11,3), (7,4), (8,4), (9,4), (10,4), (11,4), (2,5), (3,5), (6,5), (7,5), (8,5), (9,5), (10,5), (11,5), (12,5), (3,6), (4,6), (5, 6), (6,6), (7,6), (8, 6), (9,6), (10,6), (11,6), (12,6), (13,6), (5,7), (6,7), (7,7), (8, 7), (9, 7), (10, 7), (11, 7), (12,7), (13,7), (6,8), (7,8), (8,8), (9,8), (10,8), (7,9), (8,9), (9,9), (9,10)}
       {
         \startscope[shift={(\p)}]
         \draw[color=green!60, thick] (0, 0) -- (1, 0) -- (1,1) -- (0, 1) -- cycle;
         \draw[color=green!60, thick] (0,0) -- (1,1);
         \stopscope
       }
      \stoptikzpicture
}
  \eTD
\eTR
\eTABLE
\stopplacefigure

Per tant, tenim que:
\startitemize[packed]
\item El nombre de quadres interiors és igual a 50
\item El nombre de quadres exteriors és igual a 54
\item Per tant, el nombre de quadres total és 104
\item L'escala ens proporciona la informació de què $1,5 \unit{centi meter}$ al mapa corresponen a $10 \unit{kilo meter}$ a la realitat. Per tant, cada quadre dibuixat al mapa (que medeix $1 \unit{centi meter} \times 1 \unit{centi meter}$) correspon a un quadrat de $6,66 \unit{kilo meter} \times 6,66 \unit{kilo meter}$ a la realitat, és a dir, un quadrat de $44,44 \unit{square kilo meter}$ d'àrea aproximadament.
\item D'aquesta manera, l'àrea mínima és de $54 \cdot 44,44 \unit{square kilo meter} = 2.399,76 \unit{square kilo meter}$ i l'àrea màxima és igual a $104 \cdot 44,44 \unit{square kilo meter} = 4.621,76 \unit{square kilo meter}$.
\stopitemize


De la mateixa manera, podem procedir amb el mapa d'Austràlia (figura~\in[fig:planol-Austràlia-solució]).

\startplacefigure[location=here, reference=fig:planol-Austràlia-solució, title={Austràlia quadrangularitzada}]
\bTABLE[frame=off,align={middle,lohi},width=fit]
\bTR
  \bTD
     \starttikzpicture
       \node[anchor=south west,inner sep=0] at (0,0) {\externalfigure[figs/proporcionalitat-geometrica-figs-Australia-OSM.png][scale=600]};
       \draw[help lines, xstep=1,ystep=1] (0,0) grid (9,9);
       %\foreach \x in {0,1,...,9} { \node [anchor=north] at (\x,0) {\small $\x$}; }
       %\foreach \y in {0,1,...,9} { \node [anchor=east] at (0,\y) {\small $\y$}; }
       % quadres parcials
       \foreach \p in {(6,0), (7,0), (5,1), (6,1), (7,1), (1,2), (2,2), (3,2), (4,2), (5,2), (7,2), (0,3), (1,3), (3,3), (4,3), (8,3), (0,4), (7,4), (8,4), (0,5), (1,5), (2,5), (6,5), (7,5), (2,6), (3,6), (4,6), (5,6), (6,6), (3,7), (4,7), (5,7), (6,7)}
       {
         \startscope[shift={(\p)}]
         \draw[color=red!60, thick] (0, 0) -- (1, 0) -- (1,1) -- (0, 1) -- cycle;
         \draw[color=red!60, thick] (0,0) -- (1,1);
         \draw[color=red!60, thick] (0,1) -- (1,0);       
         \stopscope
       }
       % quadres totals
       \foreach \p in {(6,2), (2,3), (5,3), (6,3), (7,3), (1,4), (2,4), (3,4), (4,4), (5,4), (6,4), (3,5), (4,5), (5,5)}
       {
         \startscope[shift={(\p)}]
         \draw[color=green!60, thick] (0, 0) -- (1, 0) -- (1,1) -- (0, 1) -- cycle;
         \draw[color=green!60, thick] (0,0) -- (1,1);
         \stopscope
       }
      \stoptikzpicture
  \eTD
\eTR
\eTABLE
\stopplacefigure

Per tant, tenim que:
\startitemize[packed]
\item El nombre de quadres interiors és igual a 50
\item El nombre de quadres exteriors és igual a 14
\item Per tant, el nombre de quadres total és 64
\item L'escala ens diu que $1,4 \unit{centi meter}$ al mapa corresponen a $500 \unit{kilo meter}$ a la realitat. Per tant, cada quadre dibuixat al mapa (que medeix $1 \unit{centi meter} \times 1 \unit{centi meter}$) correspon a un quadrat de $357,14 \unit{kilo meter} \times 357,14 \unit{kilo meter}$ a la realitat, és a dir, un quadrat de $127.548,98 \unit{square kilo meter}$ d'àrea aproximadament.
\item D'aquesta manera, l'àrea mínima és de $50 \cdot 127.548,98 \unit{square kilo meter} = 6.377.449 \unit{square kilo meter}$ i l'àrea màxima és igual a $64 \cdot 127.548,98 \unit{square kilo meter} = 8.163.134,72 \unit{square kilo meter}$.
\stopitemize


I finalment, hem de comparar els dos països. Per a comparar dues magnituds normalment es divideix la major entre la menor i s'obté quantes vegades és més gran l'objecte major que l'objecte menor. Per tant, en el nostre cas, hauríem de dividir l'àrea d'Austràlia entre l'àrea de Mallorca per a saber quantes vegades és més gran Austràlia que Mallorca, és a dir, quantes {\em Mallorques} caben dins Austràlia.

Ara bé, tenim una estimació de les àrees per defecte i per excés. Quina prenem? Per saber-ho, observem aquest problema:

\startnarrower
\startexemple Es vol repartir els doblers d'una bossa entre els assistents a una festa. Es sap que hi ha entre 500 i 10.000 € dins la bossa, i que hi ha entre 100 i 200 persones. Què toca a cadascun?

Per resoldre aquest problema, està clar que volem saber què ens tocarà com a mínim i com a màxim:
\startitemize[packed]
\item La quantitat màxima que ens pot tocar seria si tenim la sort que a la bossa hi ha el màxim de doblers i som el menor nombre de persones. Per tant, com a màxim ens podria tocar: $10.000/100 = 100 €$
\item D'altra banda, el mínim que ens pot tocar correspondria a la {\em desgràcia} que fóssim el màxim nombre de persones a la festa, és a dir, 200, i hi hagués el mínim de doblers a la bossa. Per tant, ens podria tocar com a mínim $500/200 = 2,5 €$
\stopitemize

En resum, hem dividit el màxim d'una quantitat entre el mínim de l'altre i vice-versa, i no el mínim entre el mínim i el màxim entre el màxim, com algunes persones podrien pensar.
\stopexemple
\stopnarrower



En el nostre cas, hem de procedir de manera anàloga:
\startitemize[packed]
\item $8.163.134,72/2.399,76=3.401,64$
\item $6.377.449/4.621,76=1.379,87$
\stopitemize

És a dir, Austràlia és {\em com a màxim} 3.401,64 vegades més gran que Mallorca. I Austràlia és {\em com a mínim} 1.379,87 vegades més gran que Mallorca. De forma compacte, podríem dir que Austràlia és entre 1379 i 3402 vegades major que Mallorca.

\sym{\in[exer:lluna]} En primer lloc, hem de quadrangularitzar el plànol del Mare Serenitatis (figura~\in[fig:mare-serenitatis-quad]).

\startplacefigure[location=force, reference=fig:mare-serenitatis-quad, title={{\sl Mare Serenitatis} quadrangularitzada. Cortesia de Maxi González}]
\bTABLE[frame=off,align=middle]
\bTR[align=middle]
\bTD
\externalfigure[figs/proporcionalitat-geometrica-figs-Mare-Serenitatis-quad.png][scale=200]
\eTD
\eTR
\eTABLE
\stopplacefigure

Tenim que:
\startitemize[packed]
\item El nombre de quadres exteriors és 24
\item El nombre de quadres interiors és 22
\item El nombre de quadres totals és de 46
\item L'escala ens diu que 5 centímetres al mapa són 440 quilòmetres a la realitat. Per tant, cada quadre té una àrea de $1 \unit{square centi meter}$ al mapa i de $7.744 \unit{square kilo meter}$ a la realitat.
\item L'àrea mínima d'aquesta mar és de $22 \cdot 7.744 = 170.368 \unit{square kilo meter}$. I la seva àrea màxima, de $356.224 \unit{square kilo meter}$.
\stopitemize

A l'hora de comparar la superfície del {\sl Mare Serenitatis} amb Europa, podem fer igual que a l'exercici~\in[exer:Australia-vs-Mallorca]:
\startitemize[packed]
\item $10.180.000/170.368 = 59,75$
\item $10.180.000/356.224 = 28,57$
\stopitemize

És a dir Europa és entre 28,57 i 59,75 vegades més gran que el Mar de la Serenitat.

\sym{\in[exer:comparació-Islàndia-EUA]}

\startplacefigure[location=force, reference=fig:Islàndia-quadriculada-color, title={Plànol d'Islàndia amb les quadrícules pintades}]
\bTABLE[frame=off,align=middle]
\bTR[align=middle]
\bTD
    \externalfigure[figs/proporcionalitat-geometrica-figs-Islandia-quad2.png][scale=220]
\eTD
\eTR
\eTABLE
\stopplacefigure

\startplacefigure[location=force, reference=fig:USA-quadriculada-color, title={Plànol d'EUA amb les quadrícules pintades. Cortesia de Pau Wagner}]
\bTABLE[frame=off,align=middle]
\bTR[align=middle]
\bTD
    \externalfigure[figs/proporcionalitat-geometrica-figs-USA-quad.png][scale=200]
\eTD
\eTR
\eTABLE
\stopplacefigure

Tenim que:
\startitemize[packed]
\item El plànol d'Islàndia té $55$ quadres exteriors i $41$ quadres interiors. En total, $96$ quadres.
\item L'escala relaciona $2,5 \unit{centi meter}$ al plànol amb $100 \unit{kilo meter}$ a la realitat
\item Per tant, un quadre a la realitat té una àrea de $1.600 \unit{square kilo meter}$
\item Aleshores, l'àrea total d'Islàndia està entre $41 \cdot 1.600 = 65.600 \unit{square kilo meter}$ i $96 \cdot 1.600 = 153.600 \unit{square kilo meter}$.
\stopitemize

De la mateixa manera:
\startitemize[packed]
\item El plànol d'EUA té $39$ quadres exteriors i $45$ quadres interiors. En total, $84$ quadres.
\item L'escala relaciona $1,4 \unit{centi meter}$ al plànol amb $500 \unit{kilo meter}$ a la realitat
\item Per tant, un quadre a la realitat té una àrea de $127.548,98 \unit{square kilo meter}$
\item Aleshores, l'àrea total dels Estats Units d'Amèrica està entre $45 \cdot 127.548,98 = 5.739.704,1 \unit{square kilo meter}$ i $84 \cdot 127.548,98 = 1.0714.114,32 \unit{square kilo meter}$.
\stopitemize

Amb tot, EUA entre $1.0714.114,32/65.600=163,32$ i $5.739.704,1/153.600=37,36$ major que Islàndia.
\stopitemize

\stopsubject

\stopsection
