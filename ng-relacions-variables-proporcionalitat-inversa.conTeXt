\startsection[reference=seccio:proporcionalitat-inversa, title={Proporcionalitat inversa}]

\startsubject[title={Preguntes}]

\startexercici[reference=exercici:proporcionalitat-inversa-1, title={velocitat i temps d'arribada}] Aquests diagrames (vegeu taula~\in[taula:trajectes-mitjans-transport-velocitat-temps]) indiquen les velocitats i els temps d'arribada d'una sèrie de mitjans de transport. Calculeu \startitemize[a, text] \item el temps d'arribada per a la velocitat indicada \item i la velocitat necessària per arribar en el temps indicat \stopitemize

\startplacetable[location=force, reference=taula:trajectes-mitjans-transport-velocitat-temps, title={Velocitats i temps d'arribada d'un conjunt de mitjans de transport}]
\bTABLE[frame=off,align={flushleft,lohi},width=fit, offset=2mm]
  \bTR
    \bTD
      Avió
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$900 \unit{kilo meter per hour}$};
        \draw (0,-1) node[anchor=north] {$2 \unit{hour}$};
      \stoptikzpicture
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$820 \unit{kilo meter per hour}$};
        \draw (0,-1) node[anchor=north] {$? \unit{hour}$};
      \stoptikzpicture
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$? \unit{kilo meter per hour}$};
        \draw (0,-1) node[anchor=north] {$5 \unit{hour}$};
      \stoptikzpicture
    \eTD
  \eTR
  \bTR
    \bTD
      Tren
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$100 \unit{kilo meter per hour}$};
        \draw (0,-1) node[anchor=north] {$1,5 \unit{hour}$};
      \stoptikzpicture
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$80 \unit{kilo meter per hour}$};
        \draw (0,-1) node[anchor=north] {$? \unit{hour}$};
      \stoptikzpicture
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$? \unit{kilo meter per hour}$};
        \draw (0,-1) node[anchor=north] {$0,5 \unit{hour}$};
      \stoptikzpicture
    \eTD
  \eTR
  \bTR
    \bTD
      Vaixell
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$20 \, \text{Nusos}$};
        \draw (0,-1) node[anchor=north] {$80 \, \text{Min}$};
      \stoptikzpicture
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$25 \, \text{Nusos}$};
        \draw (0,-1) node[anchor=north] {$? \, \text{Min}$};
      \stoptikzpicture
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) circle (1);
        \draw (0,0) node {$? \, \text{Nusos}$};
        \draw (0,-1) node[anchor=north] {$3 \unit{hour}$};
      \stoptikzpicture
    \eTD
  \eTR
\eTABLE
\stopplacetable

\stopexercici

\startexercici[reference=exercici:proporcionalitat-inversa-2, title={processadors i temps d'acabament}] En aquests diagrama (vegeu taula~\in[taula:processadors-temps]) s'indica el nombre de processadors que s'empren per realitzar diverses tasques i el temps estimat per acabar-la (en minuts i segons).

\startplacetable[location={force, split}, reference=taula:processadors-temps, title={Nombre de processadors i temps estimat d'acabament de diverses tasques}]
\bTABLE[frame=off, align={flushleft, lohi}, width=fit, offset=2mm, split=yes]
  \bTR
    \bTD
      TASCA 1
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
    \eTD
    \bTD
      tarden
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) -- (3,0) -- (3,2) -- (0,2) --cycle;
        \draw (0.2,0.2) -- (2.8,0.2) -- (2.8,1.8) -- (0.2,1.8) -- cycle;
        \draw (1.5,1) node {05:00};
      \stoptikzpicture
    \eTD
  \eTR
  \bTR[topframe=on]
    \bTD
      TASCA 2
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture

    \eTD
    \bTD
      tarden
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) -- (3,0) -- (3,2) -- (0,2) --cycle;
        \draw (0.2,0.2) -- (2.8,0.2) -- (2.8,1.8) -- (0.2,1.8) -- cycle;
        \draw (1.5,1) node {12:30};
      \stoptikzpicture
    \eTD
  \eTR
  \bTR[topframe=on]
    \bTD
      TASCA 3
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
    \eTD
    \bTD
      tarden
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) -- (3,0) -- (3,2) -- (0,2) --cycle;
        \draw (0.2,0.2) -- (2.8,0.2) -- (2.8,1.8) -- (0.2,1.8) -- cycle;
        \draw (1.5,1) node {02:20};
      \stoptikzpicture
    \eTD
  \eTR
  \bTR[topframe=on]
    \bTD
      TASCA 4
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (1.5,0) -- (1.5,1.5) -- (0,1.5) -- cycle;
        \draw (0.75,0.75) node {CPU};
        % pins
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,1.5) -- (\x,1.7);
        }
        \foreach \x in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (\x,0) -- (\x,-0.2);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (1.5,\y) -- (1.7,\y);
        }
        \foreach \y in {0.3,0.6,...,1.5}{
        	\draw [line width=1mm] (-0.2,\y) -- (0,\y);
        }
      \stoptikzpicture
      
    \eTD
    \bTD
      tarden
    \eTD
    \bTD
      \starttikzpicture
        \draw (0,0) -- (3,0) -- (3,2) -- (0,2) --cycle;
        \draw (0.2,0.2) -- (2.8,0.2) -- (2.8,1.8) -- (0.2,1.8) -- cycle;
        \draw (1.5,1) node {00:40};
      \stoptikzpicture
    \eTD
  \eTR
\eTABLE
\stopplacetable

\startitemize[a]
\item Calculeu el temps que es tardaria a acabar
  \startitemize[A, packed, columns]
  \item La tasca 1 amb 5 CPU
  \item La tasca 2 amb 7 CPU
  \item La tasca 3 amb 5 CPU
  \item La tasca 4 amb 9 CPU
  \stopitemize
\item Calculeu el nombre de CPU necessàries per acabar:
  \startitemize[A, packed, columns]
  \item La tasca 1 en 12 hores
  \item La tasca 2 en 6 hores
  \item La tasca 3 en 1 hora
  \item La tasca 4 amb 10 minuts
  \stopitemize
\stopitemize
\stopexercici

\startexercici[reference=exercici:proporcionalitat-inversa-3, title={nombre de màquines i producció}] Una cooperativa disposa de tres màquines d'esclovellar. \startitemize[a, text] \item Es considera comprar dues màquines més del mateix tipus per augmentar la producció. Quant de temps s'estalviarien en la producció? \item Quantes màquines s'haurien de comprar per baixar el temps de producció al 50\%? \stopitemize


\blank[big]
Dades necessàries: les tres màquines esclovellen 3000 kilograms en 8 hores.
\stopexercici

\startexercici[reference=exercici:proporcionalitat-inversa-4, title={preu marginal}] Sabem quins són els preus d'una capsa de tatxes segons el nombre d'unitats comprades (vegeu taula~\in[taula:preus-tatxes]).

\startplacetable[location=force, reference=taula:preus-tatxes, title={Preus de les capses de tatxes}]
\bTABLE[frame=on,align={flushleft,lohi},width=fit, offset=2mm]
  \bTR
    \bTD[background=color, backgroundcolor=tablecolor, color=black, align={middle,lohi},style=ss,headstyle=ss]
      Unitats
    \eTD
    \bTD
      500 unitats
    \eTD
    \bTD
      1000 unitats
    \eTD
  \eTR
  \bTR
    \bTD[background=color, backgroundcolor=tablecolor, color=black, align={middle,lohi},style=ss,headstyle=ss]
      Preu capsa
    \eTD
    \bTD[align={middle,lohi}]
      3,65 €
    \eTD
    \bTD[align={middle,lohi}]
      7,00 €
    \eTD
  \eTR
  \bTR
    \bTD[background=color, backgroundcolor=tablecolor, color=black, align={middle,lohi},style=ss,headstyle=ss]
      Preu marginal
    \eTD
    \bTD
    
    \eTD
    \bTD
    
    \eTD
  \eTR
\eTABLE
\stopplacetable

\startitemize[a]
\item Podeu calcular el cost de cada tatxa. Realment això es defineix com a {\em cost marginal}\index{cost marginal}, és a dir, el cost de cada unitat.
\item Què costaria una tatxa en una capsa de 5000 unitats?
\item Quina capacitat hauria de tenir la capsa per a què el cost marginal fos de 0,001 €?
\stopitemize
\stopexercici

\startexercici[reference=exercici:proporcionalitat-inversa-5, title={llei de Pareto}] En aquest diagrama (figura~\in[figura:edicicions-wiki]) es representa el nombre de persones i el nombre d'edicions d'un wiki, com podria ser Wikipedia.

\startplacefigure[location=here, reference=figura:edicicions-wiki, title={Diagrama que representa el nombre de persones amb un nombre d'edicions determinat. Les variables són inversament proporcionals. El diagrama no està a escala.}]
\bTABLE[frame=off,align={flushleft,lohi},width=fit, offset=2mm]
  \bTR
    \bTD
      \starttikzpicture
        \draw (0,0) -- (2,0) -- (2,6) -- (0,6) --cycle;
        \filldraw[fill=red!20] (0,0) -- (2,0) -- (2,6) -- (0,6) --cycle;
        \draw (1,6) node[anchor=south] {6000 edicions};
        \draw (1,0) node[anchor=north] {10 usuaris};
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (2,0) -- (2,3) -- (0,3) --cycle;
        \filldraw[fill=orange!20] (0,0) -- (2,0) -- (2,3) -- (0,3) --cycle;
        \draw (1,3) node[anchor=south] {1200 edicions};
        \draw (1,0) node[anchor=north] {50 usuaris};
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (2,0) -- (2,1.5) -- (0,1.5) --cycle;
        \filldraw[fill=blue!20] (0,0) -- (2,0) -- (2,1.5) -- (0,1.5) --cycle;
        \draw (1,1.5) node[anchor=south] {??? edicions};
        \draw (1,0) node[anchor=north] {100 usuaris};
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (2,0) -- (2,0.75) -- (0,0.75) --cycle;
        \filldraw[fill=green!20] (0,0) -- (2,0) -- (2,0.75) -- (0,0.75) --cycle;
        \draw (1,0.75) node[anchor=south] {??? edicions};
        \draw (1,0) node[anchor=north] {200 usuaris};
      \stoptikzpicture
      \starttikzpicture
        \draw (0,0) -- (2,0) -- (2,0.375) -- (0,0.375) --cycle;
        \filldraw[fill=orange!20] (0,0) -- (2,0) -- (2,0.375) -- (0,0.375) --cycle;
        \draw (1,0.375) node[anchor=south] {??? edicions};
        \draw (1,0) node[anchor=north] {500 usuaris};
      \stoptikzpicture
    \eTD
  \eTR
\eTABLE
\stopplacefigure

\blank[medium]
\startitemize
\starthead{Part 1}

El diagrama està incomplet. El podríeu completar?

\stophead
\starthead{Part 2}

\startitemize[a]
\item El diagrama anterior no està a escala. Segons les dades que heu esbrinat, el podríeu dibuixar a escala?
\item Podríeu haver completat el diagrama anterior si haguéssiu tengut la dada que 50 usuaris fan 2000 edicions?
\stopitemize

\stophead

\stopitemize

\blank[big]
La {\em Llei de Pareto}\index{llei+de Pareto} {\em essencialment} diu que la riquesa en el món es distribueix desigualment: hi ha molt poca gent rica i moltes persones pobres \cite{wikipedia-principi-de-Pareto}. Aquesta llei es pot generalitzar a altres àmbits en el que es diu {\em Llei potencial}\index{llei+potencial} \cite{wikipedia-llei-potencial}.
\stopexercici

\startexercici[reference=exercici:proporcionalitat-inversa-6, title={pressió del gas}] Es sap que a més pressió un gas ocupa menys volum \cite{wikipedia-llei-gasos-ideals}. De fet, les variables pressió i volum són inversament proporcionals. Sabent això, podríeu completar aquesta taula (taula~\in[taula:gas-ideal-pressio-volum])?

\startplacetable[location=force, reference=taula:gas-ideal-pressio-volum, title={Preus de les capses de tatxes}]
\bTABLE[frame=on,align={flushleft,lohi},width=fit, offset=2mm]
  \bTR
    \bTD[background=color, backgroundcolor=tablecolor, color=black, align={middle,lohi},style=ss,headstyle=ss]
      Pressió
    \eTD
    \bTD
      $5 \unit{kilo pascal}$
    \eTD
    \bTD
      $12 \unit{kilo pascal}$
    \eTD
    \bTD
      $3 \unit{kilo pascal}$
    \eTD
    \bTD
      $36 \unit{kilo pascal}$
    \eTD
  \eTR
  \bTR
    \bTD[background=color, backgroundcolor=tablecolor, color=black, align={middle,lohi},style=ss,headstyle=ss]
      Volum
    \eTD
    \bTD[align={middle,lohi}]
      $2 \unit{cubic meter}$
    \eTD
    \bTD[align={middle,lohi}]
      
    \eTD
    \bTD[align={middle,lohi}]
      
    \eTD
    \bTD[align={middle,lohi}]
      
    \eTD
  \eTR
\eTABLE
\stopplacetable

\stopexercici

\startexercici[reference=exercici:proporcionalitat-inversa-7, title={pes de la càrrega i velocitat màxima d'un vehicle}] A partir d'un punt, quan major és la massa de la càrrega que duu un vehicle menor és la velocitat màxima que pot assolir (vegeu taula~\in[taula:pes-i-velocitat-maxima-bicicleta]). En realitat la massa de la càrrega i la velocitat es relacionen de forma inversament proporcional.

\startplacetable[location=force, reference=taula:pes-i-velocitat-maxima-bicicleta, title={Velocitat màxima d'una bicicleta en funció de la massa de la càrrega}]
\bTABLE[frame=on,align={middle,lohi},width=fit, offset=2mm]
  \bTR
    \bTD[background=color, backgroundcolor=tablecolor, color=black, align={middle,lohi},style=ss,headstyle=ss]
      Càrrega ($\unit{kilo gram}$)
    \eTD
    \bTD
      $20 \unit{kilo gram}$
    \eTD
    \bTD
      $60 \unit{kilo gram}$
    \eTD
    \bTD
      $80 \unit{kilo gram}$
    \eTD
    \bTD
      $100 \unit{kilo gram}$
    \eTD
  \eTR
  \bTR
    \bTD[background=color, backgroundcolor=tablecolor, color=black, align={middle,lohi},style=ss,headstyle=ss]
      Velocitat màxima ($\unit{kilo meter per hour}$)
    \eTD
    \bTD
      $50 \unit{kilo meter per hour}$
    \eTD
    \bTD
      
    \eTD
    \bTD
      
    \eTD
    \bTD
      
    \eTD
  \eTR
\eTABLE
\stopplacetable

Completeu la taula anterior.
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-1, title={caseta}] Es sap que vint persones tarden 24 dies a fer una caseta. Quant tardaran 5 persones?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-2, title={dipòsit}] Un dipòsit és ple en 12 hores utilitzant una boca d'aigua que expulsa 180 litres d'aigua per minut. Calculeu:
\startitemize[a]
\item El temps que tardaria a omplir-se si la boca d'aigua llancés 90 litres per minut
\item La quantitat d'aigua per minut que seria necessària que sortís per la boca d'aigua per omplir el dipòsit en 36 hores
\stopitemize
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-3, title={puzzle}] Cinc persones tarden 1 hora a resoldre un puzzle. Quantes hores tardaran set persones?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-4, title={construcció d'una paret}] Sis obrers tarden dues hores en fer una paret. Quant tardaran 4 obrers?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-5, title={escriure un conte}] Tres persones són capaces d'escriure un conte en 6 hores. Quant tardaria una sola persona a escriure el mateix conte?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-6, title={aixetes}] Tenim un dipòsit i 4 aixetes damunt seu que estan totes tancades. Si obrim 3 de les 4 aixetes, tardam 39 minuts. Quant tardaríem en omplir el dipòsit amb les 4 aixetes obertes?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-7, title={pintar una paret}] Quatre obrers tarden 3 hores per pintar una paret. \startitemize[a,text] \item Quantes hores tardaran 6 obrers? \item Quants d'obrers fan falta per pintar la paret en 4 hores?\stopitemize
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-8, title={banda de música}] Una colla de 5 amics formen un grup de rock. Volen llogar els instruments i els toca pagar 60 € a cadascun. Ho troben una mica car i demanen la col·laboració d'un altre amic. Quant haurà de pagar ara cadascun?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-9, title={tasca de Català}] El professor de Català ha donat a fer a 4 alumnes un treball de 60 pàgines en total, fent 15 pàgines cadascun. Si un d'ells s'ha posat malalt i els altres han d'acabar el treball, quantes pàgines de més han de fer cadascun?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-10, title={velocitat d'un cotxe}] Si un cotxe tarda 2h 20 min en arribar al seu destí anant a $80 \unit{kilo meter per hour}$. Quant tardarà si va a $100 \unit{kilo meter per hour}$?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-11, title={excavadores}] Si deu excavadores tarden 30 dies en fer un túnel pel metro de Palma, què tardarien 4 excavadores?
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-v1.1-exercici-12, title={aixetes}] Quatre aixetes tarden 6 hores per omplir un dipòsit. Què tardaran vint-i-dues aixetes? Expresseu-ho en hores, minuts i segons.
\stopexercici

\startexercici[reference=exer:proporcionalitat-inversa-Xisco-Sebastia-modificat, title={consum d'aigua $\star$}] Una persona fa un viatge pel desert. Per estalviar aigua, decideix beure aigua a raó de 33 cl per dia perquè li basti l'aigua per 15 dies. I així ho fa durant la primera setmana, després de la qual es perd.

Per això decideix reduir encara més el consum d'aigua a $10 \unit{centi liter per day}$.

\startitemize[a]
\item Calculeu per quants dies li bastarà l'aigua que li queda
\item Quina quantitat d'aigua tenia al dipòsit inicialment i després d'una setmana
\stopitemize
\stopexercici

\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\item
\stopitemize

\stopsubject

\stopsection
