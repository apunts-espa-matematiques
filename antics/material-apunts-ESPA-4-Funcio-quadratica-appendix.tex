% Appendix de seccions còniques
\chapter{Seccions còniques}

\section{El con}

El con és un cos geomètric que s'obté unint un punt $V$, el vèrtex del con, amb la vora d'un cercle, que forma la seva base (figura \in[fig:con]). L'aresta del con s'anomena {\em generatriu}.

\placefigure[here]
[fig:con]
{Un con de vèrtex $V$}
{% fitxer original: material-apunts-ESPA-4-Funcio-quadratica-appendix-figs-con.fig
\externalfigure[material-apunts-ESPA-4-Funcio-quadratica-appendix-figs-con.eps][scale=900]}

Es poden unir dos cons vèrtex contra vèrtex i allargar-los indefinidament, amb el que s'obté un con doble, de superfície infinita (figura \in[fig:con2]):

\placefigure[here]
[fig:con2]
{Un con doble (infinit)}
{% fitxer original: material-apunts-ESPA-4-Funcio-quadratica-appendix-figs-con-doble.fig
\externalfigure[material-apunts-ESPA-4-Funcio-quadratica-appendix-figs-con-doble.eps][orientation=90,scale=700]}


El con doble es pot veure com un cos de revolució que es produeix al girar la recta de la generatriu sobre l'eix del con (figura \in[fig:con-revolucio]):

% http://www.texample.net/tikz/examples/truncated-cone/
\placefigure[here]
[fig:con-revolucio]
{Un con com a cos de revolució}
{
\framed[frame=off]{
\starttikzpicture
% Primer con
  \draw[semithick] (0,3) -- (6,3/2);% top line
  \draw[semithick] (0,0) -- (6,3/2);% bottom line
  
  \draw[semithick, postaction={decoration={markings,mark=at position 0.75 with {\arrow[black, line width=1mm]{>}}}, decorate}] (0,0) arc (270:90:0.5 and 1.5);% left half of the left ellipse
  \draw[dashed,color=gray,postaction={decoration={markings,mark=at position 0.25 with {\arrow[dashed,gray, line width=1mm]{<}}}, decorate}] (0,0) arc (-90:90:0.5 and 1.5);% right half of the left ellipse
  \shadedraw[semithick,left color=gray, right color=gray!50] (4,1.5) ellipse (0.166 and 0.5);% right ellipse
  \draw (-1.5,1.5) node[anchor=north] {$\text{eix}$};

% Segon con
  \draw[semithick] (6,3/2) -- (12,3);% top line
  \draw[semithick] (6,3/2) -- (12,0);% bottom line
  \draw[semithick, postaction={decoration={markings,mark=at position 0.75 with {\arrow[black, line width=1mm]{>}}}, decorate}] (12,0) arc (270:90:0.5 and 1.5);% left half of the left ellipse
  \draw[color=gray,postaction={decoration={markings,mark=at position 0.25 with {\arrow[dashed,gray, line width=1mm]{<}}}, decorate}] (12,0) arc (-90:90:0.5 and 1.5);% right half of the left ellipse
  \shadedraw[semithick,left color=gray, right color=gray!50] (8,1.5) ellipse (0.166 and 0.5);% right ellipse

% Eix

  \draw (-2,1.5) -- (-0.5,1.5);
  \draw[loosely dashed] (0,1.5) -- (11.5, 1.5);
  \draw (12, 1.5) -- (13,1.5);

\stoptikzpicture
}
}


\section{La circumferència, l'el·lipse, la paràbola i la hipèrbola}

Les {\em seccions còniques} o simplement {\em còniques} són corbes que s'obtenen de tallar un con doble amb un pla. Segons com sigui la inclinació del pla, aquest tall variarà de forma i donarà lloc a corbes diferents.

\placefigure[here]
[fig:coniques]
{Possibles talls d'un con amb un pla}
{% fitxer original: material-apunts-ESPA-4-Funcio-quadratica-appendix-figs-wikipedia-conic-sections-with-plane.svg
\externalfigure[material-apunts-ESPA-4-Funcio-quadratica-appendix-figs-wikipedia-conic-sections-with-plane.pdf][width=\textwidth]}

\startitemize
\item Si el con es talla perpendicular al seu eix, aleshores obtenim una {\em circumferència} (figura \in[fig:coniques]-2)
\item Obtenim una {\em el·lipse} si es talla el con amb un angle menor que 90 graus però major que l'angle de la generatriu del con (figura \in[fig:coniques]-2)
\item La {\em paràbola} s'obté quan la inclinació del pla és exactament la mateixa que la generatriu del con (figura \in[fig:coniques]-1)
\item Finalment, quan la inclinació és menor que l'angle de la recta generatriu, llavors s'obté una {\em hipèrbola} (figura \in[fig:coniques]-3)
\stopitemize

\subsection{Referències externes}

\startitemize[n,packed]
\item \goto{Visualització de les seccions còniques 1}[url(https://www.youtube.com/watch?v=GDHNoQHQmtQ)]: \hyphenatedurl{https://www.youtube.com/watch?v=GDHNoQHQmtQ}

\item \goto{Visualització de les seccions còniques 2}[url(https://www.youtube.com/watch?v=bFOnicn4bbg)]: \hyphenatedurl{https://www.youtube.com/watch?v=bFOnicn4bbg}

\item \goto{Visualització de les seccions còniques 3. Simulació amb SketckUp}[url(https://www.youtube.com/watch?v=heB2s6p0Tms)] (anglès): \hyphenatedurl{https://www.youtube.com/watch?v=heB2s6p0Tms}

\item \goto{Visualització de les seccions còniques com a la llum d'una llinterna}[url(https://www.youtube.com/watch?v=yuJ3kydUUfM)]: \hyphenatedurl{https://www.youtube.com/watch?v=yuJ3kydUUfM}

\item \goto{Visualització de les seccions còniques com a les ombres d'una pilota 2}[url(http://www.youtube.com/watch?v=xeKYNbVGEpw)]: \hyphenatedurl{http://www.youtube.com/watch?v=xeKYNbVGEpw}
\stopitemize


\section{Les equacions de les seccions còniques}

Les seccions còniques corresponen a corbes algebraiques de grau 2, que tenen la forma general
\placeformula[eq:formula-general]
\startformula
ax^2 + bxy + cy^2 + dx + ey + f=0,
\stopformula
encara que hi ha formes simplicades o {\em canòniques}, que s'empren usualment:

\startitemize[a]
\item La circumferència la defineix l'equació: $x^2 + y^2 = r^2$, on $r$ és el radi
\item L'el·lipse la defineix l'equació $\frac{x^2}{a^2} + \frac{y^2}{b^2} = 1$
\item La paràbola la defineix l'equació $y=ax^2 + b^2 + c$
\item La hipèrbola la defineix l'equació $\frac{x^2}{a^2} - \frac{y^2}{b^2} = 1$
\stopitemize

Es poden classificar les seccions còniques segons el {\em discriminant} $D=b^2 -4ac$ de l'equació~(\in[eq:formula-general]):
\startitemize
\item Si $D < 0$, llavors l'equació \in[eq:formula-general] representa una el·lipse. El cas especial de la circumferència s'obté quan $a=c$ i $b=0$.
\item Si $D = 0$, llavors l'equació representa una paràbola
\item Si $D > 0$, llavors l'equació representa una hipèrbola
\stopitemize

\section{Construcció geomètrica de les còniques}

Cadascuna de les còniques es pot definir de manera alternativa de la forma següent (figura \in[fig:coniques-geom]):
\startitemize[a]
\item Donat un punt $O$, anomenat {\em centre}, i un nombre real $r > 0$, que s'anomena {\em radi}, la {\em circumferència de radi $r$ i centre $O$} és el conjunt de punts que es troben a distància $r$ del punt $O$.
\item Donats dos punts $F_1$ i $F_2$, anomenats {\em focus}, i un nombre real $k > 0$, l'{\em el·lipse} és el conjunt de punts tals que la suma de les distàncies a $F_1$ i a $F_2$ és igual a $k$, és a dir,
\startformula
d(P, F_1) + d(P, F_2) = k,
\stopformula
per a qualsevol punt de l'el·lipse.
\item Donats dos punts $F_1$ i $F_2$, anomentats {\em focus}, i un nombre real $k > 0$, la {\em hipèrbola} és el conjunt de punts tals que la diferència de les distàncies a $F_1$ i a $F_2$ és igual a $k$, és a dir,
\startformula
d(P, F_1) - d(P, F_2) = k,
\stopformula
per a qualsevol punt de la hipèrbola.
\item Donats un punt $F$, anomenat {\em focus}, i una recta $r$, anomenada {\em directriu}, la {\em paràbola} és el conjunt de punts tals que equidisten a $F$ i a $r$, és a dir,
\startformula
d(P, F) = d(P, r),
\stopformula
per a qualsevol punt $P$ de la paràbola.
\stopitemize

\placefigure
[here] % placement
[fig:coniques-geom] % reference
{Definició geomètrica de les còniques} % caption for whole group
{
\startcombination[2*2] % 3 columns, 2 rows
{

\framed[frame=off]{
\starttikzpicture
\draw (0,0) circle (2);
\draw (0,0) node[anchor=south] {$O$};
\filldraw[color=blue] (0,0) circle (2pt);
\draw (0,0) -- (2,0);
\draw (1,0) node[anchor=north] {$r$};
\stoptikzpicture}


} {Circumferència} {

% x^2/9 + y^2/4 = 1
\framed[frame=off]{
\starttikzpicture[scale=0.9]
\coordinate (F1) at (2.23606797749978969641,0);
\coordinate (F2) at (-2.23606797749978969641,0);
\draw (0,0) ellipse (3 and 2);
\filldraw[color=blue] (F1) circle (2pt);
\filldraw[color=blue] (F2) circle (2pt);
\draw[color=gray, semithick, loosely dashed] (F1) -- (1,1.88561808316412673174)--(F2);
\draw[color=gray, semithick, loosely dashed] (F1) -- (0,2)--(F2);
\draw[color=gray, semithick, loosely dashed] (F1) -- (-2,1.49071198499985979761)--(F2);
\filldraw[color=gray] (1,1.88561808316412673174) circle (2pt);
\filldraw[color=gray] (0,2) circle (2pt);
\filldraw[color=gray] (-2,1.49071198499985979761) circle (2pt);
\draw (1,1.88561808316412673174) node[anchor=south] {$P$};
\draw (0,2) node[anchor=south] {$P$};
\draw (-2,1.49071198499985979761) node[anchor=south] {$P$};

\draw (F1) node[anchor=north] {$F_1$};
\draw (F2) node[anchor=north] {$F_2$};
\stoptikzpicture}


} {El·lipse}
{

\framed[frame=off]{
\starttikzpicture
% hipèrbola y = 1/x
\coordinate (F1) at (-1.4142135623730950488,-1.4142135623730950488);
\coordinate (F2) at (1.4142135623730950488,1.4142135623730950488);
\draw[semithick] plot[domain=-2:-0.5] (\x,{divide(1,\x)});
\draw[semithick] plot[domain=0.2:3] (\x,{divide(1,\x)});
\foreach \x in {-0.75, 0.25, 0.75}
{
   \draw[color=gray, semithick, loosely dashed] (F1) -- (\x,{divide(1, \x)}) --(F2);
   \draw (\x,{divide(1, \x)}) node[anchor=north] {$P$};
   \filldraw[color=gray] (\x,{divide(1, \x)}) circle (2pt);
}
\filldraw[color=blue] (F1) circle (2pt);
\filldraw[color=blue] (F2) circle (2pt);
\draw (F1) node[anchor=east] {$F_1$};
\draw (F2) node[anchor=west] {$F_2$};

\stoptikzpicture}


} {Hipèrbola} {

\framed[frame=off]{
\starttikzpicture[domain=(-1):1, scale=3, smooth]
\coordinate (F) at (0,0.25);
\draw[very thick] (-1.2,-0.25) -- (1.2,-0.25);
\draw[very thick] plot (\x,{(\x)^2});

\foreach \x in {1,0.5,0.75}
{
   \draw[very thick,color=gray, loosely dashed] (\x, -0.25) -- (\x , {(\x)^2});
   \draw[very thick,color=gray, loosely dashed] (\x , {(\x)^2}) -- (F);
}
\filldraw[color=blue] (F) circle (0.5pt);
\draw (F) node[anchor=north] {$F$};
\stoptikzpicture}


} {Paràbola}
\stopcombination
} % whole combination in braces of placefigure

\subsection{Focus, directriu i excentricitat}

Donats una recta, anomenada {\em directriu}, i un punt $F$, anomenat {\em focus}, que no pertanyi a la recta i un nombre real positiu $e$, anomenat {\em excentricitat}, podem trobar el conjunt de punts $P$ tals que
\placeformula[eq:excentricitat]
\startformula
e = \frac{d(P, F)}{d(P, r)}.
\stopformula

Hi ha una correspondència entre aquests conjunts de punts i les seccions còniques. A més, segons l'excentricitat $e$, s'obtenen diferents còniques (figura \in[fig:excentricitat]):
\startitemize
\item Si $0 < e < 1$, aleshores s'obté una el·lipse
\item Si $e = 1$, s'obté una paràbola
\item Si $e > 1$, s'obté una hipèrbola
\stopitemize

El cas de la circumferència correspon al cas en què $e=0$ en què podem imaginar que la directriu està infinitament enfora del centre de la circumferència (seria el cas límit de l'equació~(\in[eq:excentricitat]) quan $d(P, r)$ tendeix a l'infinit).

\placefigure[force]
[fig:excentricitat]
{Excentricitat de diverses còniques}
{% fitxer original: material-apunts-ESPA-4-Funcio-quadratica-appendix-figs-wikipedia-Eccentricity.svg
\externalfigure[material-apunts-ESPA-4-Funcio-quadratica-appendix-figs-wikipedia-Eccentricity.pdf][scale=800]}


\subsection{Referències externes}

\startitemize[n,packed]
\item \goto{La generació de les seccions còniques depenent de l'excentricitat}[url(http://www.youtube.com/watch?v=fRAeGvjiM1A)] (anglès):

\hyphenatedurl{http://www.youtube.com/watch?v=fRAeGvjiM1A}
\stopitemize

\page[yes]
\chapter{Recordatori d'àrees i volums}

\section[definicions-geometriques]{Definicions geomètriques}

\startitemize[n]
%\startlines
\item Un {\em polígon} és una figura plana composta per un nombre finit de segments rectes que s'uneixen format una figura tancada. Els seus punts s'anomenen {\em vèrtexos} i els segments {\em costats}.
\item El {\em perímetre} d'un polígon és la suma de les longituds dels seus costats
\item Un {\em triangle} és un polígon que té tres costats.
\item Un {\em rectangle} és un polígon que té quatre costats que formen angles de 90º. Quan en un rectangle, tots els costats són iguals, aquests formen un {\em quadrat}
\item En general, un polígon de quatre costats s'anomena {\em quadrilàter}. Casos especials dels quadrilàter són el rectangle, el quadrat, el rombe, el romboide i el trapezi.
\item El {\em trapezi} és un quadrilàter que té un parell de costats paral·lels.
\item Un quadrilàter amb dos parells de costats paral·lels s'anomena {\em paral·lelogram}. Casos especials d'un paral·lelogram són el rombe, el romboide, el rectangle i el quadrat.
\item Un {\em rombe} és un paral·lelogram que té tots els costats iguals
\item Un {\em romboide} és un paral·lelogram tal que els costats oposats són paral·lels i els costats adjacents no són iguals i els angles no són rectes
\item Un rectangle és un paral·lelogram que té tots els angles rectes. El quadrat és el cas particular amb tots els costats iguals.
\item Quan tots els costats d'un polígon són iguals, aquest s'anomena {\em polígon regular}.
\item Segons el nombre de costats, el polígon pot ser un {\em pentàgon} (de cinc costats), un {\em hexagon} (de sis costats), un {\em heptàgon} (de set costats), etc.
\item L'{\em apotema} d'un polígon regular és el segment que va des del centre del polígon a la meitat d'un costat
\item Un {\em cercle} és la porció de pla dels punts que estan a distància menor o igual que un nombre fixat, que s'anomena {\em radi}. La {\em circumferència} és la vora del cercle
\item Un {\em políedre} és un cos geomètric delimitat per un nombre finit de cares poligonals. Les {\em arestes} són els costats dels polígons que el limiten. Els {\em vèrtexs} són els punts comuns a dues o més cares.
\item Un {\em prisma} és un políedre que té dues cares iguals i paral·leles (les {\em bases}) i cert nombre de cares laterals que són paral·lelograms (les {\em cares laterals}). Si les cares laterals no formen un angle de 90º amb les bases es parla de {\em prismes oblics}. Si les cares laterals són rectangles s'anomena {\em prisma rectangular}.
\item Una {\em piràmide} és un políedre que té per base un polígon i les seves cares laterals són triangles que tenen un vèrtex comú, el qual s'anomena {\em vèrtex} de la piràmide.
\item Un {\em cilindre} és un cos de revolució que s'obté en girar un rectangle al voltant d'un dels seus costats.
\item Un {\em con} és un cos de revolució que s'obté en girar un triangle rectangle al voltant d'un dels seus catets. El {\em costat} que va del seu vèrtex a la base (un cercle) s'anomena {\em generatriu}.
\item Una {\em esfera} és un cos de revolució que s'obté en girar un semicercle al voltant del seu diàmetre. Equivalentment són els punts de l'espai que estan a distància menor o igual que el radi d'aquest semicercle.
\stopitemize


\page[yes]
% Annex d'àrees
\section[arees-figures-planes]{Àrees de les figures planes més usuals}

\placetable[split,force]
[taula:area-figures-planes]
{Àrees de les figures planes més usuals}
{\bTABLE[frame=off,align=middle,width=broad,split=yes]
  \setupTABLE[c][each][align={middle,lohi}]
   \bTR
     \bTD \starttikzpicture
\filldraw[color=blue!30] (0,0) -- (3,0) -- (3,2) -- (0,2) -- cycle;
\draw (0,0) -- (3,0) -- (3,2) -- (0,2) -- cycle;
\draw (0,1) node[anchor=east] {$h$};
\draw (1.5,0) node[anchor=north] {$b$};
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
1. Rectangle

\startformula
A=b \cdot h
\stopformula
\stopframedtext

\eTD
     \bTD \starttikzpicture
\filldraw[color=blue!30] (0,0) -- (2,0) -- (2,2) -- (0,2) -- cycle;
\draw (0,0) -- (2,0) -- (2,2) -- (0,2) -- cycle;
\draw (0,1) node[anchor=east] {$c$};
\draw (1,0) node[anchor=north] {$c$};
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
2. Quadrat

\startformula
A=c \cdot c = c^2
\stopformula
\stopframedtext
\eTD
     \bTD \starttikzpicture
\filldraw[color=blue!30] (0,0) -- (4,0) -- (1,2) -- cycle;
\draw (0,0) -- (4,0) -- (1,2) -- cycle;
\draw[help lines] (1,2) -- (1,0);
\draw (1,1) node[anchor=west] {$h$};
\draw (2,0) node[anchor=north] {$b$};
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
3. Triangle

\startformula
A=\frac{b \cdot h}{2}
\stopformula
\stopframedtext

\eTD
   \eTR
   \bTR
     \bTD \starttikzpicture
\filldraw[color=blue!30] (0,-3) -- (1.5,0) -- (0,3) -- (-1.5,0)-- cycle;
\draw (0,-3) -- (1.5,0) -- (0,3) -- (-1.5,0)-- cycle;
\draw[help lines] (-1.5,0) -- (1.5,0);
\draw[help lines] (0,3) -- (0,-3);
\draw (-0.75,0) node[anchor=north] {$d$};
\draw (0,0.75) node[anchor=west] {$D$};
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
4. Rombe

\startformula
A = \frac{D \cdot d}{2}
\stopformula
\stopframedtext
\eTD
     \bTD \starttikzpicture
\filldraw[color=blue!30] (0,0) -- (3,0) -- (4,2) -- (1,2) -- cycle;
\draw (0,0) -- (3,0) -- (4,2) -- (1,2) -- cycle;
\draw[help lines] (1,0) -- (1,2);
\draw (1,1) node[anchor=east] {$h$};
\draw (1.5,0) node[anchor=north] {$b$};
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
5. Romboide

\startformula
A = b \cdot h
\stopformula
\stopframedtext
\eTD
     \bTD \starttikzpicture
\filldraw[color=blue!30] (0,0) -- (3,0) -- (2.5,2) -- (1,2) -- cycle;
\draw (0,0) -- (3,0) -- (2.5,2) -- (1,2) -- cycle;
\draw[help lines] (1,0) -- (1,2);
\draw (1,1) node[anchor=east] {$h$};
\draw (1.5,0) node[anchor=north] {$B$};
\draw (1.75,2) node[anchor=south] {$b$};
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
6. Trapezi

\startformula
A = \frac{(B+b) \cdot h}{2}
\stopformula
\stopframedtext
\eTD
   \eTR
   \bTR
     \bTD \starttikzpicture
\filldraw[color=blue!30] (0,0) circle (2);
\draw (0,0) circle (2);
\draw[help lines] (0,0) -- (2,0);
\draw (1.5,0) node[anchor=north] {$r$};
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
7. Cercle

\startformula
\startalign
\NC A \NC = \pi \cdot r^2 \NR
\NC L \NC = 2 \cdot \pi \cdot r
\stopalign
\stopformula
\stopframedtext

\eTD
     \bTD \space \eTD
     \bTD \starttikzpicture
\node[regular polygon, regular polygon sides=5, minimum size=4cm, draw, fill=blue!30] at (0,0) {};
% amb trigonometria, té longitud 2 cos 36º
\draw[help lines] (0,0) -- (0,-1.61803cm);
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
8. Polígon regular

\startformula
A = \frac{\text{P} \cdot \text{a}}{2}
\stopformula
\stopframedtext
\eTD
   \eTR
 \eTABLE}


\page[yes]
% Annex de volums
\section[sec:volums-principals]{Volums i àrees dels cossos geomètrics més usuals}

\placetable[split,force]
[taula:volums-principals]
{Volums (i algunes àrees) dels cossos geomètrics més usuals}
{\bTABLE[frame=off,align=middle,width=broad,split=yes]
  \setupTABLE[c][each][align={middle,lohi}]
   \bTR
     \bTD
     \starttikzpicture[line join=bevel,z=-5.5,scale=2]
\coordinate (A1) at (0,0,-1);
\coordinate (A2) at (-1,0,0);
\coordinate (A3) at (0,0,1);
\coordinate (A4) at (1,0,0);
\coordinate (B1) at (0,1,0);
\coordinate (C1) at (0,-1,0);

\draw (A1) -- (A2) -- (B1) -- cycle;
\draw (A4) -- (A1) -- (B1) -- cycle;
\draw (A1) -- (A2) -- (C1) -- cycle;
\draw (A4) -- (A1) -- (C1) -- cycle;
\draw [fill opacity=0.7,fill=green!80!blue] (A2) -- (A3) -- (B1) -- cycle;
\draw [fill opacity=0.7,fill=orange!80!black] (A3) -- (A4) -- (B1) -- cycle;
\draw [fill opacity=0.7,fill=green!30!black] (A2) -- (A3) -- (C1) -- cycle;
\draw [fill opacity=0.7,fill=purple!70!black] (A3) -- (A4) -- (C1) -- cycle;
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
1. Políedre

El volum depèn del tipus de políedre
\stopframedtext
     \eTD
     \bTD \starttikzpicture
% http://www.texample.net/tikz/examples/cuboid/ CC-BY 3.0
%%% Edit the following coordinate to change the shape of your
	%%% orthoedre
      
	%% Vanishing points for perspective handling
	\coordinate (P1) at (-7cm,1.5cm); % left vanishing point (To pick)
	\coordinate (P2) at (8cm,1.5cm); % right vanishing point (To pick)

	%% (A1) and (A2) defines the 2 central points of the cuboid
	\coordinate (A1) at (0em,0cm); % central top point (To pick)
	\coordinate (A2) at (0em,-2cm); % central bottom point (To pick)

	%% (A3) to (A8) are computed given a unique parameter (or 2) .8
	% You can vary .8 from 0 to 1 to change perspective on left side
	\coordinate (A3) at ($(P1)!.8!(A2)$); % To pick for perspective 
	\coordinate (A4) at ($(P1)!.8!(A1)$);

	% You can vary .8 from 0 to 1 to change perspective on right side
	\coordinate (A7) at ($(P2)!.7!(A2)$);
	\coordinate (A8) at ($(P2)!.7!(A1)$);

	%% Automatically compute the last 2 points with intersections
	\coordinate (A5) at
	  (intersection cs: first line={(A8) -- (P1)},
			    second line={(A4) -- (P2)});
	\coordinate (A6) at
	  (intersection cs: first line={(A7) -- (P1)}, 
			    second line={(A3) -- (P2)});

	%%% Depending of what you want to display, you can comment/edit
	%%% the following lines

	%% Possibly draw back faces

	\fill[blue!90] (A2) -- (A3) -- (A6) -- (A7) -- cycle; % face 6
	
	\fill[blue!50] (A3) -- (A4) -- (A5) -- (A6) -- cycle; % face 3
	
	\fill[blue!30] (A5) -- (A6) -- (A7) -- (A8) -- cycle; % face 4
	
	\draw[dashed] (A5) -- (A6);
	\draw[dashed] (A3) -- (A6);
	\draw[dashed] (A7) -- (A6);

	%% Possibly draw front faces
	\fill[blue!50,opacity=0.2] (A1) -- (A2) -- (A3) -- (A4) -- cycle; % f2
	\fill[blue!90,opacity=0.2] (A1) -- (A4) -- (A5) -- (A8) -- cycle; % f5

	%% Possibly draw front lines
	\draw (A1) -- (A2);
	\draw (A3) -- (A4);
	\draw (A7) -- (A8);
	\draw (A1) -- (A4);
	\draw (A1) -- (A8);
	\draw (A2) -- (A3);
	\draw (A2) -- (A7);
	\draw (A4) -- (A5);
	\draw (A8) -- (A5);
	
	% Possibly draw points
	% (it can help you understand the cuboid structure)
%	\foreach \i in {1,2,...,8}
%	{
%	  \draw[fill=black] (A\i) circle (0.15em)
%	    node[above right] {\small \i};
%	}

\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
2. Ortoedre
\startformula
\startalign
\NC A \NC = A_L + 2\cdot A_B\NR
\NC V \NC = A_B \cdot h
\stopalign
\stopformula
\stopframedtext
     \eTD
     \bTD \starttikzpicture[line join=bevel,z=-5.5,scale=1.7]
\coordinate (A1) at (0.44,0,3.44);
\coordinate (A2) at (2,0,3.16);
\coordinate (A3) at (2.75,0,4.56);
\coordinate (A4) at (1.65,0,5.7);
\coordinate (A5) at (0.22,0,5.01);
\coordinate (B1) at (0.44,-1,3.44);
\coordinate (B2) at (2,-1,3.16);
\coordinate (B3) at (2.75,-1,4.56);
\coordinate (B4) at (1.65,-1,5.7);
\coordinate (B5) at (0.22,-1,5.01);

\draw (A1) -- (A2) -- (A3) -- (A4) -- (A5) -- cycle;
\draw (B1) -- (B2) -- (B3) -- (B4) -- (B5) -- cycle;
\draw (A1) -- (B1);
\draw (A2) -- (B2);
\draw (A3) -- (B3);
\draw (A4) -- (B4);
\draw (A5) -- (B5);
\draw [fill opacity=0.7,fill=green!20] (A1) -- (A2) -- (B2) -- (B1)-- cycle;
\draw [fill opacity=0.7,fill=orange!20] (A2) -- (A3) -- (B3) -- (B2)-- cycle;
\draw [fill opacity=0.7,fill=purple!20] (A3) -- (A4) -- (B4) -- (B3)-- cycle;
\draw [fill opacity=0.7,fill=red!20] (A4) -- (A5) -- (B5) -- (B4)-- cycle;
\draw [fill opacity=0.7,fill=yellow!20] (A5) -- (A1) -- (B1) -- (B5)-- cycle;
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
3. Prisme
\startformula
\startalign
\NC A \NC = A_L + 2\cdot A_B\NR
\NC V \NC = A_B \cdot h
\stopalign
\stopformula
\stopframedtext
     \eTD
   \eTR
   \bTR
     \bTD \starttikzpicture[scale=.75, z={(.707,.3)}]
    \draw (2,3,2) -- (0,0,0) -- (4,0,0) -- (4,0,4) -- (2,3,2) 
      -- (4,0,0);
    \draw[color=gray, style=dashed] (2,3,2) -- (0,0,4) 
      -- (0,0,0);
    \draw[color=gray, style=dashed] (0,0,4) -- (4,0,4);
    \draw (4.6,-.2,2) node{ };
    \draw[|-|] (5.5,3,2) -- node[right] {$h$} (5.5,0,2);

    % spacer
    \draw (0,-1,0) node {};

\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
4. Piràmide
\startformula
\startalign
\NC A \NC = A_B + A_L\NR
\NC V \NC = \frac{A_B \cdot h}{3}
\stopalign
\stopformula
% http://www.nilesjohnson.net/tikz-demo.html
\stopframedtext
     \eTD
     \bTD \starttikzpicture[scale=0.70]
\fill[top color=blue!50!black,bottom color=blue!10,middle color=gray,shading=axis,opacity=0.25] (0,0) circle (2cm and 0.5cm);
\fill[left color=blue!50!black,right color=blue!50!black,middle color=gray!50,shading=axis,opacity=0.25] (2,0) -- (2,6) arc (360:180:2cm and 0.5cm) -- (-2,0) arc (180:360:2cm and 0.5cm);
\fill[top color=blue!90!,bottom color=blue!2,middle color=blue!30,shading=axis,opacity=0.25] (0,6) circle (2cm and 0.5cm);
\draw (-2,6) -- (-2,0) arc (180:360:2cm and 0.5cm) -- (2,6) ++ (-2,0) circle (2cm and 0.5cm);
\draw[loosely dashed] (-2,0) arc (180:0:2cm and 0.5cm);
\draw (0,0) -- (2cm,0);
\draw (1cm,0) node[anchor=north] {$r$};
\draw (2cm,3cm) node[anchor=west] {$h$};
\stoptikzpicture

\startframedtext[width=broad,align=middle,frame=off]
5. Cilindre
\startformula
\startalign
\NC A \NC = A_L + 2\cdot A_B \NR
\NC   \NC = 2 \cdot \pi \cdot r \cdot h + 2 \cdot \pi \cdot r^2\NR
\NC V \NC = A_B \cdot h = \pi \cdot r^2 \cdot h
\stopalign
\stopformula
% http://tex.stackexchange.com/questions/42812/3d-bodies-in-tikz
% http://www.texample.net/tikz/examples/dandelin-spheres/
\stopframedtext
     \eTD
     \bTD
     \starttikzpicture[scale=0.8]
\fill[top color=blue!50!black,bottom color=blue!10,middle color=gray,shading=axis,opacity=0.25] (0,0) circle (2cm and 0.5cm);
\fill[left color=blue!50!black,right color=blue!50!black,middle color=gray!50,shading=axis,opacity=0.25] (2,0) -- (0,6) -- (-2,0) arc (180:360:2cm and 0.5cm);
\draw (-2,0) arc (180:360:2cm and 0.5cm) -- (0,6) -- cycle;
\draw[loosely dashed] (-2,0) arc (180:0:2cm and 0.5cm);
\draw (0,0) -- (2cm,0);
\draw (1cm,0) node[anchor=north] {$r$};
\draw (1cm,3) node[anchor=west] {$g$};
\stoptikzpicture
% http://tex.stackexchange.com/questions/42812/3d-bodies-in-tikz

\startframedtext[width=broad,align=middle,frame=off]
6. Con
\startformula
\startalign
\NC A \NC = A_L + A_B \NR
\NC \NC = \pi \cdot g \cdot r + \pi \cdot r^2 \NR
\NC V \NC = \frac{A_B \cdot h}{3} = \frac{\pi \cdot r^2 \cdot h}{3}
\stopalign
\stopformula
\stopframedtext
     \eTD
   \eTR
   \bTR
     \bTD[nr=3]
     \starttikzpicture[scale=2]
%\filldraw[ball color=white] (0,0) circle (3);
   \draw (-1,0) arc (180:360:1cm and 0.5cm);
    \draw[loosely dashed] (-1,0) arc (180:0:1cm and 0.5cm);
    \draw (0,1) arc (90:270:0.5cm and 1cm);
    \draw[loosely dashed] (0,1) arc (90:-90:0.5cm and 1cm);
    \draw (0,0) circle (1cm);
    \shade[ball color=blue!20!white,opacity=0.30] (0,0) circle (1cm);
   \draw (0,0) -- (1cm,0);
   \draw (0.7cm,0) node[anchor=north] {$r$};
\stoptikzpicture
% http://www.texample.net/tikz/examples/map-projections/

\startframedtext[width=broad,align=middle,frame=off]
7. Esfera

\startformula
A = 4 \cdot \pi \cdot r^2
\stopformula

\startformula
V = \frac{4}{3} \cdot \pi \cdot r^3
\stopformula
\stopframedtext
     \eTD
   \eTR
 \eTABLE}

Nota: si desenvolupem el con, $A_L$ és l'àrea d'un sector circular de longitud $2\pi r$ i radi $g$.


