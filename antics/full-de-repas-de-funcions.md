---
author: Xavier Bordoy
created: '20200430140818433'
date: '2019-05-29'
keywords:
- "activitats"
- "conceptes: funcions"
- "conceptes: representació gràfica"
lang: ca
license: CC-BY 4.0
modified: '2020-04-30T14:16:52.017'
subtitle: Exercicis rutinaris
title: Full de repàs de funcions
type: text/x-markdown
---

# Preguntes


Digueu quan les funcions següents donen lloc a rectes o a corbes. Justifiqueu la resposta:

a. $y = 3x$
a. $y = 0.1x$
a. $y = -7$
a. $y = -2x - 1$
a. $y = -x -1$
a. $y = \sqrt(x)$
a. $y = \frac{2x}{3}$
a. $y = \frac{5}{4x}$
a. $2x - y = 2y + 3x$
a. $2x - y = 2y + 2x$
a. $4x + 7y = y - x^2 + 5x$
a. $2x^2 + y = 2x^2 - 7$
a. $2x - 5y = y + 20$
a. $y = 9x - \frac{x}{3}$
a. $y = 2x + \frac{3}{5}$

# Solucions

a. recta ($a=3, b=0$)
a. recta ($a = 0.1, b = 0$)
a. recta ($a=0, b=-7$)
a. recta ($a = -2, b = -1$)
a. recta ($a=-1, b=-1$)
a. corba
a. recta ($a=\frac{2}{3}$)
a. corba
a. recta ($a = \frac{1}{3}$)
a. recta ($a=0, b=0$)
a. corba
a. recta ($a=0, b=-7$)
a. recta ($a=\frac{2}{6}, b=\frac{20}{6}$)
a. recta ($a=\frac{28}{3}, b=0$)
a. recta ($a=2, b=\frac{3}{5}$)
