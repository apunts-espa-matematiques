%% carregam les opcions d'entorn
\environment plantilla-context-entorn-simple-estructurals
\environment plantilla-context-entorn-simple-visuals
\environment plantilla-context-entorn-simple-visuals-taules
\environment plantilla-context-entorn-simple-estructurals-xml


%% Capçaleres i peus
\setupheadertexts[]
\setupfootertexts[{\em \getmarking[sectionnumber] \getmarking[section]}][{\userpagenumber}]

\setupheader[text][style={\sc},color={headingcolor},after={}]
\setupfooter[text][style={\sc},color={headingcolor},before={}] %\hrule

%% Mòduls
\usemodule[units]
\usemodule[tikz]
\usetikzlibrary[decorations.markings]
\usetikzlibrary[calc,intersections,through,backgrounds,arrows, patterns, shapes.geometric,fadings,decorations.pathreplacing,shadings,shapes.geometric]
\usemodule[pgfmath]


%% Tipus llibre
\definestructureconversionset [frontpart:pagenumber] [] [romannumerals]
\setupmakeup[standard][page=yes, doublesided=no, pagestart=yes]
\setuppagenumbering [location=,alternative=doublesided]

%% Pos les Fitxes al TOC
\setupcombinedlist[content][list={chapter,section,subsection,Fitxa},interaction=all]

%% Per a què se mostrin els peus de pàgina a la teoria http://www.ntg.nl/pipermail/ntg-context/2014/078198.html
%\automigrateinserts 

%% Indentació
\setupindenting[yes,medium,next]


% Colors
\definecolor[zzttqq][r=0.6,g=0.2,b=0.]
\definecolor[qqzzcc][r=0.,g=0.6,b=0.8]
\definecolor[qqzzqq][r=0.,g=0.6,b=0.]
\definecolor[cqcqcq][r=0.7529411764705882,g=0.7529411764705882,b=0.7529411764705882]


%% el text

\starttext

\startfrontmatter

% portada
\startstandardmakeup

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\startalignment[middle]

\startcolor[middleblue]
{\switchtobodyfont[20pt] \ss Exercicis i alguns apunts teòrics de Matemàtiques per les classes d'ESPA}

\bigskip
\thinrule


\bigskip
{\sc\switchtobodyfont[14pt] Nivell ESPA 3}\par
\stopcolor
\stopalignment

\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\startalignment[flushright]
{\switchtobodyfont[18pt] \ss Xavier Bordoy}
\stopalignment

\vfill
\bigskip
\bigskip
\bigskip
\bigskip
\bigskip
\

\stopstandardmakeup
\page[yes]


\startstandardmakeup

{\small
\startalignment[flushleft]
Xavier Bordoy

Professor de Matemàtiques

Centre actual: CEPA Sud (Campos, Illes Balears)

Correu electrònic: {\tt \goto{somenxavier@gmail.com}[url(somenxavier@gmail.com)]}
\stopalignment

\vfill
{\char"00A9} {\currentdate[year]} Xavier Bordoy. Excepte quan s'indiqui el contrari, aquesta obra està subjecta a la llicència \quotation{Reconeixement 4.0 Internacional de Creative Commons} (CC-BY 4.0). Això vol dir, {\em essencialment}, que podeu copiar, modificar i distribuir qualsevol part de l'obra com vulgueu, sempre que en citeu la font de manera explí­cita, d'acord amb els termes de la llicència. Per veure una còpia de la llicència, visiteu {\tt \goto{http://creativecommons.org/licenses/by/4.0/}[url(http://creativecommons.org/licenses/by/4.0/deed.ca)]}.

\bigskip
\startalignment[middle]
{\bTABLE[frame=off]
\bTR \bTD \externalfigure[plantilla-cc-individual-gros.eps] \eTD \bTD \externalfigure[plantilla-cc-by-individual-gros.eps] \eTD \eTR
\eTABLE}
\stopalignment


\bigskip
\bigskip
\bigskip
Mathematics Subject Classification (2010):
97-01, 97A10


\bigskip
Aquest document està pensat per fer classes d'Educació Secundària de Persones Adultes (ESPA) de les Illes Balears amb el currículum establert l'any 2009 (\goto{entrada 17698 del BOIB 117 de 11 d'agost de 2009}[url(http://boib.caib.es/pdf/2009117/mp4.pdf)]) amb una metodologia magistral o que impliqui, eventualment, una reiteració d'exercicis rutinaris o l'aplicació de tècniques de resolució de problemes específiques.

El document ha estat mecanografiat. Encara que s'hagi revisat diverses vegades és possible que hi hagi errors --- el més probable de tipus tipogràfic o gramatical. Si en detecteu algun, si us plau, aviseu-me per correu electrònic. D'altra banda, si adapteu o modifiqueu aquesta obra i considereu que el canvi ha estat per millorar-la, us agraïria que m'ho communiquéssiu i, si el canvi és del meu gust, l'incorporaré a l'obra original en els mateixos termes de la llicència.

Aquest document ha estat generat, \currentdate[weekday,{ },day,{ },month,{ },year] a les \currenttime, usant el programari \goto{\Context}[url(http://wiki.contextgarden.net)] (versió \contextversionnumber), \goto{\texenginename}[url(http://www.luatex.org)] (versió \texengineversion) i \goto{Ti{\em k}Z}[url(http://sourceforge.net/projects/pgf/)] sota un entorn \goto{Linux}[url(https://www.archlinux.org)].

La informació obtinguda en aquest document pot no ser suficientment precisa. En aquest sentit, l'autor no assumeix cap responsabilitat ni obligació legal per cap error o omissió que pugui haver fet. 

}
\stopstandardmakeup

\page[yes]
\completecontent


\page[yes]


\page[yes]

\section{Materials aliens}

Els continguts següents no són propis i, per tant, es distribueixen amb les seves corresponents llicències i autories:

\startitemize
\item L'exercici \in[activitat-intuicio-regles-Weinberg] està fortament inspirat en la reflexió de n'Ewan Weinberg. \quotation{An Easy Transformation}. {\char"00A9} 2015 Ewan Weinberg. Disponible a \goto{\hyphenatedurl{http://evanweinberg.com/2015/10/16/an-easy-transformation/}}[url(http://evanweinberg.com/2015/10/16/an-easy-transformation/)].

\item L'exercici \in[exer:real-1] està inspirat en \goto{\quotation{A12 Fencing}}[url(http://map.mathshell.org/materials/tasks.php?taskid=369)] de MathShell, disponible a \goto{\hyphenatedurl{http://map.mathshell.org/materials/tasks.php?taskid=369}}[url(http://map.mathshell.org/materials/tasks.php?taskid=369)]. {\char"00A9} 2012 The MAP Summative Assessment Tasks. El material està disponible sota llicència \quotation{Reconeixement-NoComercial-SenseObraDerivada 3.0 No adaptada} (CC-BY-NC-ND 3.0).

\item L'exercici \in[exercici:tawny-ports] està extret de l'entrada \goto{\quotation{Real-World Math That Isn't Real To Students}}[url(http://blog.mrmeyer.com/2013/real-world-math-that-isnt-real-to-students/)] del blog d'en Dan Meyer, disponible a \goto{\hyphenatedurl{http://blog.mrmeyer.com/2013/real-world-math-that-isnt-real-to-students/}}[url(http://blog.mrmeyer.com/2013/real-world-math-that-isnt-real-to-students/)]. {\char"00A9} Dan Meyer. El material es distribuïa sota la llicència \quotation{Reconeixement 3.0 Estats Units d'Amèrica} (CC BY 3.0 US). Ara es distribueix sota la llicència \quotation{Reconeixement 4.0 Internacional} (CC-BY 4.0).

\item L'exercici \in[exercici:autoescola-Ramírez] està extret de l'exercici 12 del tema \quotation{Funciones y gráficas} del llibre \quotation{Llibre de text de 3r d'ESO} (pàgina 227). Editorial Anaya que es pot consultar a la pàgina del \goto{departament de Matemàtiques de l'IES Arroyo}[url(http://www.juntadeandalucia.es/averroes/iesarroyo/matematicas/matematicas.htm)], disponible a \goto{\hyphenatedurl{http://www.juntadeandalucia.es/averroes/iesarroyo/matematicas/matematicas.htm}}[url(http://www.juntadeandalucia.es/averroes/iesarroyo/matematicas/matematicas.htm)]. {\char"00A9} Anaya. 

\stopitemize

\bigskip
L'ús dels materials aliens que mantenen tots els drets de d'autor es realitza acollint-se al dret de cita i ressenya per a fins docents amparat per l'article 32.2 de la Llei de Propietat Intel·lectual (Real Decreto Legislativo 1/1996, de 12 d'abril de 1996. \goto{Entrada 8930}[url(http://boe.es/boe/dias/1996/04/22/pdfs/A14369-14396.pdf)] del BOE 97, de 22 d'abril de 1996).


\vfill
\space
 
\page[yes]

\section{Continguts oficials}

Els següents punts són els continguts oficials que s'han d'impartir en els diversos temes que conformen el temari del nivell d'ESPA 3 (\goto{BOIB 117 de 11 d'agost de 2009}[url(http://boib.caib.es/pdf/2009117/mp4.pdf)]). La classificació és meva.

\subsubject{Conjunts numèrics i càlcul}

\subsubject{Àlgebra}

\subsubject{Representació gràfica}

\subsubject{Estadística}

\stopfrontmatter

\startbodymatter

\input material-apunts-ESPA-3-Conjunts-numerics-i-calcul.tex
\page[yes]
\input material-apunts-ESPA-3-Algebra.tex
\page[yes]
\input material-apunts-ESPA-3-Representacio-grafica.tex
\page[yes]
\input material-apunts-ESPA-3-Estadistica-descriptiva-unidimensional.tex
\stopbodymatter

%\startappendices
%\stopappendices

%\startbackmatter
%\stopbackmatter



\stoptext
