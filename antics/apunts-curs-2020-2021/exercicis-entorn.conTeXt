%% Carregam entorns genèrics

\environment ../../../plantilles/plantilla-context-entorn-simple-estructurals
\environment ../../../plantilles/plantilla-context-entorn-simple-visuals
\environment ../../../plantilles/plantilla-context-entorn-simple-visuals-taules


%=Mòduls necessaris
\usemodule[units]
\usemodule[tikz]
\usetikzlibrary[decorations.markings]
\usetikzlibrary[calc,intersections,through,backgrounds,arrows, shapes.geometric,fadings,decorations.pathreplacing,shadings,shapes.geometric,patterns]
\usemodule[pgfmath]

%= Partició de mots
\sethyphenatedurlnormal{:=?&/.-_}
\sethyphenatedurlbefore{?&}
\sethyphenatedurlafter {:=/.-_}

%=Tipus llibre
\definestructureconversionset [frontpart:pagenumber] [] [romannumerals]
\setupmakeup[standard][page=yes, doublesided=no, pagestart=yes]

%=Indentació
\setupindenting[yes,medium,next]

%= Colors
%== Per als gràfics del Geogebra
\definecolor[uuuuuu][r=0.27,g=0.27,b=0.27]
\definecolor[cqcqcq][r=0.75,g=0.75,b=0.75]
\definecolor[qqqqff][r=0.,g=0.,b=1.]
\definecolor[cqcqcq][r=0.752941176471,g=0.752941176471,b=0.752941176471]
\definecolor[zzttqq][r=0.6,g=0.2,b=0.]
\definecolor[qqccqq][r=0.,g=0.8,b=0.]
\definecolor[qqwwzz][r=0.,g=0.4,b=0.6]
\definecolor[xdxdff][r=0.49019607843137253,g=0.49019607843137253,b=1.]
\definecolor[qqwuqq][r=0.,g=0.39215686274509803,b=0.]
\definecolor[qqzzqq][r=0.,g=0.6,b=0.]


% Redefineix l'exercici
\defineenumeration
  [exercici]
  [alternative=serried,before={\blank[medium]\testpage[1]},text={Exercici},stopper={.\space},width=fit,headstyle=\ss,distance=0.25em,width=fit,headcolor=exercici_color,title=yes,titlestyle=\ss,left={\bgroup\bf},right={\egroup}, after={\blank[big]}]

%= Document
%== Font del document 
\setupbodyfont [cmr,11pt] % pagella o cmr, 11pt

%== Nombre de pàgina
\setuppagenumbering [location=,alternative=doublesided]

%% Capçaleres i peus
\setupheadertexts[]
\setupfootertexts
        [{\getmarking[subject]}][{\userpagenumber}]
        [{\userpagenumber}][{\currentdate}]

% era \getmarking[chapter]

\setupheader[text][style={\ss},color={headingcolor},after={}]
\setupfooter[text][style={\sc},color={headingcolor},before={}] %\hrule

\setuphead[title][style={\scd},align=middle,before={\begingroup},after={\blank[3cm]\bigskip\endgroup},page=no, alternative=middle]
\setuphead[chapter][style={\scd},align=middle,before={\page[yes]\begingroup},after={\blank[3cm]\bigskip\endgroup},page=no, alternative=middle]

%\setuphead[section][page=yes] % or page=right
\setupheads[section,subject][style={\ssc}, alternative=margin]
\setupheads[subsubject,subsection][style={\ssb}, alternative=margin]
\setupheads[subsubsubject,subsubsection][style={\ssa}, alternative=margin]
\setuphead[title,chapter,subject,section,subsubject,subsection][color=headingcolor]

% Hyphenation
\setbreakpoints[compound]


%% Incloc les fitxes a l'índex de continguts
\setupcombinedlist[content][list={chapter,section,subsection,Fitxa},interaction=all]



