\startmode[espa2a]

\section[seccio:mesclats]{Proporcionalitat numèrica: directa, inversa i composta}

\startexercici[exer:exer-e-50] En Carles té una feina on li paguen per hores. Per 3 hores de feina ha cobrat 18 euros:
\startitemize[a,packed]
\item Quant cobrarà si treballa 7 hores?
\item La setmana passada va cobrar 45 euros. Quantes hores va treballar?
\stopitemize

\stopexercici

\startexercici Cinc viatjants arriben a un refugi de muntanya, on hi ha un cartell que posa \quotation{El menjar que hi ha basta per 7 viatjants i 4 setmanes}. Fins quant de temps es poden quedar al refugi?
\stopexercici

\startexercici En un quiosc, sabem que 5 caramels costen 5,75 euros. \startitemize[a,text] \item Si volem endur-nos-en 160 caramels, quant ens costaran? \item Si només tenguéssim 5 euros, quants caramels ens hem podríem dur?\stopitemize
\stopexercici

\startexercici Segons les estadístiques 2 de cada 5 persones tenen càries. Si són 360 les persones enquestades, quantes tenen càries?
\stopexercici

\startexercici Durant l'estiu dues-centes persones consumeixen 500 litres d'aigua per dia. Quants litres consumiran 60 persones?
\stopexercici

\startexercici Pintar 50 $\text{m}^2$ de paret costa 500 euros. Quant costa pintar 420 $\text{m}^2$? Quants metres podrem pintar si només tenim 264 euros?
\stopexercici

\startexercici La tripulació d'un vaixell menja 250 kg en un mes. Quants quilograms menjaran en 80 dies? Si només disposen de 100 kg de menjar quantes persones podrien viatjar al vaixell durant un mes?
\stopexercici

\startexercici Tres retoladors costen 4,80. Quant costen 5 retoladors?
\stopexercici

\startexercici Si cada dues hores plou 70 litres, quant haurà plogut en tot el dia?
\stopexercici

\startexercici[exer:exer-e-51] Un tren que circula a 100 km/h tarda 5 hores a arribar a una ciutat. A quina velocitat circula un altre tren que tarda 6 hores i quart a fer el mateix recorregut?
\stopexercici

\startexercici Si un pintor ha pintat 75 $\text{m}^2$ de paret amb 126 kg de pintura:
\startitemize[a,packed]
\item Quanta pintura hauria necessitat per pintar 300 $\text{m}^2$ de paret?
\item Amb 50 kg, quina superfície de paret pot pintar?
\stopitemize
\stopexercici

\startexercici[exer:exer-f-1] Un constructor vol repartir 1000 € entre tres dels seus empleats de manera directament proporcional a l'antiguitat a l'empresa. L'empleat A fa 9 anys que fa feina a l'empresa, i els empleats B i C, en fa 3 anys. Quina part li correspon a cadascú?
\stopexercici

\startexercici Dues màquines funcionant 6 hores diàries consumeixen 1500 KWh en un dia. Quant consumirien 3 màquines funcionant 8 hores diàries?
\stopexercici

\startexercici En una pastisseria, sabem que 5 pastissets costen 15,55 euros. \startitemize[a, text] \item Si volem endur-nos-en 80 pastissets, quant ens costaran? \item Si només tenguéssim 25 euros, quants pastissets ens podríem endur?\stopitemize
\stopexercici

\startexercici A la fruiteria del cap de cantó hi ha 10 taronges per a cada 4 plàtans. Quantes taronges hi hauria en 100 plàtans?
\stopexercici

\startexercici Es creu que per construir la piràmide de Keops hi varen fer feina 20.000 persones durant 10 hores diàries i varen tardar 20 anys a acabar-la. Què haguessin tardat si, amb el mateix ritme de treball, haguessin fet feina 10.000 persones més fent feina 8 hores diàries?
\stopexercici


\startexercici Un 5 \% d'alumnes tenen la grip al gener. Quants alumnes tendran la grip a una classe de 70 persones?
\stopexercici

\startexercici A un institut, 63 alumnes, que són el 15 \% del total, duen ulleres. Quants d'alumnes té l'institut?
\stopexercici

\startexercici En encalentir una barra de metall d'1 m a $200 \Degrees \Celsius$, aquesta s'ha dilatat fins a mesurar 1,04 metres. Una barra de 60 cm d'un altre metall, en encalentir-la a la mateixa temperatura, s'ha dilatat fins a fer 61,9 cm. Quin metall s'ha dilatat menys en proporció?
\stopexercici

\startexercici En Jaume posa els seus estalvis a un dipòsit al 3,5 \% a 10 anys.
\startitemize[a,packed]
\item Si disposa de 10.500 €, quin interès li hauran fet aquests doblers al final del període?
\item Si decidís treure els diners abans d'hora, li cobrarien una comissió del 4\% respecte del capital inicial. Al cap de 4 anys, necessita treure els diners. Perdrà o guanyarà doblers?
\stopitemize
\stopexercici

\startexercici Quatre persones tarden vint minuts a pintar una paret. Quant minuts tardaran deu persones?
\stopexercici

\startexercici Si dotze pitreres de pollastre valen 4,90 euros:
\startitemize[a,packed]
\item Què valdran 26 pitreres?
\item Quantes pitreres podrem comprar amb 50 euros?
\stopitemize
\stopexercici


\startexercici Si amb 7 litres de gasoil puc fer 250 km, quants quilòmetres podré fer amb 10 litres? Quants litres de gasoil consumirà aquest cotxe si feim un recorregut de 789 km?
\stopexercici


\startexercici Si tardem 1 hora i 20 minuts per anar del punt $A$ al punt $B$ a una velocitat constant de 80 km/h. A quant haurem d'anar per només tardar 50 minuts?
\stopexercici


\startexercici Tres socis aporten 10.000, 20.000 i 50.000 euros respectivament per crear una empresa. Al llarg de l'any, l'empresa té uns beneficis de 200.000 euros. Què li toca a cadascú?
\stopexercici


\startexercici Quatre amics van a comprar-se gominoles. Aporten respectivament 1, 5, 7 i 11 euros. Si 12 gominoles costes 2,5 euros, quantes gominoles li toca a cadascú?
\stopexercici


\startexercici Set persones tarden 20 minuts a pintar $4 \Square \Meter$ de paret. Què tardaran 8 persones per pintar $10 \Square \Meter$ de paret? Quantes persones necessitarem per pintar $100 \Square \Meter$ de paret en 2 hores?
\stopexercici

\startexercici En la compra d'un abric de 80 euros, ens fan un descompte del 25\%. I després ens afegeixen l'IVA. Què pagarem finalment?
\stopexercici


\startexercici Quatre obrers han fet 8 marjades de pedra treballant durant 8 hores al dia. Quantes hores diàries haurien de treballar 6 obrers per fer-ne 10?
\stopexercici

\startexercici En un grup de 80 persones, 24 persones són dones. Quin tant per cent suposen les dones sobre el total del grup?
\stopexercici

\startexercici El 80\% dels empleats d'una empresa són no fumadors. Si els no fumadors de l'empresa són en total 200, quants empleats hi ha a l'empresa (entre fumadors i no fumadors)?
\stopexercici

\startexercici El professor de Català demana a tres alumnes fer un treball d'en Pompeu Fabra de 80 pàgines de manera que faci menys pàgines de treball aquell que tengui una nota major a l'anterior examen. Si les notes de l'anterior examen han estat de: 4, 6 i 8 punts, quantes pàgines li toca fer a cadascú?
\stopexercici

\startexercici A les darreres eleccions, el 40\% de les persones votà el partit $A$, el 20\% el partit $B$ i la resta el partit $C$. Quantes persones votaran cada partit si en total hi ha haver 40.000 vots?
\stopexercici

\startexercici En un grup de joves matemàtics sabem que:
\startitemize[a,packed]
\item El 25\% del grup són homes
\item El 40\% del grup és de menys de 22 anys
\stopitemize
Si hi ha 200 dones en aquest grup, troba:
\startitemize[a,packed]
\item quants homes hi ha al grup
\item quants menors de 22 anys hi ha al grup
\item quina quantitat de persones hi ha al grup
\stopitemize
\stopexercici

\startexercici Tenim un dipòsit i 4 aixetes damunt seu que estan totes tancades. Si obrim 3 de les 4 aixetes, el dipòsit tarda 39 minuts a omplir-se. Quant tardaria a omplir-se si tenguéssim obertes 2 aixetes?
\stopexercici

\startexercici Tres amics aporten 10, 25 i 80 euros per fer una travessa. Si els toquen 25000 euros, què li toca a cadascú?
\stopexercici

\startexercici Tardem 25 minuts a anar a la feina, anant a 6 km/h. Si volem arribar amb només 10 minuta, a quants quilòmetres per hora hem d'anar?
\stopexercici

\startexercici[exer:exer-e-52] Necessitem 4 metres de tela per fer una cortina. Si 2,5 m de tela costen 48 euros, quant haurem de pagar per la tela?
\stopexercici

\startexercici Tres socis compren 250, 400 i 5050 accions d'una companyia. Al cap d'un any, aquesta reparteix dividends de 12 euros per cada 25 accions. Què guanya cada accionista?
\stopexercici

\startexercici[exer:exer-e-53] Quinze persones fan el muntatge d'uns plafons solars en tres setmanes.
\startitemize[a,packed]
\item Quant tardarien 35 persones a fer aquest mateix muntatge?
\item Si volem muntar-lo en només 15 dies, quantes persones necessitarem?
\stopitemize
\stopexercici

\startexercici En una pastisseria, sabem que 5 pastissets costen 15,55 euros.
\startitemize[a,packed]
\item Si volem endur-nos-en 80 pastissets, quant ens costaran?
\item Si només tenguéssim 25 euros, quants pastissets ens podríem endur?
\stopitemize
\stopexercici

\startexercici Tres amics van al Go Karts!. Per problemes monetaris es veuen obligats a llogar un sol cotxe entre els tres. Paguen 2, 3 i 10 euros respectivament. Si sabem que amb 15 euros poden colcar 20 minuts, quants minuts manarà cadascú el cotxe?
\stopexercici

\startexercici Tres excavadores tarden quatre setmanes en fer un clot.
\startitemize[a,packed]
\item Quants dies tardaran dues excavadores?
\item Si volem fer el clot només en 10 dies, quantes excavadores hem de menester?
\stopitemize
\stopexercici


\startexercici En una quiosc, sabem que 5 caramels costen 5,75 euros.
\startitemize[a,packed]
\item Si volem endur-nos-en 80 caramels, quant ens costaran?
\item Si només tenguéssim 5 euros, quants caramels ens podríem endur?
\stopitemize
\stopexercici


\startexercici Tres amics van a un cibercafè. No tenen doblers per llogar cadascú un ordinador, per tant decideixen llogar-ne un entre els tres. Es treuen tot el que tenen a les butxaques i aporten: 1,20 euros, 2,50 euros i 80 cèntims. Si sabem que una hora de connexió costa 6 euros, quants minuts utilitzarà l'ordinador cadascú?
\stopexercici


\startexercici Un tren que va a 80 km/h tarda 40 minuts en arribar al seu destí
\startitemize[a]
\item Quants minuts tardaria si anàs a 50 km/h?
\item Si volguéssim arribar només en 15 minuts, a quants quilòmetres per hora hauria d'anar el tren?
\stopitemize
\stopexercici

\startexercici En Jaume ha cobrat 200 € per 25 hores de feina. Quant cobrarà na Marta que ha fet 30 hores de feina?
\stopexercici

\startexercici Al Congrés dels diputats, que té 350 escons, hi havia l'any 2008\footnote{Font: \href{ca.wikipedia.org/Eleccions generals espanyoles de 2008}{http://ca.wikipedia.org/wiki/Eleccions_generals_espanyoles_de_2008}}: 169 diputats del PSOE, 154 diputats del PP i la resta d'altres grups. Quin tant per cent d'escons té cada partit polític a la Cambra?
\stopexercici

\startexercici En cada cas, digues quina relació hi ha entre les dues magnituds:
\startitemize[a,packed]
\item La longitud del costat d'un quadrat i el seu perímetre
\item En nombre de gallines en una granja i els quilograms de pinso que mengen
\item La velocitat a que va un cotxe i el temps que tarda per anar del punt $A$ al punt $B$
\stopitemize
\stopexercici

\startexercici[exer:exer-e-54] Dos germans en Vicenç i na Coloma, obren una llibreta al banc. Cada mes, en Vicenç hi posa 40 euros i na Coloma 80 euros. Al cap d'alguns anys, tenen a la llibreta 3360 euros. Com els han de repartir? Què li toca a cadascun?
\stopexercici

\startexercici[exer:exer-e-55] Quatre socis decideixen comprar accions de borsa. Aporten 100, 500, 1000 i 5000 euros, respectivament. Després d'un any, obtenen uns beneficis de 25 000 euros. Què obté cadascú?
\stopexercici

\startexercici El 28\% dels espanyols té carnet de moto. Si a Espanya hi ha 40 000 000 persones, ¿quants espanyols tenen carnet de moto?
\stopexercici

\startexercici A una ciutat, el 40\% de la població té cotxe. D'aquests, el 20\% té un tot-terreny. I d'aquests un 10\% s'ha comprat el cotxe en aquest darrer any. Si a la ciutat hi ha 20000 persones, quanta gent s'ha comprat un tot-terreny aquest darrer any?
\stopexercici

\startexercici A un gimnàs, que té 250 socis, el 20\% del socis van a la classe d'aeròbic, el 30\% fan peses i la resta fan spinning. Quanta gent hi ha a cada classe?
\stopexercici

\startexercici Dins una empresa que té 280 empleats, hi ha 10 càrrecs directius, 54 vice-directors, i la resta és personal administratiu. Quin tant per cent suposa cada grup en el total dels empleats de l'empresa?
\stopexercici

\startexercici A l'Estat espanyol hi havia l'any 2008, 15.683.433 de dones fèrtils\footnote{Font: \href{en.wikipedia.org/Demographics of Spain}{http://en.wikipedia.org/wiki/Demographics_of_Spain}, prenent com a dones fèrtils aquelles les edats de les quals estan compreses entre 16 i 64 anys.}. Si suposem que el 40\% d'aquestes va tenir un o més fills, i d'aquestes, un 10\% va tenir un part múltiple. I d'aquestes, un 1\% va tenir més de 4 fills. Quantes dones varen tenir  més de 4 fills a Espanya l'any 2008?
\stopexercici

\page[yes]
\starttextrule{Solucions de \about[seccio:mesclats]}

\startitemize[n,packed]
\item Exercici~\in[exer:exer-e-50]: \startitemize[a,text] \item 42 € \item 7.5 h \stopitemize
\item Exercici~\in[exer:exer-e-51]: 80 km/h
\item Exercici~\in[exer:exer-f-1]: 200 €, 200 € i 600 €.
\item Exercici~\in[exer:exer-e-52]: 76,80 €
\item Exercici~\in[exer:exer-e-53]: \startitemize[a,text] \item 9 dies, \item 21 persones \stopitemize
\item Exercici~\in[exer:exer-e-54]: 2240 € a na Coloma, i 1120 € a en Vicenç
\item Exercici~\in[exer:exer-e-55]: \startitemize[a,text] \item 378,78 €, \item 1893,93 €, \item 3787,87 €, \item 18939,39 € \stopitemize
\stopitemize
\stoptextrule

\stopmode