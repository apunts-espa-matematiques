\startsection[reference=seccio:problemes-de-teorema-de-pitagores, title={Problemes del teorema de Pitàgores}]

\startsubject[title={Preguntes}]

\startexercici[reference=exer:sense-nom-4] Trobeu la diagonal d'una porteria de futbol de 2,44 \unit{meter} d'alt i 7,32 \unit{meter} de llarg.
\stopexercici

\startexercici[reference=exer:propi-cames-modificat] Una persona obri les cames formant un angle recte. Quina distància separarà els seus peus

\blank[big]
Dades necessàries: la longitud de les cames és de 1,2 \unit{meter} 
\stopexercici

\startexercici[reference=exer:talaiot-18] En una urbanització s'han protegit 310 finestres quadrades de 1,26 \unit{meter} de costat amb una cinta adhesiva especial, com es veu a la figura~\in[fig:finestra-cinta]. Quants metres de cinta s'han fet servir?

\startplacefigure[location=here, reference=fig:finestra-cinta, title={Esquema de la cinta adhesiva}]
\bTABLE[frame=off,align={middle,lohi}]
\bTR
\bTD
\externalfigure[figs/geometria-teorema-de-pitagores-ESPAD-1.png][scale=1000]
\eTD
\eTR
\eTABLE
\stopplacefigure
\stopexercici

\startexercici[reference=exer:talaiot-19] Una escala de 3,7 \unit{meter} de longitud es troba recolzada en una paret, quedant el peu a 1,5 \unit{meter} d'ella. A quina altura arriba l'escala sobre la paret?
\stopexercici

\startexercici[reference=exer:talaiot-33] Un futbolista entrena corrent la diagonal del terreny de joc d'un camp de futbol, anada i tornada, $30$ cops tots els dies (figura~\in[fig:camp-futbol]). Quina distància total recorre? El terreny de joc té unes mides de $105 \times 67 \unit{meter}$.

\startplacefigure[location=here, reference=fig:camp-futbol, title={Camp de futbol}]
\bTABLE[frame=off,align={middle,lohi}]
\bTR
\bTD
\externalfigure[figs/geometria-teorema-de-pitagores-ESPAD-2.png][scale=2000]
\eTD
\eTR
\eTABLE
\stopplacefigure
\stopexercici


\startexercici[reference=exer:propi-antic-4] En una habitació que fa $10 \times 50$ metres, volem anar de cantó a cantó. Calculeu quina distància recorrerem.
\stopexercici

\startexercici[reference=exer:propi-antic-5] Un edifici té una altura de 80 metres i nosaltres esteim a una distància de 25 metres. Calculeu la distància que hi ha des del punt més alt de l'edifici a on esteim
\stopexercici

\startexercici[reference=exer:sense-nom-8] Des d'una torre de fusta $20 \unit{meter}$ d'alçada es vol muntar una tirolina al terra. Si volem que la base de la tirolina estigui a $100 \unit{meter}$ de la torre, de quina llargària necessitem la corda de la tirolina?
\stopexercici

\startexercici[reference=exer:sense-nom-9] Amb un punter làser sabem que la distància des d'un observador a l'extrem superior d'un arbre és de $20 \unit{meter}$. Si sabem que l'observador està a $7 \unit{meter}$ de l'arbre, calculeu l'altura de l'arbre
\stopexercici

\startexercici[reference=exer:sense-nom-10] Volem mesurar l'altura de la muntanya $B$. Les dades que sabem és que \startitemize[a,text] \item La muntanya $A$ fa $200 \unit{meter}$ \item la distància entre els cims és de $500 \unit{meter}$ i \item el desplaçament horitzontal entre les muntanyes és de $100 \unit{meter}$\stopitemize (vegeu figura~\in[fig:muntanyes])
\startplacefigure[location=force, reference=fig:muntanyes, title={Diversos triangles}]
 \bTABLE[frame=off,align={middle,lohi}]
  \bTR
    \bTD
      \externalfigure[figs/geometria-teorema-de-pitagores-muntanyes.eps][scale=600]
    \eTD
  \eTR
 \eTABLE
\stopplacefigure
\stopexercici

\startexercici[reference=exercici:inspirat-per-Santillana-1] Dues persones surten al mateix temps des del mateix punt: una en bicicleta en direcció nord i l'altra a peu en direcció est a $20 \unit{kilo meter per hour}$ i $5 \unit{kilo meter per hour}$, respectivament. Calculeu quina distància els separarà al llarg de 6 hores. I al cap de 10 hores.
\stopexercici

\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exer:sense-nom-4]} La diagonal és de $7,71 \unit{meter}$
\sym{\in[exer:propi-cames-modificat]} Aproximadament $1,70 \unit{meter}$
\sym{\in[exer:talaiot-18]} Necessitarem $551,8 \unit{meter}$ de cinta
\sym{\in[exer:talaiot-19]} L'escala arriba als $3,38 \unit{meter}$ d'altura
\sym{\in[exer:talaiot-33]} El futbolista recorre $7473 \unit{meter}$
\sym{\in[exer:propi-antic-4]} Recorrerem $50,99 \unit{meter}$
\sym{\in[exer:propi-antic-5]} La distància és de $83,81 \unit{meter}$
\sym{\in[exer:sense-nom-8]} La distància és $101,98 \unit{meter}$
\sym{\in[exer:sense-nom-9]} L'altura de l'arbre fa $18,73 \unit{meter}$
\sym{\in[exer:sense-nom-10]} La muntanya $B$ té una alçada de $689,89 \unit{meter}$
\sym{\in[exercici:inspirat-per-Santillana-1]} \startitemize[a, text] \item La distància que separa les persones al cap de 6 hores és de $123,69 \unit{kilo meter}$. \item I al cap de 10 hores és de $206,15 \unit{kilo meter}$ \stopitemize
\stopitemize

\stopsubject

\stopsection
