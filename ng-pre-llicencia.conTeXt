\startstandardmakeup

{\char"00A9} 2021-\currentdate[year] Xavier Bordoy. Alguns drets reservats. Llevat que s'hi indiqui el contrari (vegeu a la pàgina~\at[seccio:copyrights-externs]), aquesta obra està subjecta a la llicència \quotation{Reconeixement-CompartirIgual 4.0 Internacional de Creative Commons} (CC-BY-SA 4.0). Per veure una còpia de la llicència, visiteu {\tt \goto{https://creativecommons.org/licenses/by-sa/4.0/deed.en}[url(https://creativecommons.org/licenses/by-sa/4.0/deed.en)]}.

\godown[1cm]
\startalignment[middle]
\bTABLE[frame=off]
  \bTR \bTD \externalfigure[figs/cc-by-sa.eps][scale=900] \eTD \eTR
\eTABLE
\stopalignment
\godown[1cm]

\blank[big]
\noindent{Mathematics Subject Classification (2020): 00-01, 00A06, 00A07}

\blank[1cm]
Aquest document està pensat per practicar els exercicis procedimentals del currículum de l'Educació Secundària de Persones Adultes de les Illes Balears \cite{curriculum-espa}.

\blank[medium]
La versió del document és la \directlua{
   tex.sprint(os.resultof("cat VERSIO.txt"))}.\currentdate[year,-, mm,-,dd] (revisió \goto{\directlua{
   tex.sprint(math.floor(os.resultof("git log --pretty=oneline | wc -l")+1))}a}[url(https://repo.or.cz/apunts-espa-matematiques.git/shortlog/HEAD)]). Podeu trobar el codi font a la web del projecte: \goto{https://repo.or.cz/apunts-espa-matematiques.git}[url(https://repo.or.cz/apunts-espa-matematiques.git)].
\vfill
Xavier Bordoy

Professor de Matemàtiques de Secundària

Palma, Illes Balears

Correu electrònic: {\tt \goto{somenxavier@posteo.net}[url(somenxavier@posteo.net)]}

Mastodon: {\tt \goto{somenxavier@mathstodon.xyz}[url(https://mathstodon.xyz/@somenxavier)]}

\stopstandardmakeup
