\startsection[reference=seccio:propietats-potencies, title={Propietats de les potències}]

\startsubject[title={Preguntes}]

\startexercici Completeu:
\startitemize[a, columns]
\item $\frac{8^{-3}}{8^{-10}} = 8^{\framed{   }}$
\item $(8^4)^8 = 8^{\framed{   }} = 2^{\framed{   }}$
\item $5^3 \cdot 5^{-5} = 5^{\framed{   }}$
\item $\frac{4^{-6}}{4^7} = 2^{\framed{   }}$
\item $\left(5^8 \cdot 4^{-8}\right)^{-11} = 5^{\framed{   }} \cdot 4^{\framed{   }}$
\item $\left(7^{-4} \cdot 9^{4}\right)^{-10} = \left(\frac{9}{7}\right)^{\inframed{   }}$
\stopitemize
\stopexercici


\startexercici Si es pot, reduïu aquestes expressions a una sola potència:
\startitemize[a, columns, three]
\item $5^4 \cdot 5^3 \cdot 5^7$
\item $7^4 \cdot 7^{-2} \cdot 7^3$
\item $4^2 \cdot 4^{-4} \cdot 4^{-1}$
\column
\item $6^4 \cdot 6^2 \colon 6^3$
\item $8^2 \cdot 8^{-1} \colon 8^5$
\item $3^{-4} \cdot 3^5 \cdot 3^7$
\column
\item $3^{10} \colon 3^5 \colon 3^2$
\item $5^{-3} \colon 5^4 \cdot 5^8$\
\item $9^2 \colon 9^{-4} \colon 9^5$
\stopitemize
\stopexercici

\startexercici Expresseu com una sola potència:
\startitemize[a, columns, three]
\item $5^2 \cdot 3^2$
\item $4^{14} \cdot 2^{14}$
\item $7^6 \cdot 3^6$
\item $45^2 \colon 9^2$
\item $25^7 \colon 5^7$
\item $63^3 \colon 7^3$
\item ${\left(4^3\right)}^4$
\item ${\left(5^7\right)}^2$
\item ${\left(21^5\right)}^8$
\item ${\left(5^2\right)}^{-3}$
\item ${\left(6^{-4}\right)}^7$
\item ${\left(3^{-1}\right)}^{-2}$
\stopitemize
\stopexercici

\startexercici[reference=exer:exercici-17] Reduïu a una sola potència:
\startitemize[a, columns]
\item \snappedmath{\left(5 \cdot 5^3\right)^2 \colon 5^{-2}}
\item \snappedmath{2^3 \cdot {\left(2^{-3} \colon 2 \cdot 2^7\right)}^3 \cdot \left(2^{11} \colon 2^4 \colon 2^3\right)^{-4}}
\item \snappedmath{(-3)^4 \colon (-3)^{-3} \cdot \left((-3)^9\right)^{-2}}
\item \snappedmath{\left(\left((-2)^3\right)^5\right)^0}
\item \snappedmath{\left(a^5 \colon a^3\right)^2 \cdot \left(a^{-7} \colon a^{-2}\right)^4}
\item \snappedmath{3 \cdot \left(3^2\right)^2 \cdot \left(3^3\right)^3 \cdot (3^4)^4}
\item \snappedmath{3^5 \colon \left( \left(3^{23} \colon 3^{17}\right)^2 \cdot 3^7\right)}
\item \snappedmath{5^3 \cdot 5^{-6} \colon 5^{-9}}
\item \snappedmath{\left(\frac{2}{3}\right)^{-3} \cdot 2^3 \colon 3^{5}}
\item \snappedmath{(-2)^4 \cdot 4^2 \colon (-8)^2}
\stopitemize
\stopexercici 

\startexercici[reference=exer:exercici-18] Reduïu a una sola potència:
\startitemize[a,columns]
\item \startformula \frac{\left(5^3 \cdot 5^2\right)^3}{5^4 \colon 5^2} \stopformula
\item \startformula \frac{4^3 \cdot 4^3 \cdot 4^7}{4^9 \cdot 4^{-11} \colon 4^{15}} \cdot 4^3 \stopformula
\item \startformula \frac{\left(4^{23} \cdot 4^{14}\right)^2}{4^{25} \colon 4^{23}} \stopformula
\item \startformula \left(\frac{2^3 \colon 2^5}{2}\right)^3 \cdot 2^{-5} \colon 2^3 \cdot 2 \stopformula
\item \startformula\left(2^{3} \cdot 2 \colon 2^{11}\right)^{-3} \colon \left(2^{-15} \colon 2^{11}\right)\stopformula
\item \startformula\left(6^7 \cdot \frac{6^3 \cdot 6^8}{6^{-3}} \right)^{-2} \cdot 6^2\stopformula
\item \startformula(0,5^2)^3 \cdot 0,5^3 \colon (0,5^8 \colon 0,5^4)\stopformula
\item \startformula\left(2^{-3} \cdot 2^{-8} \colon 2^{10}\right) \left((2^3)^6 \colon 2\right)\stopformula
\item \startformula\left(\frac{5}{3}\right)^{-4} \colon 5^{-6} \cdot 3^4\stopformula
\stopitemize
\stopexercici 

\startexercici[reference=exer:exercici-21] Simplifiqueu fins a obtenir una sola potència:
\startitemize[a, columns, three]
\item \startformula
\frac{2^{-6} \cdot (2^2)^{-4}}{2^{10} \colon 2^{-3}}
\stopformula

\item \startformula
\frac{(x^8 \cdot x^3 \colon x)^3}{(x^{10} \colon x^2)^3}
\stopformula

\item \startformula
\frac{5^3 \cdot (5^3)^{-2} \cdot 5^2}{(5^2)^4 \cdot 5^3 \cdot 5^{20}}
\stopformula
\stopitemize
\stopexercici

\startexercici[reference=exer:exercici-19] Completeu:
\startitemize[a,columns]
\item $(3^6 \colon 3^{-4})^3 \cdot 3^{\text{\framed{\ \ }}} = 3^{17}$
\item $(3^6 \cdot (3^2)^3)^4 \colon 3^{40} \cdot 3^{\text{\framed{\ \ }}} = 3^{-10}$
\item $(2^{\text{\framed{\ \ }}})^{-2} \colon 2^6 = 2^{34}$
\item $(3^{\text{\framed{\ \ }}})^{-3} \cdot 3^3 \colon 3 = 3^{-31}$
\item $(3^8 \cdot 3^3)^{\text{\framed{\ \ }}} \; \; \colon 3^{-2} = 3^{13}$
\item $(5^2 \cdot 5^3)^{\text{\framed{\ \ }}} \; \cdot (5^2)^3 = 5^{105}$
\stopitemize
\stopexercici 

\startexercici[reference=exer:exercici-16] Simplifiqueu (potser sigui convenient factoritzar els nombres més grans):
\startitemize[a, columns]
\item $5^{-3} \colon 5^4$

\item $\left(2^{3}\right)^{-2} \cdot 2^{-3}$

\item $\displaystyle \frac{\left(4\right)^2 \cdot 2^3}{2^{-2}}$

\item $\displaystyle \left(\frac{5}{2}\right)^{-3} \cdot \left(\frac{2}{5}\right)^4$

\item $\displaystyle \frac{\frac{2}{5} \cdot \left(\frac{2}{5}\right)^{-2}}{\left(\frac{2}{5}\right)^2}$

\item $\displaystyle \frac{4^2 \cdot 2^3 \colon 2^{-1}}{2^{-3} \cdot 8} $

\item $\displaystyle \frac{8^3 \cdot 2^3}{4^2} \cdot \left(\frac{1}{2}\right)^{-5}$
\stopitemize
\stopexercici

\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exer:exercici-17]} \startitemize[a,text] \item $5^{10}$ \item $2^{-4}$ \item $(-3)^{-11}$ \item 1 \item $a^{-16}$ \item $3^{30}$ \item $1$ \item $5^6$ \item $2^{-1} \cdot 3^{-2}$ \item $-2^2$ \stopitemize

\sym{\in[exer:exercici-18]} \startitemize[a,text] \item $5^{13}$ \item $4^{33}$ \item $4^{72}$ \item $2^{-16}$ \item $2^{47}$ \item $6^{-40}$ \item $0,5^5$ \item $2^{-4}$ \item $5^{-10} \cdot 3^8$  \stopitemize

\sym{\in[exer:exercici-21]} \startitemize[a,text] \item $2^{-27}$ \item $x^6$ \item $5^{-32}$ \stopitemize

\sym{\in[exer:exercici-19]} \startitemize[a,text] \item $-13$ \item $-18$ \item $-20$ \item $11$ \item $1$ \item $20$ \stopitemize

\sym{\in[exer:exercici-16]} \startitemize[a,text] \item $5^{-7}$ \item $2^{-9}$ \item $2^9$ \item $\left(\frac{2}{5}\right)^7$ \item $(\frac{5}{2})^3$ \item $2^8$ \item $2^{13}$ \stopitemize
\stopitemize

\stopsubject

\stopsection
