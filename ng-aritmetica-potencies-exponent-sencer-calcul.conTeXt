\startsection[reference=seccio:potencies-exponent-sencer, title={Potències d'exponent sencer}]

\startsubject[title={Preguntes}]

\startexercici[reference=exer:exercici-1] Calculeu les potències següents:

\startitemize[a,columns,four]
\item \snappedmath{5^3}
\item \snappedmath{\left(\frac{2}{3}\right)^4}
\item \snappedmath{254^0}
\column
\item \snappedmath{\left(\frac{-8}{12}\right)^3}
\item \snappedmath{\left(-3\right)^5}
\item \snappedmath{\left(-50\right)^5}
\column
\item \snappedmath{\left(-10\right)^7}
\item \snappedmath{0,2^3}
\item \snappedmath{\left(-0,22\right)^4}
\column
\item \snappedmath{\left(\frac{3}{2}\right)^4}
\item \snappedmath{22^1}
\item \snappedmath{22^0}
\stopitemize


\stopexercici

\startexercici[reference=exer:exercici-2] Es poden descompondre els nombres següents en forma potencial? En cas afirmatiu, trobeu les seves descomposicions en forma de potència:
\startitemize[a,columns,three,packed]
\item 64
\item 16
\column
\item 81
\item 420
\column
\item 576
\item 1296
\stopitemize
\stopexercici


\startexercici[reference=exer:exercici-3] Digueu quines de les expressions següents són iguals:
\startitemize[A]
\item \startitemize[a,text]
\item $2^3$
\item $-2^3$
\item $(-2)^3$
\item $-(-2)^3$
\stopitemize
\item \startitemize[a,text]
\item $3^0$
\item $-3^4$
\item $(-3)^4$
\item $-(-3)^0$
\item $(-3)^0$
\stopitemize
\item \startitemize[a,text]
\item $9^3$
\item $3^9$
\item $(-3)^3$
\item $-(-3)^3$
\item $-(-9)^3$
\stopitemize
\stopitemize
\stopexercici

\startexercici[reference=exer:exercici-4] Expresseu en forma de potència:

\startitemize[a, columns, three, text]
\item \startformula \frac{3}{5} \cdot \frac{3}{5} \cdot \frac{3}{5} \stopformula
\item \startformula 2 \cdot 3 \cdot 2 \cdot 3 \cdot 2 \cdot 3 \stopformula
\item \startformula (2\cdot a) \cdot (2 \cdot a) \cdot (2 \cdot a) \stopformula
\item \startformula 3 \cdot b \cdot 3 \cdot b \cdot 3 \stopformula
\item \startformula 16 \stopformula
\item \startformula 100000 \stopformula
\item \startformula (-3) \cdot (-3) \cdot (-3) \cdot (-3) \stopformula
\stopitemize
\stopexercici

\startexercici[reference=exer:exercici-5] Calculeu les potències següents i digueu quantes operacions feim:
\startitemize[a,columns, four]
\item $4^3$
\item $5^6$
\item $2^3$
\item $10^6$
\stopitemize
\stopexercici

\startexercici[reference=exer:exercici-6] Ordeneu de menor a major els nombres següents:
\startitemize[a,text]
\item $-2^3$
\item $2^3$
\item $3^2$
\item $-3^2$
\item $(-3)^2$
\stopitemize
\stopexercici

\startexercici[reference=exer:exercici-8] Ordeneu de menor a major les expressions següents, utilitzant els símbols $<$ i $=$ segons convengui
\startitemize[A]
\item \startitemize[a,text]
\item $10^4$
\item $-10^4$
\item $10^0$
\item $0,1^{10}$
\stopitemize
\item \startitemize[a,text]
\item $\left(\frac{1}{2}\right)^2$
\item $\left(\frac{-3}{4}\right)^4$
\item $\left(\frac{-4}{-6}\right)^2$
\item $5^1$
\item $\frac{2^2}{3}$
\stopitemize
\item \startitemize[a,text]
\item $3^2$
\item $(-5)^2$
\item $-5^2$
\item $(-3)^3$
\item $\left(\frac{1}{3}\right)^3$
\stopitemize
\stopitemize
\stopexercici

\subsection{Potències d'exponent negatiu}

\startexercici Calculeu:
\startitemize[a,columns,three]
\item $2^{-3}$
\item $\left(\frac{2}{3}\right)^{-3}$
\item $(0,1)^{-3}$
\item $\left(\frac{-4}{6}\right)^2$
\column
\item $5^{-4}$
\item $\left(\frac{2}{5}\right)^{-4}$
\item $5^{-2}$
\item $\left(\frac{5}{3}\right)^{-3}$
\column
\item $100^{-2}$
\item $\left(\frac{1}{2}\right)^{-4}$
\item $(-3)^{-2}$
\item $10^{-4}$
\stopitemize
\stopexercici

\startexercici Expresseu aquestes fraccions com a potències, a ser possible d'exponent negatiu:
\startitemize[a,columns,three]
\item $\frac{1}{100}$
\item $\frac{1}{8}$
\column
\item $\frac{1}{2^4}$
\item $\frac{1}{a^5}$
\column
\item $\frac{125}{27}$
\item $\frac{16}{32}$
\stopitemize
\stopexercici

\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exer:exercici-1]} \startitemize[a,text] \item $125$, \item $16/81$, \item $1$, \item $-512/1728 = 8/27$, \item $-243$, \item $-3125 \cdot 10^5$, \item $-10^7$, \item $0,008$, \item $0,00234256$, \item $81/16$, \item $22$, \item $1$  \stopitemize

\sym{\in[exer:exercici-2]} \startitemize[a,text] \item $2^6$, \item $2^4$, \item $3^4$, $9^2$, \item No es pot posar en forma potencial ($420 = 2^2 \cdot 3 \cdot 5 \cdot 7$), \item Factoritzant en nombres primers, tenim que $520 = 2^6 \cdot 3^2 = \left(2^3\right)^2 \cdot 3^2 = \left(2^3 \cdot 3\right)^2 = 24^2$, \item Factoritzant en nombres primers, obtenim $2^4 \cdot 3^4 = 6^4$\stopitemize

\sym{\in[exer:exercici-3]} \startitemize[a,text] \item  $a=d$, $b=c$, \item $a=e$, \item $a=e$\stopitemize

\sym{\in[exer:exercici-4]} \startitemize[a,text] \item $\left( \frac{3}{5} \right)^3$, \item $2^3 \cdot 3^3$, \item $(2 \cdot a)^3$, \item $3^3 \cdot b^2$, \item $2^4$, $4^2$, \item $10^5$\stopitemize

\sym{\in[exer:exercici-5]} \startitemize[a,text] \item 2 operacions: $4 \cdot 4 \cdot 4 = 64$, \item 4 operacions: $5 \cdot 5 \cdot 5 \cdot 5 \cdot 5 \cdot 5 = (5 \cdot 5 \cdot 5) (5 \cdot 5 \cdot 5) = (25 \cdot 5) (25 \cdot 5) = (125) (125) = 15.625$ \item 2 operacions: $(2 \cdot 2) \cdot = 4 \cdot 2 = 8$\item 4 operacions\stopitemize

\sym{\in[exer:exercici-6]} \startitemize[a,text] \item $-2^3 = -8$, \item $2^3 = 8$, \item $3^2 = 9$, \item $-3^2 = -9$, \item $(-3)^2 = +9$.\stopitemize Per tant, $d < a < b < c = e$

\sym{\in[exer:exercici-8]} \startitemize[A] \item $b < d < c < a$, \item $a < c < e < d < b$, ja que \startitemize[a,text] \item $\left(\frac{1}{2}\right)^2 = \frac{1}{4}$ \item $\left(\frac{-3}{4}\right)^4 = \frac{81}{4} = 20,25$ \item $\left(\frac{-4}{-6}\right)^2 = \frac{4}{9} \simeq 0,\overparent{4}$ \item $5^1 = 5$ \item $\frac{2^2}{3} = \frac{4}{3}$\stopitemize  \item $d < c < e < a < b$, ja que \startitemize[a,text] \item $3^2 = 9$, \item $(-5)^2 = 25$, \item $-5^2 = -25$, \item $(-3)^2 = -27$, \item $\left(\frac{1}{3}\right)^3 = \frac{1}{27}$ \stopitemize \stopitemize
\stopitemize

\stopsubject

\stopsection
