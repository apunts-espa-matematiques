\startsection[reference=seccio:funcio-afi-representacio-grafica, title={Representació de funcions afins}]

\startsubject[title={Preguntes}]

\startsubsubject[title={Representació gràfica en general}]

Abans de representar pròpiament ls funcions afins, convé representar funcions en general. Recordeu que per a cada funció heu de fer la taula de valors corresponents i representar els punts obtinguts en el pla cartesià (vegeu la secció~\about[seccio:pla-cartesia]).

\blank[big]
\startexercici[reference=exer:funcio-afi-representacio-1] Representeu gràficament:
\startitemize[a,columns,three]
\item $y = 3x -9$
\item $y = 2x+1$
\item $y = 10/x$
\item $y = x^2 - 2$
\item $y = 60/(x+1)$
\item $y = x - 2$
\item $y = x^2 - x$
\item $y = 3x + 6$
\item $y= \sqrt{x} + 2$
\item $y = x^2 -3x +1$
\item $y = x/2 + 1$
\item $y = 5-x$
\stopitemize
\stopexercici

\startexercici[reference=exer:funcio-afi-representacio-2] Representeu gràficament les funcions següents:
\startitemize[a,columns,three]
\item $2x^2 + 4y = 12$
\item $8x - 5y = 10$
\item $x \cdot y = -30$
\item $3x - 6y = 3$
\item $2x + 8y = 9$
\item $2x^2 - y = 8$
\stopitemize
\stopexercici

\startexercici[reference=exer:funcio-afi-representacio-3] Quines de les funcions següents donen lloc a rectes i quines no:
\startitemize[a,columns, packed]
\item \startformula y = 2x- 4 \stopformula
\item \startformula y = 2x \stopformula
\item \startformula y = 2 \stopformula
\item \startformula y = -x - 2 \stopformula
\item \startformula y = x^2 \stopformula
\item \startformula y= \frac{x}{2} \stopformula
\item \startformula y = 0,5 x \stopformula
\item \startformula y = \frac{x}{x} + 1 \stopformula
\item \startformula y = \frac{2x^2}{x} + 1 \stopformula
\item \startformula y = \frac{5}{x} -3 \stopformula
\item \startformula y = x^3 -3 \stopformula
\item \startformula 3x - 5y = 2x + 2 \stopformula
\item \startformula 6x - 10y +x^2 = 2y + x^2 \stopformula
\item \startformula x = 2y - 3 \stopformula
\item \startformula 2y + x = 3y -3x \stopformula
\stopitemize

Podeu esbrinar quin tipus de funció dóna lloc a rectes i quines no? Quin aspecte té la fórmula corresponent?
\stopexercici

\startexercici[reference=exer:funcio-afi-representacio-8, title={rectes o corbes}] Digueu quan les funcions següents donen lloc a rectes o a corbes. Justifiqueu la resposta:

\startitemize[a, columns]
\item \startformula y = 3x \stopformula
\item \startformula y = 0.1x \stopformula
\item \startformula y = -7 \stopformula
\item \startformula y = -2x - 1 \stopformula
\item \startformula y = -x -1 \stopformula
\item \startformula y = \sqrt(x) \stopformula
\item \startformula y = \frac{2x}{3} \stopformula
\item \startformula y = \frac{5}{4x} \stopformula
\item \startformula 2x - y = 2y + 3x \stopformula
\item \startformula 2x - y = 2y + 2x \stopformula
\item \startformula 4x + 7y = y - x^2 + 5x \stopformula
\item \startformula 2x^2 + y = 2x^2 - 7 \stopformula
\item \startformula 2x - 5y = y + 20 \stopformula
\item \startformula y = 9x - \frac{x}{3} \stopformula
\item \startformula y = 2x + \frac{3}{5} \stopformula
\stopitemize
\stopexercici

\stopsubsubject

\startsubsubject[title={Representació de funcions afins i significat geomètric dels paràmetres}]

Una vegada sabem que les funcions afins donen lloc a rectes i vice-versa, podem representar més fàcilment les funcions afins.

\blank[big]
\startexercici[reference=exer:funcio-afi-representa-4pre-1] Representeu gràficament aquestes funcions:
\startitemize[a, columns, three]
\item $y=3x$
\item $y=-2x-1$
\item $y=0,1x$
\item $y=-x-1$
\item $y=-7$
\item $y=\frac{5x}{4}$
\item $y=x$
\item $y=\frac{2x}{3}$
\item $y=9x - 3$
\item $y=2x+5$
\item $y=2x$
\stopitemize
\stopexercici

\startexercici[reference=exer:funcio-afi-representa-4pre-2] Representeu gràficament aquestes funcions:
\startitemize[a, columns, three]
\item $y=2x-4$
\item $y=2x$
\item $y=2$
\item $y=\frac{x}{2}$
\item $y=0,5x$
\item $y=x+1$
\item $y=2x+1$
\item $y=2x-3$
\item $y=2x+3$
\item $y=3x+3$
\item $y=-3x+3$
\item $y=-x+3$
\item $y=x+3$
\stopitemize
\stopexercici

\blank[1cm]
Una vegada que sabem la forma de les funcions afins, podem estudiar el significat gràfic dels seus paràmetres.

\blank[big]
\startexercici[reference=exer:saber-creixement-rectes, title={creixement/decreixement}] Digueu si les gràfiques corresponents a les funcions següents són creixents o decreixents. Com ho sabeu?
\startitemize[a,columns,three]
\item \startformula y = 2x + 4  \stopformula
\item \startformula y = -2x + 4  \stopformula
\item \startformula y = 2x-4  \stopformula
\item \startformula y = -2x - 4  \stopformula
\item \startformula y = -2  \stopformula
\item \startformula y = -2x  \stopformula
\item \startformula y = -4  \stopformula
\item \startformula y = \frac{x}{3} + 2  \stopformula
\item \startformula y = \frac{x}{3} - \frac{2}{5} \stopformula
\stopitemize
\stopexercici

\startexercici[reference=exer:funcio-afi-representacio-7, title={pendent i creixement}] Quina fórmula dóna lloc a una recta amb major pendent? Per què? Quina creix i quina decreix?
\startitemize[a]
\item \startitemize[a, text] \item $y = 2x + 3$, \item $y = 4x+3$ \stopitemize
\item \startitemize[a, text] \item $y = 5x+10$, \item $y = 5x+20$ \stopitemize
\item \startitemize[a, text] \item $y = -5x + 12$, \item $y = 5x + 12$ \stopitemize
\item \startitemize[a, text] \item $y = 20x + 100$, \item $y = 20x$, \item $y = -20x$, \item $y = 10x + 200$ \stopitemize
\stopitemize
\stopexercici


\startexercici[reference=exer:funcio-afi-representacio-5, title={emparellament}] Identifiqueu el gràfic amb la fórmula corresponent: \startitemize[a, text] 
\item $y = x + 2$
\item $y = 2x + 4$
\item $y = 2x$
\item $y = -x + 2$
\stopitemize
Digueu el motiu d'aquesta identificació.

Gràfiques:

\placetable[split,force,none]
[taula]
{gràfiques}
{
\bTABLE[frame=off,align=middle,width=fit,split=yes]
   \bTR
     \bTD
	% y = 2x +4: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1cm,ystep=1cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,1,2,3,4,5,6}
	   \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[anchor=north] {\tfx $\x$};
	\draw[->,color=black] (0.0,-3.2) -- (0.0,8.4);
	\foreach \y in {-3,-2,-1,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[anchor=east] {\tfx $\y$};
	
	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-6:6, color=red, very thick] plot(\x,{(--4.0--2.0*\x)/1.0});
	\stoptikzpicture
	
	a
      \eTD
     \bTD[width=0.5cm]
     \eTD
     \bTD
	% y = x +2: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,1,2,3,4,5,6}
	   \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0.0,-3.2) -- (0.0,8.4);
	\foreach \y in {-3,-2,-1,1,2,3,4,5,6,7,8}
	   \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-6:6, color=blue, very thick] plot(\x,{(--2.0--1.0*\x)/1.0});
	\stoptikzpicture
	
	b
     \eTD
   \eTR
   \bTR
     \bTD
	% y = 2: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1cm,ystep=1cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,1,2,3,4,5,6}
	  \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0,-3.2) -- (0,8.4);
	\foreach \y in {-3,-2,-1,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-7:6, color=green, very thick] plot(\x,{(2.0)/1.0});
	\stoptikzpicture
	
	c
      \eTD
     \bTD[width=0.5cm]
     \eTD
     \bTD
	% y = 2x: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,1,2,3,4,5,6}
	  \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0.0,-3.2) -- (0.0,8.4);
	\foreach \y in {-3,-2,-1,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-6:6, color=orange, very thick] plot(\x,{(2.0*\x)/1.0});
	\stoptikzpicture
	
	d
     \eTD
   \eTR
   \bTR
     \bTD
	% y = -x +2: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1cm,ystep=1cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,1,2,3,4,5,6}
	  \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0,-3.2) -- (0,8.4);
	\foreach \y in {-3,-2,-1,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-6:6, color=red, very thick] plot(\x,{(--2.0-1.0*\x)/1.0});
	\stoptikzpicture

	e
      \eTD
     \bTD[width=0.5cm]
     \eTD
     \bTD
	% y = -x: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,1,2,3,4,5,6}
	  \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0.0,-3.2) -- (0.0,8.4);
	\foreach \y in {-3,-2,-1,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-7:6, color=blue, very thick] plot(\x,{(-1.0*\x)/1.0});
	\stoptikzpicture
	
	f
     \eTD
   \eTR
\eTABLE}

Quines fórmules tenen els gràfics que no estan emparellats amb cap fórmula anterior?
\stopexercici

\startexercici[reference=exer:funcio-afi-representacio-6, title={emparellament}] Identifiqueu el gràfic amb la seva fórmula:

Fórmules:
\startitemize[a, columns, four]
\item $y = x$
\item $y = 2x$
\item $y = 5$
\item $y = -x + 1$
\stopitemize

Gràfiques:

\placetable[split,force,none]
[taula]
{gràfiques}
{
\bTABLE[frame=off,align=middle,width=fit,split=yes]
   \bTR
     \bTD
	% y = x: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1cm,ystep=1cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,-1,1,2,3,4,5,6}
	  \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0,-3.2) -- (0,8.4);
	\foreach \y in {-3,-2,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-6:6, color=blue, very thick] plot(\x,{(\x)/1.0});
	\stoptikzpicture
	
	a
      \eTD
     \bTD[width=0.5cm]
     \eTD
     \bTD
	% y = 5: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,-1,1,2,3,4,5,6}
	  \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0,-3.2) -- (0,8.4);
	\foreach \y in {-3,-2,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-7:6, color=orange, very thick] plot(\x,{(5.0)/1.0});
	\stoptikzpicture
	
	b
     \eTD
   \eTR
   \bTR
     \bTD
	% y = 2x: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1cm,ystep=1cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0);
	\foreach \x in {-6,-5,-4,-3,-2,-1,1,2,3,4,5,6}
	  \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0,-3.2) -- (0,8.4);
	\foreach \y in {-3,-2,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-7:6, color=green, very thick] plot(\x,{(2.0*\x)/1.0});
	\stoptikzpicture
	
	c
      \eTD
     \bTD[width=0.5cm]
     \eTD
     \bTD
	% y = -x+1: Geogebra modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm,scale=0.5]
	\draw [color=gray,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-7,-3) grid (6,8);
	\draw[->,color=black] (-6.9,0) -- (6.1,0.0);
	\foreach \x in {-6,-5,-4,-3,-2,-1,1,2,3,4,5,6}
	  \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0,-3.2) -- (0,8.4);
	\foreach \y in {-3,-2,1,2,3,4,5,6,7,8}
	  \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};

	\clip(-7,-3) rectangle (6,8);
	\draw [domain=-7:6, color=red, very thick] plot(\x,{(-1.0*\x+1.0)/1.0});
	\stoptikzpicture
	
	d
     \eTD
   \eTR
\eTABLE}


\stopexercici

\stopsubsubject


\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exer:funcio-afi-representacio-8]} \startitemize[a, text] \item recta ($a=3, b=0$), \item recta ($a = 0.1, b = 0$), \item recta ($a=0, b=-7$), \item recta ($a = -2, b = -1$), \item recta ($a=-1, b=-1$), \item corba, \item recta ($a=\frac{2}{3}$), \item corba, \item recta ($a = \frac{1}{3}$), \item recta ($a=0, b=0$), \item corba, \item recta ($a=0, b=-7$), \item recta ($a=\frac{2}{6}, b=\frac{20}{6}$), \item recta ($a=\frac{28}{3}, b=0$), \item recta ($a=2, b=\frac{3}{5}$)) \stopitemize

\stopitemize

\stopsubject

\stopsection
