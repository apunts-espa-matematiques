\startmode[espa4a]

\section{Interpretació d'un gràfic}

\startactivitat{{\em Interpretant gràfiques de distància-temps}}. Activitat en grup. (vegi's~\goto{el fitxer específic de l'activitat}[url(02-activitat-gràfics-distància-temps/01-interpretació-distància-temps.pdf)]).
\stopactivitat

\startexercici[exercici:globus]
Es molla un globus que s'eleva i, a l'assolir certa altura, rebenta. La gràfica següent representa l'altura, amb el pas del temps, en la que es troba el globus fins que rebenta.

\placefigure
  [force,none]
  [fig:globus-rebenta]
  {Recorregut d'un globus}
  {\bTABLE[frame=off,align={middle,lohi}]
\bTR
\bTD
\starttikzpicture
    \draw[help lines,color=gray!20,step=0.5] (-0.1,-0.1) grid (7.2,6);
    \draw[->] (-0.2,0) -- (7.2,0);
    \draw (7.2,0) node[anchor=west] {$\text{Temps (min)}$};
    \draw[->] (0,-0.2) -- (0,6.2);
    \draw (1,6) node[anchor=south] {$\text{Altura (m)}$};

\foreach \x/\xtext in {1/2, 2/4, 3/6, 4/8, 5/10, 6/12}
{
\draw (\x, -0.1 cm) -- (\x, 0.1 cm);
\draw (\x, -0.3 cm) node {$\xtext$};
}

\draw (-0.1 cm,1) -- (0.1 cm,1);
\draw (0,1) node[anchor=east] {$100$};

\draw (-0.1 cm,2) -- (0.1 cm,2);
\draw (0,2) node[anchor=east] {$200$};

\draw (-0.1 cm,3) -- (0.1 cm,3);
\draw (0,3) node[anchor=east] {$300$};

\draw (-0.1 cm,4) -- (0.1 cm,4);
\draw (0,4) node[anchor=east] {$400$};

\draw (-0.1 cm,5) -- (0.1 cm,5);
\draw (0,5) node[anchor=east] {$500$};

\draw (-0.1 cm,6) -- (0.1 cm,6);
\draw (0,6) node[anchor=east] {$600$};

\draw[domain=0:5, variable=\x, color=blue, ultra thick] plot({0.16*(\x)^2+0.4*\x}, {\x});

\stoptikzpicture
\eTD
\eTR
\eTABLE}


\startitemize[a,packed]
\item A quina altura rebenta el globus?
\item Quan tarda en rebentar des de que l'amollam?
\item Quines variables intervenen?
\item Quina escala s'utilitza per a cada variable?
\item Quin és el domini de definició d'aquesta funció?
\item Quin és el seu recorregut
\item Quina altura guanya el globus entre el minut 0 i el 4? I entre el 4 i el 8? En quin d'aquests intervals creix més ràpidament la funció?
\stopitemize

\doifmode{copyright}{
Font: exercici extret de {\em Llibre de text de 3r d'ESO}. Tema \quotation{Funciones y gráficas}. Ed. Anaya. Exercici 1. Pàgina 225. IES Arroyo. Dpt. Matemàtiques (\href{Averroes}{http://www.juntadeandalucia.es/averroes/iesarroyo/matematicas/matematicas.htm}).
}
\stopexercici

\startexercici[exercici:capacitat-respiratòria] Per mesurar la capacitat espiratòria dels pulmons es fa una prova que consisteix en inspirar al màxim i després espirar tan ràpid com sigui possible en un aparell que s'anomena \quotation{espiròmetre}. Aquesta corba indica el volum d'aire que entra i surt dels pulmons.

\placefigure
  [force,none]
  [fig:capacitat-respiratòria]
  {Capacitat respiratòria}
  {\bTABLE[frame=off,align={middle,lohi}]
\bTR
\bTD
\starttikzpicture[xscale=0.95]
    \draw[help lines,color=gray!20,step=0.5] (-0.1,-0.1) grid (15.2,8);
    \draw[->] (-0.2,0) -- (15.2,0);
    \draw (13,0) node[anchor=south, fill=white] {$\text{Temps (s)}$};
    \draw[->] (0,-0.2) -- (0,8.2);
    \draw (1,8) node[anchor=south] {$\text{Volum (l)}$};

\foreach \x in {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14}
{
\draw (\x, -0.1 cm) -- (\x, 0.1 cm);
\draw (\x, -0.3 cm) node {$\x$};
}

\foreach \y in {1, 2, 3, 4, 5, 6, 7, 8}
{
\draw (-0.1 cm,\y) -- (0.1 cm,\y);
\draw (0,\y) node[anchor=east] {$\y$};
}


% La funció \frac{60(x+1)+24}{20+(x+1)^2}+0.5 no és exactament la que tenia a l'exercici original, però és molt semblant.
% L'únic error és que s'ha de veure que l'assímptota és menor que el volum inicial
\draw[domain=0:15, variable=\x, color=blue, ultra thick] plot({\x}, {(60*(\x+1) +24)/(20+(\x+1)^2) + 0.5});

\stoptikzpicture
\eTD
\eTR
\eTABLE}

\startitemize[a,packed]
\item Quin és el volum en el moviment inicial?
\item Quin temps va durar l'observació?
\item Quin és la capacitat màxima dels pulmons d'aquesta persona?
\item Quin és el volum als 10 segons després d'iniciar-se la prova?
\stopitemize

\doifmode{copyright}{
Font: exercici extret de {\em Llibre de text de 3r d'ESO}. Tema \quotation{Funciones y gráficas}. Ed. Anaya. Exercici 2. Pàgina 225. IES Arroyo. Dpt. Matemàtiques (\href{Averroes}{http://www.juntadeandalucia.es/averroes/iesarroyo/matematicas/matematicas.htm})
}

\stopexercici

\startexercici[exercici:porta-col-legi] En la porta d'un col·legi hi ha una parada de llaminadures. En aquesta gràfica es veu la quantitat de doblers que hi ha a la caixa al llarg d'un dia:

\placefigure
  [force,none]
  [fig:parada-de-llaminadura]
  {Venda de llaminadures}
  {\bTABLE[frame=off,align={middle,lohi}]
\bTR
\bTD
\starttikzpicture
    \draw[help lines,color=gray!20,step=0.5] (-0.1,-0.1) grid (10.2,6);
    \draw[->] (-0.2,0) -- (10.2,0);
    \draw (10.2,0) node[anchor=west] {$\text{Hores}$};
    \draw[->] (0,-0.2) -- (0,6.2);
    \draw (1,6) node[anchor=south] {$\text{Ingressos (€)}$};

\foreach \x/\xtext in {0/8, 1/9, 2/10, 3/11, 4/12, 5/13, 6/14, 7/15, 8/16, 9/17, 10/18}
{
\draw (\x, -0.1 cm) -- (\x, 0.1 cm);
\draw (\x, -0.3 cm) node {$\xtext$};
}

\draw (-0.1 cm,1) -- (0.1 cm,1);
\draw (0,1) node[anchor=east] {$4$};

\draw (-0.1 cm,2) -- (0.1 cm,2);
\draw (0,2) node[anchor=east] {$8$};

\draw (-0.1 cm,3) -- (0.1 cm,3);
\draw (0,3) node[anchor=east] {$12$};

\draw (-0.1 cm,4) -- (0.1 cm,4);
\draw (0,4) node[anchor=east] {$16$};

\draw (-0.1 cm,5) -- (0.1 cm,5);
\draw (0,5) node[anchor=east] {$20$};

\draw (-0.1 cm,6) -- (0.1 cm,6);
\draw (0,6) node[anchor=east] {$24$};


\draw[color=blue, ultra thick] (0,1) -- (0.5,1.25) -- (3,1.25) -- (3.5,4) --(5, 4) -- (6,5.5);
\draw[color=blue, ultra thick] (7,1) -- (7.5, 1.5) -- (9,1.5) -- (10, 3.5);

\stoptikzpicture
\eTD
\eTR
\eTABLE}

\startitemize[a,packed]
\item A quina hora comencen les classes pel matí?
\item A quina hora és el pati? Quant dura?
\item La parada es tanca al migdia i l'amo s'enduu els doblers a casa. Quins varen ser els ingressos aquest matí?
\item Quin és l'horari d'horabaixa del col·legi?
\item Aquesta funció és contínua o discontínua?
\stopitemize

\doifmode{copyright}{
Font: exercici extret de {\em Llibre de text de 3r d'ESO}. Tema \quotation{Funciones y gráficas}. Ed. Anaya. Exercici 4. Pàgina 225. IES Arroyo. Dpt. Matemàtiques (\href{Averroes}{http://www.juntadeandalucia.es/averroes/iesarroyo/matematicas/matematicas.htm})
}

\stopexercici

\startexercici[exercici:anada-institut] Na Marta, en Marc, n'Elena i en Lluís comenten com ha anat la seva anada al l'institut aquest matí.

\bigskip
\startitemize[distance=2.4em]
\sym{\sc Marta:} Vaig anar amb motocicleta; però se m'oblidà un treball que havia d'entregar i vaig haver de tornar a ca meva. Després vaig córrer tot el que pogué fins a arribar a l'escola.

\sym{\sc Marc:} Ma mare me va dur en cotxe; però ens trobàrem un embús en el semàfor que hi ha a la meitat de camí i ens va retardar molt.

\sym{\sc Elena:} Me vaig trobar en el portal de ca nostra un amic que anava a un altre col·legi. Vàrem fer junts una part del camí i, quan ens vàrem separar, vaig haver de fer més via perquè, amb la xerrada, se me va fer tard.

\sym{\sc Lluís:} Vaig sortir de casa molt aviat perquè havia quedat amb na Maria i era tard. Després vàrem fer el camí junts amb més calma. 
\stopitemize

\bigskip
Els quatre van al mateix col·legi i cadascuna d'aquestes gràfiques mostra, {\em en distint ordre}, la trajectòria que han duit a terme des de la sortida de les seves cases fins a l'entrada al col·legi. En totes les gràfiques s'ha utilitzat la mateixa escala.

\placefigure
  [split,force,none]
  [fig:recorregut-diverses-persones]
  {Recorregut de Marta, Marc, Elena i Lluís}
  {\bTABLE[frame=off,align={middle,lohi}, split=yes]
\bTR \bTD \starttikzpicture
    \draw[help lines,color=gray!20,step=0.5] (-0.6,-0.6) grid (5.2,5);
    \draw (-1.2,0) -- (5.2,0);
    \draw (5,0) node[anchor=south east] {$\text{Temps}$};
    \draw (0,-1.2) -- (0,5.2);
    \draw (1,5) node[anchor=north] {$\text{Distància}$};


\draw[color=blue, ultra thick] (0,0) -- (2.5,1) -- (4.5,4);
\draw[red] (5, 5) node[anchor=north,fill=white] {$\text{A}$};

\stoptikzpicture
 \eTD \bTD \starttikzpicture
    \draw[help lines,color=gray!20,step=0.5] (-0.6,-0.6) grid (5.2,5);
    \draw (-1.2,0) -- (5.2,0);
    \draw (5,0) node[anchor=south east] {$\text{Temps}$};
    \draw (0,-1.2) -- (0,5.2);
    \draw (1,5) node[anchor=north] {$\text{Distància}$};


\draw[color=blue, ultra thick] (0,0) -- (1,1) -- (2,0) -- (4.5,3.5);
\draw[red] (5, 5) node[anchor=north,fill=white] {$\text{B}$};

\stoptikzpicture \eTD \eTR
\bTR \bTD \starttikzpicture
    \draw[help lines,color=gray!20,step=0.5] (-0.6,-0.6) grid (5.2,5);
    \draw (-1.2,0) -- (5.2,0);
    \draw (5,0) node[anchor=south east] {$\text{Temps}$};
    \draw (0,-1.2) -- (0,5.2);
    \draw (1,5) node[anchor=north] {$\text{Distància}$};


\draw[color=blue, ultra thick] (0,0) -- (1,1.5) -- (4,3);
\draw[red] (5, 5) node[anchor=north,fill=white] {$\text{C}$};

\stoptikzpicture \eTD \bTD \starttikzpicture
    \draw[help lines,color=gray!20,step=0.5] (-0.6,-0.6) grid (5.2,5);
    \draw (-1.2,0) -- (5.2,0);
    \draw (5,0) node[anchor=south east] {$\text{Temps}$};
    \draw (0,-1.2) -- (0,5.2);
    \draw (1,5) node[anchor=north] {$\text{Distància}$};


\draw[domain=0:1, variable=\x, color=blue, ultra thick] plot({\x}, {2*(\x)^2});
\draw[color=blue, ultra thick] (1,2) -- (1.5,2);
\startscope[shift={(1.5,2)}]
\draw[domain=0:0.5, variable=\x, color=blue, ultra thick] plot({\x}, {2*(\x)^2});
\stopscope
\draw[color=blue, ultra thick] (2,2.5) -- (2.5,2.5);
\startscope[shift={(2.5,2.5)}]
\draw[domain=0:0.5, variable=\x, color=blue, ultra thick] plot({\x}, {2*(\x)^2});
\stopscope
\draw[color=blue, ultra thick] (3,3) -- (3.5,3);
\startscope[shift={(3.5,3)}]
\draw[domain=0:0.5, variable=\x, color=blue, ultra thick] plot({\x}, {4*(\x)^2});
\stopscope
\draw[red] (5, 5) node[anchor=north,fill=white] {$\text{D}$};

\stoptikzpicture \eTD \eTR
\eTABLE}

\startitemize[a,packed]
\item Quina és la gràfica que relaciona amb la descripció que ha fet cadascú?
\item Qui viu més aprop del col·legi?
\item Qui va tardà menys en arribar-hi?
\stopitemize

\doifmode{copyright}{
Font: exercici extret de {\em Llibre de text de 3r d'ESO}. Tema \quotation{Funciones y gráficas}. Ed. Anaya. Exercici 5. Pàgina 226. IES Arroyo. Dpt. Matemàtiques (\href{Averroes}{http://www.juntadeandalucia.es/averroes/iesarroyo/matematicas/matematicas.htm})
}

\stopmode