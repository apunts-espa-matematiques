\startsection[title={Càlcul de percentatges}]

\startsubject[title={Preguntes}]

\startexercici[reference=exercici:calcul-percentatges-1, title={càlcul}] Calculeu els tants per cent següents:
\startitemize[a,columns, three]
\item 24\% de 90 
\item 79\% de 60
\item 82\% de 30
\item 0,3\% de 3590
\item 3\% de 450
\item 80\% de 2.945
\item 200\% de 480
\item 150\% de 300
\item 250\% de 4000 
\stopitemize
\stopexercici

\startexercici[reference=exercici:calcul-percentatges-2, title={\% $\rightarrow$ fracció}] Expresseu els percentatges següents en forma de fracció:
\startitemize[a,columns, four, packed]
\item 70 \%
\item 10 \% 
\item 5 \% 
\item 2 \%
\item 25\%
\item 40\%
\item 12\%
\item 7\%
\item 110\%
\item 224\%
\item 550\%
\item 24\%
\item 95\%
\item 15\%
\item 100\%
\item 90\%
\stopitemize

Les podeu simplificar?
\stopexercici


\startexercici[reference=exercici:calcul-percentatges-5, title={\% del total}] Trobeu quin tant per cent representa la part indicada del total
\startitemize[a, columns, three]
\item 2 de 6
\item 10 de 12
\item 20 de 50
\item 25 de 300
\item 90 de 95
\item 8 de 1000
\item 1 de 25
\item 40 de 60
\item $3/4$ de 60
\item $4/5$ de 80
\item $2/3$ de 30
\item $1/6$ de 900
\item 90 \char"2030\ de 2500
\item 20 \char"2030\ d'un milió
\item 40 \char"2030\ de 10.000
\item 25 \char"2030\ de 1500
\item 250 de 100.000
\item 1 d'un milió
\stopitemize

Podríeu expressar les informacions anteriors com a una fracció?
\stopexercici

\startexercici[reference=exercici:calcul-percentatges-3, title={transformació \%}] Completeu aquesta taula (taula~\in[taula:percentatges-0]):

\startplacetable[location=here, reference=taula:percentatges-0, title={Percentatges, tants per mil, tants per u i fraccions}]
\bTABLE[frame=off, width=fit]
\bTR
\bTD
{\bTABLE[setups={table4:header, table4:frame, table4:style}]
\bTR \bTD \%  \eTD \bTD \char"2030 \eTD \bTD tant per u \eTD \bTD fracció \eTD \eTR
\bTR \bTD 20 \eTD \bTD \eTD \bTD \eTD \bTD \eTD \eTR
\bTR \bTD 50  \eTD \bTD \eTD \bTD \eTD \bTD \eTD \eTR
\bTR \bTD \eTD \bTD 25 \eTD \bTD \eTD \bTD \eTD \eTR
\bTR \bTD \eTD \bTD 0,05 \eTD \bTD \eTD \bTD \eTD \eTR
\eTABLE}
\eTD
\bTR
\bTR
\bTD
{\bTABLE[setups={table4:header, table4:frame, table4:style}]
\bTR \bTD \%  \eTD \bTD \char"2030 \eTD \bTD tant per u \eTD \bTD fracció \eTD \eTR
\bTR \bTD \eTD \bTD \eTD \bTD 0.4 \eTD \bTD \eTD \eTR
\bTR \bTD \eTD \bTD \eTD \bTD 1.2 \eTD \bTD \eTD \eTR
\bTR \bTD 300 \eTD \bTD \eTD \bTD \eTD \bTD \eTD \eTR
\bTR \bTD 0,001 \eTD \bTD \eTD \bTD \eTD \bTD \eTD \eTR
\eTABLE}
\eTD
\eTR
\eTABLE
\stopplacetable
\stopexercici

\startexercici[reference=exercici:calcul-percentatges-4, title={transformació \%}] Completeu la taula següent (taula~\in[taula:percentatges-1]):

\startplacetable[location=here, reference=taula:percentatges-1, title={Percentatges, tants per mil i tants per u}]
\bTABLE[setups={table4:header, table4:frame, table4:style}]
\bTR \bTD \eTD \bTD \%  \eTD \bTD \char"2030 \eTD \bTD tant per u \eTD \bTD fracció \eTD \eTR
\bTR \bTD 3 de cada 7 \eTD \bTD \eTD \bTD \eTD \bTD \eTD \bTD \eTD \eTR
\bTR \bTD 0,09 per 1 \eTD \bTD \eTD \bTD \eTD \bTD \eTD \bTD \eTD \eTR
\bTR \bTD 39 per mil \eTD \bTD \eTD \bTD \eTD \bTD \eTD \bTD \eTD \eTR
\bTR \bTD  \eTD \bTD \eTD \bTD \eTD \bTD \eTD \bTD 3/4 \eTD \eTR
\bTR \bTD \eTD \bTD \eTD \bTD \eTD \bTD 0.5 \eTD \bTD \eTD \eTR
\bTR \bTD \eTD \bTD \eTD \bTD 15 \eTD \bTD \eTD \bTD \eTD \eTR
\eTABLE
\stopplacetable
\stopexercici

\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exercici:calcul-percentatges-1]} 
\stopitemize

\stopsubject

\stopsection
