\startsection[reference=seccio:funcio-afi-problemes, title={Problemes de funció afí}]

\startsubject[title={Preguntes}]

\startexercici[reference=exer:estefania-capses, title={capses de xocolata}] N'Estefania està ajudant a la banda de música dels seus amics a recaptar diners per a fer una gira. El grup decideix vendre tabletes de xocolata. Cada tableta es ven per 1,50 € i cada caixa conté 20 tabletes. D'altra banda, cada caixa els costa 0,20 €.

\startitemize[a]
\item Existeix una relació entre el nombre de capses venudes i els diners recaptats?
\item Feis el gràfic que relaciona aquestes dues quantitats?
\item Calculeu quants de diners s'hauran recaptat si es venen 100 capses de xocolata
\item Al final la banda ha recaptat 2.831 €. Quantes capses han venut?
\stopitemize
\stopexercici

\startexercici[title={capses de xocolata 2}] La banda de l'exercici anterior (exercici~\in[exer:estefania-capses]) decideix canviar de proveïdor: ara cada capsa els costa 0,10 €, però decideixen abaixar el preu de la tableta fins a 1,10 €. Guanyaran més o menys?
\stopexercici

\startexercici[title={clics}] Els ingressos de la pàgina {\tt www.matematiques.org} són deguts als conceptes següents:
\startitemize
\item 10 euros al mes fixos, degut a l'aportació dels fundadors
\item 0,32 euros per cada clic sobre la publicitat de la pàgina
\stopitemize
Quina funció relaciona el nombre de clics sobre la publicitat de la pàgina i els ingressos? Si els ingressos de la pàgina han estat de 458 €, quants de clics s'han fet a la pàgina?
\stopexercici

\startexercici[title={factura del gas}] En la factura del gas d'una ciutat es paga una quantitat fixa de 15 €, i 0,75 € per a cada metre cúbic consumit.
\startitemize[a,text]
\item Quan es paga per 3 $\text{m}^3$? I per 5 $\text{m}^3$?
\item Representeu la funció metres cúbics consumits-cost
\item Quina és la variable dependent i independent?
\item Què s'hagués consumit si s'hagués pagat 200 €?
\stopitemize
\stopexercici


\startexercici[title={anunci per paraules}] Un anunci per paraules en un diari costa 0,80 € per paraula, i s'estableix un mínim de cinc paraules per a poder ser admés.
\startitemize[a]
\item Elaboreu una taula i una gràfica de la funció que relaciona el nombre de paraules amb el preu de l'anunci.
\item Quines són les variables del gràfic?
\item Quantes paraules s'han d'emprar en un anunci per a què costi 20 €?
\stopitemize
\stopexercici

\startexercici[title={cridada telèfon}] Per fer una cridada de telèfon tenim els costos següents:
\startitemize
\item Simplement per l'establiment de cridada, 1,50 euros
\item Per cada minut, 0,320 euros
\stopitemize

Trobeu la funció que relaciona el cost d'una cridada de telèfon i el número de minuts que conversam. Quants minuts podríem parlar si tenguéssim 20 €?
\stopexercici

\startexercici[title={una granja de vaques}] En una granja, tenim que:
\startitemize
\item Les despeses fixes (llum, telèfon, etc) representen 240 euros
\item Dotze vaques mengen $450 \unit{kilo gram}$ de pinso al mes
\stopitemize

Tenint en compte que un kg de pinso val 1,32 €, calculeu la funció que relaciona les despeses en el mes i el número de vaques de la granja.

\startitemize[a]
\item Si tenim 250 vaques, quines despeses tendrem?
\item Si volem que els costos de la granja com a màxim siguin 10.000 €, quantes vaques podríem tenir?
\stopitemize
\stopexercici

\startexercici[reference=exercici:tawny-ports, title={vins d'Oporto}] En aquesta carta de vins d'Oporto (figura~\in[fig:carta]), tenim el darrer preu tapat:

\startplacefigure[location=here, reference=fig:carta, title={Carta de vins (en anglès)}]
\bTABLE[frame=off,align={middle,lohi}]
\bTR
  \bTD {\externalfigure[figs/modelitzacio-model-afi-Dan-Meyer-Tawny-ports.jpeg][scale=500]} \eTD
\eTR
\eTABLE
\stopplacefigure

\startitemize[a]
\item Quin és el darrer preu? Es pot establir de qualque manera?
\item Què valdria un vi que tingués 42 anys? I un vi de 137?
\item Es pot establir una fórmula que relacioni els anys i el preu del vi?
\item Podeu saber quants anys tendrà un vi que costi 25€?
\stopitemize

\stopexercici

\startexercici[title={dilatació}] La longitud $L$ d'una barra de metall és una funció lineal en funció de la temperatura $T$, on $L$ es mesura en centímetres i $T$ en graus Celsius. S'han realitzat les mesures següents: $L=124,91$ quan $T=0$, i $L=125,11$ quan $T=100$.
\startitemize[a]
\item Trobeu una fórmula que doni $L$ en funció de $T$.
\item Quina serà la longitud de la barra quan la temperatura sigui de $20 \unit{degrees}$?
\item A quina temperatura s'hauria d'encalentir la barra per a què fes $125,17 \unit{centi meter}$ de llarg?
\stopitemize
\stopexercici

\startexercici[reference=exer:cotxes, title={predicció dels cotxes}] Sabem que l'any 2012, es varen vendre 20.000 cotxes a un concessionari, i que l'any 2014, es varen vendre 40.000.
\startitemize[a,packed]
\item Podeu saber quants cotxes es vendran l'any 2020?
\item Aproximeu el nombre de cotxes venuts l'any 2010
\item Si segueix aquest ritme, hi haurà algun any en que el nombre de cotxes  venuts sobrepassi el milió?
\stopitemize
\stopexercici


\startexercici[title={nàufrag}] Un nàufrag decideix intentar partir de la illa on està. Si va a una velocitat de $2 \unit{meter per second}$ amb una balsa, quina funció relaciona el temps que passa i la distància a la que es troba de la illa? Representeu-la gràficament.

\blank[big]
Abans de partir, el nàufrag veu un vaixell a la deriva a 200 metres que, segons els seus càlculs, va a una velocitat de 1 m/s. Serà capaç d'agafar el vaixell en algun moment? Si la resposta és afirmativa, quan?
\stopexercici

\startexercici[title={manteniment del jardí}] Per pagar el manteniment del jardí d'una comunitat de veïns, hem de pagar 100 € al mes fixos i 10 euros per hora treballada. Què ens costarà al mes en funció de les hores que hi fan feina?
\stopexercici

\startexercici[reference=exercici:autoescola-Ramírez, title={preus de l'autoescola}] En l'autoescola Ramírez les tarifes són les següents:

\startframedtext[width=fit, location=middle]
\startalignment[middle]
Preu de cada classe ............................ 15 €

Preu de la matrícula ....................... 150 €

\stopalignment
\stopframedtext

\startitemize[a]
\item Si hem utilitzat els serveis de Ramírez i amb 5 classes hem obtingut el carnet. Què hem pagat?
\item Quan haguéssim pagat si haguéssim fet 6 classes? I amb 7 classes?
\item Feis una gràfica que relacioni el que costa obtenir el carnet segons el nombre de classes rebudes
\item Si volem gastar com a màxim 2000 €, quantes classes podrem fer?
\stopitemize
\stopexercici

\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exercici:tawny-ports]} Representeu gràficament la recta que passa per $(10,10)$ i $(20,13)$ i trobeu $f(30)$. Aquest seria el preu {\em lògic} de venda. La fórmula seria $f(x) = (x-10)\cdot 3 + 10$.

\sym{\in[exer:cotxes]}. L'any 2020, hi havia 100.000 cotxes, l'any 2013, 30.000 cotxes (la recta és $y = 10.000x − 20.100.000$). L'any 2110 es produiran un milió de cotxes
\stopitemize

\stopsubject

\stopsection
