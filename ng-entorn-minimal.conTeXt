%= Idioma del document
\mainlanguage[ca]

%=Enllaços del pdf es mostre i apareixen en lletra normal (per defecte és en negreta)
\setupinteraction[state=start,style=\tf]
\setupinteractionscreen[option=bookmark]
\placebookmarks[part,chapter,section,subsection][part] 

%= Entorns estructurals: exercicis, notes, etc.
% Veure: https://wiki.contextgarden.net/Command/defineenumeration
\definecolor[exercici_color][darkblue]

\defineenumeration
  [exercici]
  [alternative=serried, before={\blank[medium]\testpage[1]}, text={Exercici}, stopper={.\space}, width=fit, headstyle=\ss, distance=0.25em, width=fit, headcolor=exercici_color, title=yes, titlestyle=\ss, titledistance=0em, left={\bgroup\bf}, right={\egroup}, after={\blank[big]}, prefix=yes, prefixsegments=section, way=section]

\defineenumeration
  [exemple]
  [alternative=serried,text={Exemple},stopper={.\space}, headcolor=exercici_color, headstyle=\tf, distance=0.25em, width=fit, title=yes, titlestyle=\tf, titledistance=0em, left={\bgroup\bf}, right={\egroup}, prefix=yes, prefixsegments=section, way=section]


%= Mòduls

\usemodule[tikz]
\usetikzlibrary[decorations.markings]
\usetikzlibrary[trees]
\usetikzlibrary[calc, intersections, through, backgrounds, arrows, shapes.geometric, fadings, decorations.pathreplacing, shadings, shapes.geometric, patterns, fit, positioning, shapes.symbols, chains]
\usemodule[pgfmath]
%\usemodule[filter]
     
%= Aspecte
% Font del document
\setupbodyfont [kpfonts, 10pt] % modern o cmr, 11pt
\setupbodyfontenvironment[default][em=italic] % Cursiva del document (per defecte és 'slanted')

% Tipus de paper
\setuppapersize[executive][executive] % Executive dins A4 \setuppapersize[executive][A4]
\setuplayout[location={middle,middle}]

% Nombres de pàgina
\setuppagenumbering[location={footer,middle}]
\definestructureconversionset [frontpart:pagenumber] [] [romannumerals] % nombres romans abans de la part del cos

% Indentació
\setupindenting[yes,medium,next]
\setupitemize[margin=2em,headstyle=bold,afterhead={\blank[medium]}] % indentació dels itemizes

% Embelliment de seccions
\setuplabeltext[part=Part~]
\definemakeup[part][align=middle, pagestate=start]
\setuphead[part][placehead=yes, bodypartlabel=part, style={\bf \ssd}, page=yes, alternative=middle, before=\startpartmakeup, after=\stoppartmakeup, header=empty]

\setuphead[title][style={\bf \ssd}]
\setuphead[chapter][style={\bf \ssd}, alternative=middle, page=yes]

\setupheads[section][style={\bf \ssc}, alternative=margin, page=yes]
\setupheads[subject][style={\bf \ssc}, alternative=margin, page=no]
\setupheads[subsubject,subsection][style={\ssb}, alternative=margin]
\setupheads[subsubsubject,subsubsection][style={\ssa}, alternative=margin]

%% No faig un reset del nombre de les seccions després dels capítols i parts
%% https://tex.stackexchange.com/questions/610888/not-restart-section-numbering-after-each-part-in-context
%% NO funciona del tot: pareix que posa a 0 en comptador després d'un `subject`
\defineresetset[default][1,1,0][1] %% reset part and chapter, but not section
\setuphead[sectionresetset=default]


% Portada
\startuseMPgraphic{cover}
StartPage ;
fill Page withcolor white ;
pickup pencircle scaled 2mm ;
path p ; p := tensecircle(1cm,.75cm,.15cm) xsized(PaperWidth-2cm) ;
draw p shifted center Page withcolor .720green ;
StopPage ;
\stopuseMPgraphic
\defineoverlay[cover][\useMPgraphic{cover}]


% Apèndixos

\setuplabeltext[appendix=Apèndix~]
\setuplist[chapter][label=yes,width=fit,stopper=~~]

