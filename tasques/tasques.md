# Versions 


## curriculum 


### Tasks 

- [#58] continguts: àrea i volum de cossos geomètrics 
- [#59] continguts: problemes d'equacions 
- [#60] continguts: conversió fracció -> decimal 
- [#62] continguts: arrodoniment i truncament de nombres decimals 
- [#63] continguts: oposat i valor absolut de nombre sencer i problemes quotidians 
- [#65] continguts: problemes nombres (naturals, sencers, decimals, fraccions) 
- [#75] continguts: àrea i perímetre de figures planes triangularitzant 
- [#93] Posar més exercicis d'equacions amb fraccions; incloses composes (tipus frac{x+2}{5}) 
- [#97] a estadística, els mesos de l'any tenen mediana però no tenen mitjana 
- [#103] pensar les equacions de segon grau per resoldre amb el mètode de canvi de variable 
- [#115] Localitza punts sobre la Terra usant coordenades geogràfiques 
- [#117] Representa conjunts o regions senzills del pla usant coordenades 
- [#118] Fa servir representacions gràfiques per resoldre equacions de primer grau 
- [#119] Representa dades provinents de fenòmens regits per funcions afins 
- [#120] àrees i volums de poliedres i cossos de revolució 
- [#121] desenvolupament de cossos geomètrics 
- [#122] interès simple i compost 
- [#123] TAE 
- [#124] préstecs 
- [#131] Característiques d'una funció: domini, recorregut, màxims i mínims 
- [#132] sketch de les funcions més usuals: afins, quadràtiques, exponencial, de proporcionalitat inversa 
- [#133] Pul·lir els exercicis de probabilitat 
- [#134] Càlcul de percentatges encadenats 
- [#135] Transformació de percentatges en índexs de variació 
- [#137] Revisar les seccions Representació de funcions, Representació de funcions afins i Representació de funcions quadràtiques 
- [#140] Posar el càlcul de imatges d'una funció [https://docs.google.com/document/d/1YEe2VOijth7bQGfg_MNpneV8y4HYPdWoMSSagDPOLRo/edit?usp=sharing] i punts de tall amb els eixos de funcions afins [https://docs.google.com/document/d/1M7HGwmlmQlbwmFZHiVBMqeM2A8vnfpnLFJwMeOhwFiM/edit?usp=sharing] 
- [#142] Refer els exercicis de notació científica: de NC a ordinari, a l'inrevés però amb ordinari de veres, després pseudonotació científica a nombre ordinari o a notació científica; després comparar coses expressades amb pseudonotació científica; després tipus test un ordinari expressar en notació científica (ex. 2E10, 20E9, 0.2E8), ... 
- [#143] Refer exercicis de representació gràfica: paràboles i funcions afins 
- [#144] equacions de primer grau: falten equacions amb parèntesis i fraccions combinades 
- [#145] Cossos geomètrics: exercicis de càlcul de volum i àrea; sense i amb Pitàgores 
- [#148] Estadística: fer problemes més fàcils de desviació mitjana (que doni un nombre sencer la mitjana i qye hi hagi poques dades) i posar-los just després del primer exercici 
- [#149] Fer més exercicis de notació científica 
- [#150] Fer més exercicis de representació de rectes i de paràboles 

# Versions 


## curriculum 


### Tasks 

- [#58] continguts: àrea i volum de cossos geomètrics 
- [#59] continguts: problemes d'equacions 
- [#60] continguts: conversió fracció -> decimal 
- [#62] continguts: arrodoniment i truncament de nombres decimals 
- [#63] continguts: oposat i valor absolut de nombre sencer i problemes quotidians 
- [#65] continguts: problemes nombres (naturals, sencers, decimals, fraccions) 
- [#75] continguts: àrea i perímetre de figures planes triangularitzant 
- [#93] Posar més exercicis d'equacions amb fraccions; incloses composes (tipus frac{x+2}{5}) 
- [#97] a estadística, els mesos de l'any tenen mediana però no tenen mitjana 
- [#103] pensar les equacions de segon grau per resoldre amb el mètode de canvi de variable 
- [#115] Localitza punts sobre la Terra usant coordenades geogràfiques 
- [#117] Representa conjunts o regions senzills del pla usant coordenades 
- [#118] Fa servir representacions gràfiques per resoldre equacions de primer grau 
- [#119] Representa dades provinents de fenòmens regits per funcions afins 
- [#120] àrees i volums de poliedres i cossos de revolució 
- [#121] desenvolupament de cossos geomètrics 
- [#122] interès simple i compost 
- [#123] TAE 
- [#124] préstecs 
- [#131] Característiques d'una funció: domini, recorregut, màxims i mínims 
- [#132] sketch de les funcions més usuals: afins, quadràtiques, exponencial, de proporcionalitat inversa 
- [#133] Pul·lir els exercicis de probabilitat 
- [#134] Càlcul de percentatges encadenats 
- [#135] Transformació de percentatges en índexs de variació 
- [#137] Revisar les seccions Representació de funcions, Representació de funcions afins i Representació de funcions quadràtiques 
- [#140] Posar el càlcul de imatges d'una funció [https://docs.google.com/document/d/1YEe2VOijth7bQGfg_MNpneV8y4HYPdWoMSSagDPOLRo/edit?usp=sharing] i punts de tall amb els eixos de funcions afins [https://docs.google.com/document/d/1M7HGwmlmQlbwmFZHiVBMqeM2A8vnfpnLFJwMeOhwFiM/edit?usp=sharing] 
- [#142] Refer els exercicis de notació científica: de NC a ordinari, a l'inrevés però amb ordinari de veres, després pseudonotació científica a nombre ordinari o a notació científica; després comparar coses expressades amb pseudonotació científica; després tipus test un ordinari expressar en notació científica (ex. 2E10, 20E9, 0.2E8), ... 
- [#143] Refer exercicis de representació gràfica: paràboles i funcions afins 
- [#144] equacions de primer grau: falten equacions amb parèntesis i fraccions combinades 
- [#145] Cossos geomètrics: exercicis de càlcul de volum i àrea; sense i amb Pitàgores 
- [#148] Estadística: fer problemes més fàcils de desviació mitjana (que doni un nombre sencer la mitjana i qye hi hagi poques dades) i posar-los just després del primer exercici 
- [#149] Fer més exercicis de notació científica 
- [#150] Fer més exercicis de representació de rectes i de paràboles 

## revisió 


### Tasks 

- [#5] Ajuntar els nous apunts (directori 'apunts-curs-2020-2021') amb els vells 
- [#10] Mirar els apunts antics (directori 'antics') un per un si tenen exercicis diferents, per mòduls 
- [#29] Llevar figures que no utilitz 
- [#38] [error] A l'exercici 322 de proporcionalitat geomètrica: he de canviar l'escala numèrica de manera que el resultat obtingut sigui creïble: doni quilòmetres i no metres 
- [#41] Posar els drets d'autor externs, una vegada fet purga 
- [#42] Llevar exercicis en favor dels exercicis automàtics [https://repo.or.cz/exercicis-automatics-matematiques.git] 
- [#54] Substituir imatges per gràfics vectorials (TikZ) 
- [#55] Intentar reduir de forma dràstica els drets d'autor externs (sobre tot que no siguin Creative Commons) 
- [#56] Posar els problemes del Joan Alcover de proporcionalitat 
- [#57] Posar les solucions 
- [#72] Les solucions de les operacions de fraccions han d'estar simplificades i també *no* simplificades. Despista que només estigui simplificats 
- [#76] No he de tenir més de 100 pàgines en total 
- [#79] Mirar els exercicis d'altres companys (Javi Gómez) d'ESPA 2.1. Sobretot equacions, sistemes, etc. 
- [#83] Aprendre del Toomates: Els exercicis de toomates estan molt bé perquè estan molt ben classificat: fas una cosa, i exercici d'això, un altra, etc. No posa tots els exercicis junts de cossos geomètrics sinó que els diferencia. Això està molt bé. A llarg termini ho he de tenir així. Encara que tengui 1000 pàgines, els apunts han de ser com una col·lecció de mini-fitxes: teoria essencial, exercici d'allò, solucions i resolució. Per exemple, fórmula del volum de l'esfera, exercicis, solucions i resolució. Dues pàgines, no més 
- [#91] Que el llibret sigui independent del currículum vigent: més o menys sempre es fa el mateix; són temes fixes 
- [#92] Mirar el currículum antic i fer les parts que no tenia fetes 
- [#95] especificar les llicències individuals per a passar el test del programa reuse [reuse lint] 
- [#96] Alinear-me amb el Common Core; són clars i concrets. El currículum d'Espanya s'enrolla. Potser puc fer referència a les competències dels estàndards estatunidencs a cada apartat 
- [#98] revisar els accents diacrítics segons la nova normativa 
- [#113] Simplificar les taules. De vegades tenc una taula dins una taula 
- [#129] [error] Posar els nombres periòdics amb símbol adient [https://mailman.ntg.nl/archives/list/ntg-context@ntg.nl/thread/I7LOGTTO5EIR5DXOCYXTIVCHVZGHBSIF/] 
- [#136] Mirar els apunts d'en Xisco Sebastià i incorporar els que siguin interessants; per exemple el del globus terraqui 
- [#141] Basar els continguts dels apunts d'ESPA en les proves lliure d'ESO. Integrar de qualque manera els enunciat dels Cangur 
- [#151] Revisa si s'ha de posar 8€ o bé 8 € 
- [#152] Llevar continguts externs que siguin CC-BY-{NC, ND}-SA i copyrights. De manera que tengui un manual compatible amb CC-BY-SA 4.0 
- [#153] Publicar llibre 
- [#154] Posar les solucions de probabilitat composta al final i no just després de cada exercici 

