\startsection[reference=seccio:pla-cartesia, title={Pla cartesià}]

\startsubject[title={Preguntes}]

\startsubsubject[title={Llegir punts}]

\startexercici[reference=exer:pla-cartesia-llegir-1] Escriviu les coordenades dels punts següents (figura~\in[figura:pla-cartesia-1]):

\startplacefigure[location=here, reference=figura:pla-cartesia-1, title={Punts al pla cartesià}]
\bTABLE[frame=off]
 \bTR
  \bTD
        % del geogebra modificat perquè el codi del geogebra és molt lleig
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
	\draw [color=cqcqcq,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-6.18,-3.14) grid (4.5,4.5);
	\draw[->,color=black] (-6.18,0) -- (4.5,0);
	\foreach \x in {-6,-5,-4,-3,-2,-1,1,2,3,4}
	\draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt) node[below] {\tfx $\x$};
	\draw[->,color=black] (0,-3.14) -- (0,4.5);
	\foreach \y in {-3,-2,-1,1,2,3,4}
	\draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt) node[left] {\tfx $\y$};
	\draw[color=black] (0pt,-10pt) node[right] {\tfx $0$};
	\clip(-6.18,-3.14) rectangle (4.5,4.5);
	\draw [fill=dark blue] (3,0) circle (2pt);
	\draw[color=blue] (3,0) node[anchor=south] {$A$};
	\draw [fill=dark blue] (2,1) circle (2pt);
	\draw[color=blue] (2,1) node[anchor=south] {$B$};
	\draw [fill=dark blue] (1,-1) circle (2pt);
	\draw[color=blue] (1,-1) node[anchor=north] {$C$};
	\draw [fill=dark blue] (-2,3) circle (2pt);
	\draw[color=blue] (-2,3) node[anchor=south] {$D$};
	\draw [fill=dark blue] (-4,0) circle (2pt);
	\draw[color=blue] (-4,0) node[anchor=south] {$E$};
	\draw [fill=dark blue] (-3,-1) circle (2pt);
	\draw[color=blue] (-3,-1) node[anchor=north] {$F$};
	\stoptikzpicture
  \eTD
 \eTR
\eTABLE
\stopplacefigure
\stopexercici

\startexercici[reference=exer:pla-cartesia-llegir-2] Quines coordenades tenen els punts següents (figura~\in[figura:pla-cartesia-2])?

\startplacefigure[location=here, reference=figura:pla-cartesia-2, title={Punts al pla cartesià}]
\bTABLE[frame=off]
 \bTR
  \bTD
        % de l'exportació de geogebra però modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
	  % Pla cartesià
	  \draw [help lines,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-5.8,-3.8) grid (7.3,6.4);
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (-5.8,0) -- (7.3,0);
	  % Nombres a l'eix
	  \foreach \x in {-5,-4,-3,-2,-1,1,2,3,4,5,6,7}
	  {
	    \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt);
	    \draw (\x,0) node[anchor=north] {\small $\x$};
	  }
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (0,-3.8) -- (0,6.4);
  	  % Nombres a l'eix
	  \foreach \y in {-3,-2,-1,1,2,3,4,5,6}
	  {
	    \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
	    \draw (0, \y) node[anchor=west] {\small $\y$};
	  }

	  % Punts
	  \draw [fill=blue!50] (1, 1) circle (2pt);
	  \draw[color=blue] (1, 1) node[anchor=south west] {$A$};
	  \draw [fill=blue!50] (4, 2) circle (2pt);
	  \draw[color=blue] (4, 2) node[anchor=south west] {$B$};
	  \draw [fill=blue!50] (6, 1) circle (2pt);
	  \draw[color=blue] (6, 1) node[anchor=south west] {$C$};
	  \draw [fill=blue!50] (2,-2) circle (2pt);
	  \draw[color=blue] (2, -2) node[anchor=west] {$D$};
	  \draw [fill=blue!50] (4, -3) circle (2pt);
	  \draw[color=blue] (4, -3) node[anchor=west] {$E$};
	  \draw [fill=blue!50] (1,3) circle (2pt);
	  \draw[color=blue] (1, 3) node[anchor=south west] {$F$};
	  \draw [fill=blue!50] (1,4) circle (2pt);
	  \draw[color=blue] (1,4) node[anchor=south west] {$G$};
	  \draw [fill=blue!50] (1,5) circle (2pt);
	  \draw[color=blue] (1,5) node[anchor=south west] {$H$};
	  \draw [fill=blue!50] (-1,-1) circle (2pt);
	  \draw[color=blue] (-1,-1) node[anchor=north east] {$I$};
	  \draw [fill=blue!50] (-2,1) circle (2pt);
	  \draw[color=blue] (-2,1) node[anchor=south] {$J$};
	  \draw [fill=blue!50] (-1,-2) circle (2pt);
	  \draw[color=blue] (-1,-2) node[anchor=north east] {$K$};
	  \draw [fill=blue!50] (-3,-2) circle (2pt);
	  \draw[color=blue] (-3,-2) node[anchor=north east] {$L$};
	  \draw [fill=blue!50] (-4,-1) circle (2pt);
	  \draw[color=blue] (-4,-1) node[anchor=north east] {$M$};
	  \draw [fill=blue!50] (-4,1) circle (2pt);
	  \draw[color=blue] (-4,1) node[anchor=south] {$N$};
	\stoptikzpicture
  \eTD
 \eTR
\eTABLE
\stopplacefigure
\stopexercici

\stopsubsubject

\startsubsubject[title={Escriure punts}]

\startexercici[reference=exer:pla-cartesia-escriure-1] \startitemize[a,text] \item Representeu al pla cartesià els punts següents: $A=(1,4)$, $B=(4,1)$, $C=(-5,2)$, $D=(-3,-1)$, $E=(6,-3)$, $F=(0,2)$, $G=(-2,0)$ i $H= \text{origen de coordenades}$, i \item digueu a quin quadrant pertanyen. \stopitemize
\stopexercici


\startexercici[reference=exer:pla-cartesia-escriure-2] Representeu al pla cartesià els punts següents: $A=(1,2)$, $B=(2,1)$ , $C=(1,1)$, $D=(2,2)$, $E=(-1,2)$, $F=(0,2)$, $G=(1,0)$, $H=(0,0)$ i $I=(-2,-3)$. Digueu a quin quadrant pertanyen.
\stopexercici

\startexercici[reference=exer:pla-cartesia-escriure-3] Representeu al pla cartesià els punts següents i digueu a quin quadrant pertanyen: $A=(5,6)$, $B=(-3,4)$, $C=(7,-3)$, $D=(-1,-5)$, $E=(0,-2)$, i $F=\text{origen de coordenades}$.
\stopexercici

\startexercici[reference=exer:pla-cartesia-escriure-4] \startitemize[a, text] \item Representeu al pla cartesià els punts següents: $A=(1,2)$, $B=(2,1)$, $C=(-3,2)$ i $D=(-4,-1)$, $E=(2,-3)$, $F=(0,3)$, $G=(-2,0)$ i $H=\text{origen de coordenades}$. \item Digueu a quin quadrant pertanyen. \stopitemize
\stopexercici

\stopsubsubject


\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exer:pla-cartesia-llegir-1]} Les coordenades dels punts són $A=(3,0)$, $B=(2,1)$, $C=(1,-1)$, $D=(-2, 3)$, $E=(-4,0)$ i $F=(-3,-1)$.
\sym{\in[exer:pla-cartesia-llegir-2]} Les coordenades dels punts són $A=(1,1)$, $B=(4,2)$, $C=(6,1)$, $D=(2,-2)$, $E=(4,-3)$, $F=(1,3)$, $G=(1,4)$, $H=(1,5)$, $I=(-1, -1)$, $J=(-2,1)$, $K=(-1, -2)$, $L=(-3,-2)$, $M=(-4, -1)$, i $N=(-4, 1)$.

\sym{\in[exer:pla-cartesia-escriure-1]}

\startitemize[a]

\item Si representam els punts al pla cartesià obtenim (figura~\in[figura:solucions-pla-cartesia-1]):

\startplacefigure[location=force, reference=figura:solucions-pla-cartesia-1, title={Representació de punts al pla cartesià}]
\bTABLE[frame=off]
 \bTR
  \bTD
        % de l'exportació de geogebra però modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
	  % Pla cartesià
	  \draw [help lines,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-5.8,-3.8) grid (7.3,5.4);
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (-5.8,0) -- (7.3,0);
	  % Nombres a l'eix
	  \foreach \x in {-5,-4,-3,-2,-1,1,2,3,4,5,6,7}
	  {
	    \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt);
	    \draw (\x,0) node[anchor=north] {\small $\x$};
	  }
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (0,-3.8) -- (0,5.4);
  	  % Nombres a l'eix
	  \foreach \y in {-3,-2,-1,1,2,3,4,5}
	  {
	    \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
	    \draw (0, \y) node[anchor=west] {\small $\y$};
	  }

	  % Punts
	  \draw [fill=blue!50] (1, 4) circle (2pt);
	  \draw[color=blue] (1, 4) node[anchor=south] {$A$};
	  
	  \draw [fill=blue!50] (4, 1) circle (2pt);
	  \draw[color=blue] (4, 1) node[anchor=south] {$B$};
	  
	  \draw [fill=blue!50] (-5, 2) circle (2pt);
	  \draw[color=blue] (-5, 2) node[anchor=south] {$C$};
	  
	  \draw [fill=blue!50] (-3,-1) circle (2pt);
	  \draw[color=blue] (-3, -1) node[anchor=north] {$D$};
	  
	  \draw [fill=blue!50] (6, -3) circle (2pt);
	  \draw[color=blue] (6, -3) node[anchor=north] {$E$};
	  
	  \draw [fill=blue!50] (0,2) circle (2pt);
	  \draw[color=blue] (0, 2) node[anchor=east] {$F$};
	  	  
	  \draw [fill=blue!50] (-2,0) circle (2pt);
	  \draw[color=blue] (-2,0) node[anchor=south] {$G$};
	  
	  \draw [fill=blue!50] (0,0) circle (2pt);
	  \draw[color=blue] (0,0) node[anchor=north west] {$H$};
	  
	  
	\stoptikzpicture
  \eTD
 \eTR
\eTABLE
\stopplacefigure

\item 1r quadrant: $A$, $B$, $F$, $H$; 2n quadrant: $C$, $F$, $G$, $H$; tercer quadrant: $D$, $G$, $H$; quart quadrant: $E$, $H$.

\stopitemize

\sym{\in[exer:pla-cartesia-escriure-2]}

\startitemize[a]

\item La representació és la següent (figura~\in[figura:solucions-pla-cartesia-2]).

\startplacefigure[location=force, reference=figura:solucions-pla-cartesia-2, title={Representació de punts al pla cartesià}]
\bTABLE[frame=off]
 \bTR
  \bTD
        % de l'exportació de geogebra però modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
	  % Pla cartesià
	  \draw [help lines,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-4.3,-3.9) grid (3.3,3.9);
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (-4.3,0) -- (3.3,0);
	  % Nombres a l'eix
	  \foreach \x in {-4,-3,-2,-1,1,2,3}
	  {
	    \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt);
	    \draw (\x,0) node[anchor=north] {\small $\x$};
	  }
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (0,-3.9) -- (0,3.9);
  	  % Nombres a l'eix
	  \foreach \y in {-3,-2,-1,1,2,3}
	  {
	    \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
	    \draw (0, \y) node[anchor=west] {\small $\y$};
	  }

	  % Punts
	  \draw [fill=blue!50] (1, 2) circle (2pt);
	  \draw[color=blue] (1, 2) node[anchor=south] {$A$};
	  
	  \draw [fill=blue!50] (2, 1) circle (2pt);
	  \draw[color=blue] (2, 1) node[anchor=south] {$B$};
	  
	  \draw [fill=blue!50] (1, 1) circle (2pt);
	  \draw[color=blue] (1, 1) node[anchor=south] {$C$};
	  
	  \draw [fill=blue!50] (2,2) circle (2pt);
	  \draw[color=blue] (2, 2) node[anchor=south] {$D$};
	  
	  \draw [fill=blue!50] (-1, 2) circle (2pt);
	  \draw[color=blue] (-1, 2) node[anchor=east] {$E$};
	  
	  \draw [fill=blue!50] (0,2) circle (2pt);
	  \draw[color=blue] (0, 2) node[anchor=east] {$F$};
	  	  
	  \draw [fill=blue!50] (1,0) circle (2pt);
	  \draw[color=blue] (1,0) node[anchor=south] {$G$};
	  
	  \draw [fill=blue!50] (0,0) circle (2pt);
	  \draw[color=blue] (0,0) node[anchor=north west] {$H$};

	  \draw [fill=blue!50] (-2,-3) circle (2pt);
	  \draw[color=blue] (-2,-3) node[anchor=east] {$I$};
	  
	  
	\stoptikzpicture
  \eTD
 \eTR
\eTABLE
\stopplacefigure

\item I aquests punts pertanyen als quadrants següents:

	\startitemize[packed]

	\item 1r quadrant: $A$, $B$, $C$, $D$, $F$, $G$, $H$
	\item 2n quadrant: $E$, $F$, $H$
	\item 3r quadrant: $H$, $I$
	\item 4t quadrant: $G$, $H$

	\stopitemize

\stopitemize


\sym{\in[exer:pla-cartesia-escriure-3]} La representació dels punts és la següent (figura~\in[figura:solucions-pla-cartesia-3])

\startplacefigure[location=force, reference=figura:solucions-pla-cartesia-3, title={Representació de punts al pla cartesià}]
\bTABLE[frame=off]
 \bTR
  \bTD
        % de l'exportació de geogebra però modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
	  % Pla cartesià
	  \draw [help lines,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-5.8,-5.2) grid (8.3,6.6);
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (-5.8,0) -- (8.3,0);
	  % Nombres a l'eix
	  \foreach \x in {-5,-4,-3,-2,-1,1,2,3,4,5,6,7,8}
	  {
	    \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt);
	    \draw (\x,0) node[anchor=north] {\small $\x$};
	  }
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (0,-5.8) -- (0,6.4);
  	  % Nombres a l'eix
	  \foreach \y in {-5,-4,-3,-2,-1,1,2,3,4,5,6}
	  {
	    \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
	    \draw (0, \y) node[anchor=west] {\small $\y$};
	  }

	  % Punts
	  \draw [fill=blue!50] (5, 6) circle (2pt);
	  \draw[color=blue] (5, 6) node[anchor=south] {$A$};
	  
	  \draw [fill=blue!50] (-3, 4) circle (2pt);
	  \draw[color=blue] (-3, 4) node[anchor=south] {$B$};
	  
	  \draw [fill=blue!50] (7, -3) circle (2pt);
	  \draw[color=blue] (7, -3) node[anchor=north] {$C$};
	  
	  \draw [fill=blue!50] (-1,-5) circle (2pt);
	  \draw[color=blue] (-1, -5) node[anchor=north] {$D$};
	  
	  \draw [fill=blue!50] (0, -2) circle (2pt);
	  \draw[color=blue] (0, -2) node[anchor=east] {$E$};
	  	  
	  \draw [fill=blue!50] (0,0) circle (2pt);
	  \draw[color=blue] (0,0) node[anchor=north west] {$F$};
	  
	  
	\stoptikzpicture
  \eTD
 \eTR
\eTABLE
\stopplacefigure

\sym{\in[exer:pla-cartesia-escriure-4]}

\startitemize[a]

\item Vegeu la figura~\in[figura:solucions-pla-cartesia-4] per veure la representació dels punts al pla cartesià.

\startplacefigure[location=force, reference=figura:solucions-pla-cartesia-4, title={Representació de punts al pla cartesià}]
\bTABLE[frame=off]
 \bTR
  \bTD
        % de l'exportació de geogebra però modificat
	\starttikzpicture[line cap=round,line join=round,>=triangle 45,x=1.0cm,y=1.0cm]
	  % Pla cartesià
	  \draw [help lines,dash pattern=on 2pt off 2pt, xstep=1.0cm,ystep=1.0cm] (-5.8,-3.8) grid (3.3,4.4);
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (-5.8,0) -- (3.3,0);
	  % Nombres a l'eix
	  \foreach \x in {-5,-4,-3,-2,-1,1,2,3}
	  {
	    \draw[shift={(\x,0)},color=black] (0pt,2pt) -- (0pt,-2pt);
	    \draw (\x,0) node[anchor=north] {\small $\x$};
	  }
	  
	  % Eix de coordenades
	  \draw[<->,color=black] (0,-3.8) -- (0,4.4);
  	  % Nombres a l'eix
	  \foreach \y in {-3,-2,-1,1,2,3,4}
	  {
	    \draw[shift={(0,\y)},color=black] (2pt,0pt) -- (-2pt,0pt);
	    \draw (0, \y) node[anchor=west] {\small $\y$};
	  }

	  % Punts
	  \draw [fill=blue!50] (1, 2) circle (2pt);
	  \draw[color=blue] (1, 2) node[anchor=south] {$A$};
	  
	  \draw [fill=blue!50] (2, 1) circle (2pt);
	  \draw[color=blue] (2, 1) node[anchor=south] {$B$};
	  
	  \draw [fill=blue!50] (-3, 2) circle (2pt);
	  \draw[color=blue] (-3, 2) node[anchor=south] {$C$};
	  
	  \draw [fill=blue!50] (-4,-1) circle (2pt);
	  \draw[color=blue] (-4, -1) node[anchor=north] {$D$};
	  
	  \draw [fill=blue!50] (2, -3) circle (2pt);
	  \draw[color=blue] (2, -3) node[anchor=north] {$E$};
	  	  
	  \draw [fill=blue!50] (0,3) circle (2pt);
	  \draw[color=blue] (0,3) node[anchor=east] {$F$};

	  \draw [fill=blue!50] (-2,0) circle (2pt);
	  \draw[color=blue] (-2,0) node[anchor=south] {$G$};

	  \draw [fill=blue!50] (0,0) circle (2pt);
	  \draw[color=blue] (0,0) node[anchor=north west] {$H$};
	  
	  
	\stoptikzpicture
  \eTD
 \eTR
\eTABLE
\stopplacefigure

\item Els punts pertanyen als quadrants següents: \startitemize[a, text] \item 1r quadrant: $A$, $B$, $F$, $H$ \item 2n quadrant: $C$, $F$, $G$, $H$ \item 3r quadrant: $D$, $G$, $H$ \item 4t quadrant: $E$, $H$ \stopitemize

\stopitemize

\stopitemize

\stopsubject

\stopsection
