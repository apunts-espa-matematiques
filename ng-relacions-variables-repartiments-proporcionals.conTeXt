\startsection[reference=seccio:repartiments-proporcionals, title={Repartiments proporcionals}]

\startsubject[title={Preguntes}]

\startsubsubject[title={Directament proporcionals}]

\startexercici[reference=exercici:repartiments-proporcionals-1]  Na Laura, na Núria i en Pep juguen a la loteria. Aporten, respectivament, 20 euros, 30 euros i 10 euros. Al final toca (!). Els toca un premi de 10.000 euros. Quant toca a cadascú si han acordat repartir-s'ho proporcionalment?
\stopexercici

\startexercici[reference=exercici:repartiments-proporcionals-2] Dos germans en Vicenç i na Coloma obren una llibreta al banc. Cada mes, en Vicenç hi posa 40 euros i na Coloma 80 euros. Al cap d'uns anys tenen a la llibreta 3360 euros. Com els han de repartir? Què li toca a cadascú?
\stopexercici

\startexercici[reference=exercici:repartiments-proporcionals-3] Quatre socis decideixen comprar accions de borsa. Aporten 100, 500, 1000 i 5000 euros respectivament. Després d'un any obtenen uns beneficis de 25000 €. Què obté cadascú?
\stopexercici

\startexercici[reference=exercici:repartiments-proporcionals-4] Tres socis d'una empresa tenen el 10\%, el 40\% i el 50\% del capital de l'empresa. L'empresa quebra i han de pagar un deute de 23.000 euros. Què ha de pagar cadascú?
\stopexercici

\startexercici[reference=exercici:repartiments-proporcionals-5] Tres amics van als Go Karts!. Per problemes monetaris es veuen obligats a llogar un sol cotxe entre els tres. Paguen 2, 3 i 10 euros respectivament. Si sabem que amb 15 euros podem colcar 20 minuts, quants minuts manarà cadascú el cotxe?
\stopexercici

\startexercici[reference=exercici:repartiments-proporcionals-6] Tres amics van a un cibercafè. No tenen doblers per llogar cadascun un ordinador, per tant, decideixen llogar-ne un entre els tres. Es treuen tot el que tenen a les butxaques i aporten: 1,20 €, 2,50 € i 80 cèntims. Si sabem que una hora de connexió costa 6 €, quants minuts utilitzarà l'ordinador cadascú?
\stopexercici

\stopsubsubject

\startsubsubject[title={Inversament proporcionals}]

\startexercici[reference=exercici:repartiments-proporcionals-7] El cost de la matrícula en una acadèmia de música és menor com més notables s'han aconseguit en el curs anterior. Tres amics, en Pere, na Sara i n'Elionor, han tret 2, 3 i 5 notables, respectivament, i entre els tres han pagat 310 €. Quant els hi ha costat la matrícula a cadascun?
\stopexercici

\startexercici[reference=exercici:repartiments-proporcionals-8] El professor d'Anglès demana traduir 59 pàgines d'una novel·la a tres alumnes de manera que tradueixi més el que menys positius tengui. Si el primer alumne té 2 positius, el segon, 5 i el tercer, 7, ¿quantes pàgines ha de traduir cada un d'ells?
\stopexercici

\startexercici[reference=exercici:repartiments-proporcionals-9] Un padrí deixa als seus tres néts la seva herència, que és de 120.000 €, però decideix que qui tengui més poc capital al moment de la seva mort se n'endurà més doblers, i qui tengui més capital, se n'endurà més poc (per a així afavorir els més pobres).

En el moment de la seva mort, els capitals dels tres néts són de 1.000, 2.000 i 10.000 euros.

Què rebrà cada nét?
\stopexercici

\stopsubsubject

\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exercici:repartiments-proporcionals-2]} 1120€ i 2240€
\sym{\in[exercici:repartiments-proporcionals-3]} \startitemize[a,text] \item 378,78 € \item 1893,93 €\item 3787,87€ \item 18939,39 € \stopitemize
\stopitemize

\stopsubject

\stopsection
