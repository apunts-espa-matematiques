<!--
SPDX-FileCopyrightText: 2023 Xavier Bordoy

SPDX-License-Identifier: CC-BY-4.0
-->

# Exercicis procedimentals de Matemàtiques per a les classes d'ESPA
 
## Continguts

En aquest repositori trobareu un recull d'exercicis procedimentals per a ESPA (Educació Secundària de Persones Adultes) de les Illes Balears. L'objectiu d'aquest material és servir de resolsament a l'alumnat d'ESPA que vol practicar els procediments més usuals. Per tant, aquí no trobareu activitats riques.

Els exercicis estan basat en el currículum d'ESPA actual ([2019](http://www.caib.es/eboibfront/ca/2019/11074/628460/decret-85-2019-de-8-de-novembre-d-ordenacio-dels-e)), el qual és diferent del de l'ESO. Ara bé, es poden aprofitar alguns exercicis per a les classes d'aquest darrers estudis.

Podeu descarregar-vos la [darrera versió dels exercicis](ng-exercicis.pdf).

## Drets d'autor

L'autoria d'aquesta obra correspon a en Xavier Bordoy.

Els continguts d'aquest repositori estan subjectes a la llicència de [Reconeixement CompartirIgual de Creative Commons 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en) (CC-BY-SA 4.0), llevat que s'hi indiqui el contrari. Això vol dir, *essencialment*, que podeu copiar, modificar i redistribuir l'obra sempre que en citeu de manera adequada la font i si hi heu fet algun canvi (per exemple, la podríeu traduir a qualsevol idioma sense demanar-m'ho, només citant l'obra original). A més, en cas d'emprar algun fragment de l'obra o fer-ne una obra derivada, la vostra obra ha de mantenir la mateixa llicència.

## Retroacció

Si feis servir aquest manual i trobeu que es pot millorar alguna cosa (la qual cosa segur que es pot fer!), si us plau digueu-m'ho a través del correu electrònic, el qual trobareu al document PDF (no el faig públic aquí per evitar problemes d'*spam*). També podeu contribuir-hi amb nou material o amb modificacions, sempre d'acord amb la llicència de l'obra.

## Tasques i canvis recents

Podeu veure la llista de [tasques pendents](tasques/tasques.md) i de [tasques fetes](tasques/.taskbook/archive/archive.json). Per administrar les tasques utilitz l'eina [taskbook](https://github.com/klaussinani/taskbook). Amb aquesta eina les tasques són fàcilment accessibles, es desen en text pla i estan integrades al repositori. Em referiré a les tasques durant les trameses de git amb el seu codi: així, per exemple, `#24` correspondrà a la tasca vint-i-quatrena, és a dir, `Millorar la taula de cossos geomètrics: fer-la com la de figures planes`

També podeu veure la llista de [canvis més destacats](CANVIS.md). Si voleu veure els canvis permenoritzats, podeu consultar [la bitàcola del git del projecte](https://repo.or.cz/apunts-espa-matematiques.git/shortlog).

## Versions

De forma general, seguiré l'esquema de versions recollit als documents ["versions semàntiques per a documents"](https://semverdoc.org/) i ["versió semàntiques per a programari"](https://semver.org/): `Versió.Subversio.revisió`: de forma habitual, els canvis a la versió corresponen a canvis abruptes, els de la subversió a millores i els de la revisió a petites correccions. Podeu consultar la versió actual al fitxer ["VERSIO.txt"](VERSIO.txt). Al document pdf simplement s'afegeix com a sufix la data de compilació.

