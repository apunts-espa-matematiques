% Fitxer ConTeXt MIV

% carregam les opcions d'entorn
\environment ng-entorn-minimal.conTeXt
\environment ng-entorn-taules.conTeXt
\environment ng-entorn-colors.conTeXt

% bibliografia
\usebtxdataset[default][ng-bibliografia.bib]
\setupbtx[dataset=default]
\usebtxdefinitions[aps]

% el text

\starttext

\startfrontmatter
\input ng-pre-portada.conTeXt
\input ng-pre-llicencia.conTeXt
\completecontent
\stopfrontmatter

\startbodymatter
\startpart[title={Aritmètica}]
\input ng-aritmetica-nombres-naturals-operacions.conTeXt
\input ng-aritmetica-divisibilitat.conTeXt
\input ng-aritmetica-sencers-operacions.conTeXt
\input ng-aritmetica-fraccions-operacions.conTeXt
\input ng-aritmetica-fraccions-simplificacio-i-etc.conTeXt
\input ng-aritmetica-percentatges.conTeXt
\input ng-aritmetica-problemes-nombres-fraccions.conTeXt
\input ng-aritmetica-arrodoniment-i-truncament.conTeXt
\input ng-aritmetica-representacio-sobre-la-recta-numerica.conTeXt
\input ng-aritmetica-sequencies.conTeXt
\input ng-aritmetica-potencies-exponent-sencer-calcul.conTeXt
\input ng-aritmetica-propietats-de-les-potencies.conTeXt
\input ng-aritmetica-notacio-cientifica-calcul.conTeXt
\input ng-aritmetica-notacio-cientifica-problemes.conTeXt
\stoppart

\startpart[title={Relacions entre variables}]
\input ng-relacions-variables-proporcionalitat-directa.conTeXt
\input ng-relacions-variables-percentatges-problemes.conTeXt
\input ng-relacions-variables-repartiments-proporcionals.conTeXt
\input ng-relacions-variables-proporcionalitat-inversa.conTeXt
\input ng-relacions-variables-conversio-unitats.conTeXt
\input ng-relacions-variables-funcions-elements-de-un-grafic.conTeXt
\input ng-relacions-variables-funcions-representacio-introduccio.conTeXt
\input ng-relacions-variables-funcio-afi-representacio.conTeXt
\input ng-relacions-variables-funcio-quadratica-representacio.conTeXt
\input ng-relacions-variables-funcio-lineal-problemes.conTeXt
\input ng-relacions-variables-funcio-afi-problemes.conTeXt
\input ng-relacions-variables-funcio-quadratica-problemes.conTeXt
\input ng-relacions-variables-funcio-exponencial-problemes.conTeXt
\stoppart

\startpart[title={Àlgebra}]
\input ng-algebra-equacions-primer-grau.conTeXt
\input ng-algebra-equacions-segon-grau.conTeXt
\input ng-algebra-equacions-segon-grau-problemes-geometrics.conTeXt
\input ng-algebra-sistemes-de-equacions-lineals-2-per-2.conTeXt
\input ng-algebra-sistemes-de-equacions-lineals-2-per-2-problemes-comparativa.conTeXt
\input ng-algebra-sistemes-de-equacions-lineals-2-per-2-problemes-mescles.conTeXt
\stoppart

\startpart[title={Geometria}]
\input ng-geometria-teorema-de-Pitagores.conTeXt
\input ng-geometria-teorema-de-Pitagores-problemes.conTeXt
\input ng-proporcionalitat-geometrica-escala-grafica.conTeXt
\input ng-proporcionalitat-geometrica-escala-numerica.conTeXt
\input ng-proporcionalitat-geometrica-figures-semblants.conTeXt
\input ng-proporcionalitat-geometrica-homotecies.conTeXt
\input ng-proporcionalitat-geometrica-teorema-de-tales.conTeXt
\input ng-geometria-pla-cartesia.conTeXt
\stoppart

\startpart[title={Estadística}]
\input ng-estadistica-tipus-de-variables.conTeXt
\input ng-estadistica.conTeXt
\input ng-estadistica-parametres-dispersio.conTeXt
\input ng-estadistica-grafics-i-diagrames.conTeXt
\stoppart

\startpart[title={Probabilitat}]
\input ng-probabilitat-experiments-simples.conTeXt
\input ng-probabilitat-experiments-compostos.conTeXt
\input ng-probabilitat-probabilitat-condicionada.conTeXt
\stoppart

\stopbodymatter


%\startappendices
%
%\stopappendices

\startbackmatter
\input ng-pre-continguts-aliens.conTeXt
% Bibliografia
\startchapter[title=Referències]
\placelistofpublications
\stopchapter
%% Índex alfabètic
\completeindex
\stopbackmatter

\stoptext
