\startsection[reference=seccio:resolcio-equacions-2n-grau, title={Equacions de segon grau}]

\startsubject[title={Preguntes}]

\startexercici[reference=exer:eq-segon-grau-senzilles, title={senzilles}] Resoleu les equacions de segon grau següents:
\startitemize[a,columns]
\item $8x^2 - 2 = 10x^2 -5x$
\item $9x - 8 = 7 -x^2$
\item $10x - 8x = x^2 -5$
\item $3x^2 + 2x = 5x -2$
\item $3x - 5x^2 = 2x - 490$
\item $-9x^2 + 81x -4 = -4$ 
\item $- 20x = 8x^2-2+2x^2+2$ 
\item $4x^2 + 2x - 4 = -2x +4$
\item $9x^2 - 63 x + 90 = 0$
\item $2x^2+2x-4 = 0$
\stopitemize
\stopexercici

\startexercici[reference=exer:eq-segon-grau-senzilles-2, title={senzilles}] Resoleu les equacions de segon grau següents:
\startitemize[a,columns]
\item $x^2-6x+8 = 0$
\item $-x^2+2x+8 = 0$
\item $-x^2-4x-6 = 0$
\item $15x^2 + 2x - 8 = 0$
\item $2x^2 - 5x + 2 = 0$
\item $2x^2 - 5x - 7 = 0$
\item $3x^2 - 5x + 4 = 0$
\item $9x^2 + 6x + 1 = 0$
\item $3x^2 - 6x + 2 = 0$
\item $x^2 + x = 3x - x^2$
\stopitemize
\stopexercici


\startexercici[reference=exer:eq-segon-grau-senzilles-3, title={senzilles i llargues}] Resoleu les equacions de segon grau següents:
\startitemize[a,columns]
\item $-x^2 - 3x + 10 = x^2+3x-10$
\item $-2x^2 + 4x -3 = -2x + x^2$
\item $2x^2 + 4x +1 = -1$
\item $2x + 1 = -2 -x^2$
\item $-2x^2 +x + 23 = -4x^2 -11x +7$
\item $2x+4x-x^2 + 7 = x^2 +2x -9$
\item $-x+4-x^2 -1+x= -2x^2 +19$
\item $-x^2 -3x = x^2+2x +3x +6$
\item $2x^2 +2= -x^2 +2 +6x -x^2$
\item $-x^2 -3x -x^2 +2= 2x^2-14-3x$
\stopitemize
\stopexercici

\startexercici[reference=exer:eq-segon-grau-incompletes, title={incompletes}] Resoleu les equacions de segon grau següents:
\startitemize[a,columns]
\item $9x^2 - 225 = 0$
\item $3x^2 + 2x = -2x$ 
\item $5x^2 = 10$
\item $2x^2+6x = 0$
\item $-3x^2 +432=0$
\item $4x = 3x^2$
\item $3x^2 - x = -5x^2 +x$ 
\item $10 = -6x^2$
\item $6x^2 + 27 = 9x^2$
\item $3x^2-27 = 0$
\item $x^2 - 6x = 30$
\item $3x^2 - 115 = 185$
\item $x^2 = 121x$
\item $5x^2 - 7x=0$
\item $5x^2 - 2x = 5x -7x$
\item $3x^2 - 5 = 2x + x^2$
\stopitemize
\stopexercici

\startexercici[reference=exer:eq-segon-grau-parentesis, title={parèntesis}] Resoleu les equacions de segon grau següents:
\startitemize[a, columns]
\item $7-(x-3) = x^2 -4x$
\item $4(x-2) + 5 (x^2 -1) = -13$
\item $2(x^2 +x) - (4x^2 -5) = 3x^2+5$
\item $x-(x^2 + 2) = 3(x-x^2) -2$
\item $10(x-2) +5 = 5 - (4-x^2)$
\item $3(x^2 -x) + x^2 = 3x$
\item $5-(x^2 + 2) +2 = x +5$
\item $2x - (x-x^2) = 5 -4(x-x^2)$
\stopitemize
\stopexercici

\startexercici[reference=exer:eq-segon-grau-fraccions, title={fraccions simples}] Resoleu les equacions de segon grau següents:
\startitemize[a,columns]
\item \startformula \frac{x}{2} - \frac{3x^2}{4} = 5x \stopformula
\item \startformula x^2 - \frac{5x}{4} = 3x^2 - \frac{5}{3} \stopformula
\item \startformula 10x^2 - \frac{4x^2}{5} + \frac{x}{4} = 0 \stopformula
\item \startformula \frac{x}{2} - \frac{x^2}{10} = 0 \stopformula
\item \startformula 5 = \frac{x^2}{2} - \frac{2x}{3} + \frac{1}{4} \stopformula
\item \startformula x - \frac{x^2}{2} = x^2 - 3 \stopformula
\item \startformula \frac{3 x^2}{2} + \frac{13x}{4} = - \frac{1}{2} \stopformula
\item \startformula \frac{2}{3} + \frac{3x^2}{10} - 10= 5 \stopformula
\stopitemize
\stopexercici

\startexercici[reference=exer:eq-segon-grau-fraccions-compostes, title={fraccions compostes}] Resoleu les equacions de segon grau següents:
\startitemize[a, columns]
\item \startformula \frac{3x-1}{2} + \frac{x^2}{3} + 5= x^2 +3 \stopformula
\item \startformula \frac{x^2+1}{3} - \frac{x-2}{2} = 0 \stopformula
\item \startformula \frac{x}{5} + 2x = 2 -\frac{x^2-2}{2} \stopformula
\item \startformula 5x - \frac{x^2+1}{3} = 10 - \frac{4x-5}{6} \stopformula
\item \startformula 10 - \frac{2}{3} + 5x^2 =  \frac{1-x}{5} \stopformula
\item \startformula \frac{x-5}{2} +4x^2 = \frac{5x-1}{2} \stopformula
\stopitemize
\stopexercici


\startexercici[reference=exer:eq-segon-grau-multiplicar-incognites, title={multiplicació d'expressions amb incògnites}] Resoleu les equacions de segon grau següents:
\startitemize[a]
\startcolumns[n=2]
\item $(x-2)^2 -5 = 10$
\item $3(x+4)^2 = 10$
\item $(x-2)^2 - 8 = 20x$
\item $5(x-1)^2 = 2$
\item $(x-1)^2 = -4$
\item $(x-5)^2 = 5x^2$
\item $(3x-1)^2 = 0$
\item $(x-3)(x-8)=0$
\item $(2x-1)^2 = 25$
\item $(x-5)^2 = 0$
\item $(x-2)(x+2) = 7$
\item $(x+2)^2 + 3x -5 = 0$
\stopcolumns
\item $(x+2)(x-3) + 3x = (2x-4)(x+2)$
\item $3(x+2)+4x^2-4 = 8 -x(1-x) + 7x$
\stopitemize
\stopexercici

\startexercici[reference=exer:eq-segon-grau-mesclats-repas, title={mesclats}] Reduïu i resoleu les equacions següents:
\startitemize[a, columns]
\item \startformula 2x(x+1) - 2(x+2) = 0 \stopformula
\item \startformula 7x + 3 + 5x^2 = -3x^2 + 7x + 35 \stopformula
\item \startformula \frac{x}{3} - \frac{x^2}{5} = 2(x+1) \stopformula
\item \startformula \frac{(x-1)(x+1)}{3} = \frac{(x-1)^2}{2} \stopformula
\column
\item \startformula 4x - (2x^2 -5x) = \frac{x}{2} \stopformula
\item \startformula \frac{x}{2} + 3 (x^2+2) = 311 \stopformula
\item \startformula \frac{x}{3} - 5 (1+x^2) = 311 \stopformula
\item \startformula 5x^2 + 2 (x^2 + x) -4 = \frac{5x^2}{2} + 11x^2 \stopformula
\stopitemize
\stopexercici

\stopsubject

\page[yes]
\startsubject[title={Solucions}]

\startitemize[1][distance=0.5cm]
\sym{\in[exer:eq-segon-grau-senzilles]} \startitemize[a,text] \item $x=2, 1/2$ \item $x \simeq -10,437; 1,437$ \item $x \simeq -1,4495; 3,4495$ \item No té solució \item $x=10, -49/5$ \item $x=0, 9$ \item $x=0, 2$ \item $x=-2, 1$ \item $x=2, 5$ \item $x=1, -2$ \stopitemize
\sym{\in[exer:eq-segon-grau-senzilles-2]} \startitemize[a, text] \item $x=2, 4$ \item $x=-2, 4$, \item No té solució \item $x = 2/3, -4/5$ \item $x=1/2, 2$ \item $x=-1, 7/2$ \item No té solució \item $x=-1/3$ \item $x\simeq 0,42; 1,57$ \item $x=0, 1$ \stopitemize
\sym{\in[exer:eq-segon-grau-senzilles-3]} \startitemize[a,text] \item $x=-5, 2$ \item $x=1$ \item No té solució \item $x=-2, -4$ \item $x=-2, 4$ \item $x=-4, 4$ \item $x=-1, -3$ \item $x = 0, 3/2$ \item $x=2, -2$ \stopitemize
\sym{\in[exer:eq-segon-grau-incompletes]} \startitemize[a, text] \item $x=5, -5$ \item $x=0, -4/3$ \item $x\simeq \pm 1,41$ \item $x=0, 3$ \item $x = \pm 12$ \item $x=0, 4/3$ \item $x=0, 1/4$ \item No té solució \item $x=\pm 3$ \item $x=\pm 3$ \item $x\simeq -3,24; 9,24$ \item $x=\pm 10$ \item $x=0, 121$ \item $x=0, 7/5$ \item $x=0$ \item $x=0, 3/2$ \stopitemize
\sym{\in[exer:eq-segon-grau-parentesis]} \startitemize[a, text] \item $x=-2, 5$\item $x=0, -4/5$ \item $x=0, 1$ \item $x=2, 8$ \item $x=-1, 0$ \item No té solució \item No té solució \stopitemize
\sym{\in[exer:eq-segon-grau-fraccions]} \startitemize[a, text] \item $x=-6, 0$ \item $x \simeq -1,277; 0,652$ \item $x=0, x\simeq -0,027$ \item $x=0, 5$ \item $x \simeq -2,4868; 3,8201$ \item $x \simeq -1,1196; 1,7863$ \item $x = -2, -1/6$ \item $x \simeq \pm 6,91$ \stopitemize
\sym{\in[exer:eq-segon-grau-fraccions-compostes]} \startitemize[a, text] \item $x = -3/4, 3$ \item No té solució \item $x \simeq -5,49; 1,09$ \item $x \simeq 2,27; 14,72$ \item No té solució \item $x=-1/2, 1$\stopitemize
\sym{\in[exer:eq-segon-grau-multiplicar-incognites]} \startitemize[a, text] \item $x\simeq -1,87; 5,87$ \item $x\simeq -5,82; -2,17$ \item $x \simeq -0,16; 24,16$ \item $x \simeq 0,36; 1,63$ \item No té solució \item $x= 1/3$ \item $x=3, 8$ \item $x=-2, 3$ \item $x=5$ \item $x=-2, 2$ \item $x=-7,14; 0,14$ \item $x\simeq -0,732; 2,732$ \item $x=-1, 2$ \stopitemize
\sym{\in[exer:eq-segon-grau-mesclats-repas]} \startitemize[a, text] \item $x \simeq \pm 1,4142$ \item $x=-2, 2$ \item $x\simeq -6,87; -1,45$ \item $x=1, 5$ \item $x= 0, 17/4$ \item $x=-61/6; 10$ \item No té solució \item No té solució \stopitemize

\stopitemize

\stopsubject

\stopsection

