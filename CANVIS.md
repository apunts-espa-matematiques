<!--
SPDX-FileCopyrightText: 2023 Xavier Bordoy

SPDX-License-Identifier: CC-BY-4.0
-->

# Llista de canvis

## 0.5 [encara no alliberada]

- canvi de font (de modern, 12pt a kpfonts, 10pt)
- incorporat el fitxer VERSIO.txt en el que pos la versió actual. D'aquesta manera no hauré de canviar la versió a mà al document ConTeXt sinó només a aquest document. La versió segueix les recomonanacions de [semver](https://semver.org/)
- canvi de llicència: de CC-BY 4.0 a CC-BY-SA 4.0
- De DIN A4 a US Executive (7x11 polsades)

## 0.4 [2024-03-24]

- probabilitat: re-redacció completa d'exercicis d'esdeveniments compostos i purga dels de simples
- pas de GNU Make a Ninja com a sistema de dependències
- llevada la teoria: el manual és d'exercicis, no de teoria.
- incorporació de continguts antics existents (probabilitat, proporcionalitat geomètrica, equacions de segon grau, pla cartesià, representació de funcions afins, de funcions quadràtiques i introducció a la representació de funcions)
- nou contingut: problemes de mescles
- unificació de marges: ja no hi ha canvis dels marges del paper en els temes de proporcionalitat geomètrica.
- supressió de modes: professor, grisos, espa4, espa4a, etc. Tenc un sol document.
- posades algunes solucions a les seccions corresponents
- correcció d'errors
- correcció d'alguns problemes d'alineament (gràcies Wolfgang Schuster)
- ConTeXt:
    - compilació del document usant la versió LMTX en comptes de MKIV
    - ús del mòdul de context `unit` en comptes de `units`
    - ús de sintaxi moderna (startsection ... stopsection entre d'altres)
    - correcció d'errors de sintaxi

## 0.3 [2023-04-04]

- refets els exercicis d'estadística
- correcció d'errors
- incorporació de nous exercicis
- integració de la teoria al llibret d'exercicis com a apèndixos
- incorporació dels exercicis de versions anteriors

## 0.2.1 [2022-10-01]

- aclariment de llicències de continguts aliens
- incorporació de solucions (seqüències, nombres sencers, problemes de fraccions i fraccions)
- correccions de solucions
- posats alguns problemes més

## 0.2 [2021-09-27]

- llevats tots els modes: tendré un sol document per tots els nivells d'ESPA
- basat només en el currículum nou (2019)
- el temari es restringeix al cursos d'ESPA 1 i ESPA 3 (#50, #53)
- revisats els exercicis: me qued amb menys pocs #8 #42 i més atractius i visuals
- separada la teoria dels exercicis en un documents a part
- entorn del ConTeXt: separ a un fitxer a part l'entorn de les taules
- entorn del ConTeXt: simplificat
- llevats els colors de les seccions, subseccions, etc.
- llevat el peu de pàgina i simplement pos el nombre de pàgina
- canviada la portada per una de més xula
- canvi progressiu dels fitxers d'imatges per gràfics vectorials (usant TiKZ) #54
- s'usa \part per a separar els grans blocs. S'embelleixen d'acord amb algunes referències [https://www.mail-archive.com/ntg-context@ntg.nl/msg97219.html] [https://tex.stackexchange.com/q/69486/61233] 
- els exercicis i exemples estan enumerats amb la secció corresponent
- no es reinicia el comptador de les seccions després de cada part ni capítol [https://tex.stackexchange.com/questions/610888/not-restart-section-numbering-after-each-part-in-context]
- cada secció té com a subseccions: teoria, preguntes i solucions
- afegeix el fitxer CANVIS.md amb els canvis principals del projecte

## 0.1.1 [2021-05-14]

- corregit el Mathematical Subject Classification (ara és del 2020)

## 0.1 [2021-05-08]

- afegeix el fitxer README.md del projecte amb els continguts principals
- solució modular: un document per a cada secció

## versió inicial [2021-04-22]
			
- versió inicial
