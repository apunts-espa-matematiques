\startsection[title={Elements d'un gràfic}]

\startsubject[title={Preguntes}]

\startexercici[reference=exer:elements-grafic-1] Calculeu \startitemize[a, text] \item el domini de definició, \item els intervals de creixement i decreixement, \item els màxims i mínims, \item punts de talls amb els eixos, \item continuïtat i \item simetries  \stopitemize dels gràfics següents:


\startplacetable[location={split,force,none,width=fit}, reference=taula:combinations2, title={Relacions: funcions o no}]
% de matplotlib
	\bTABLE[frame=off,align={middle, lohi},split=yes]
	\bTR
	\bTD
	a
	
	{ \starttikzpicture[scale=0.4]
	    \draw[very thin,color=gray] (-3,-3) grid (3,4);
	    \draw[->] (-0.2,0) -- (4.2,0) node[right] {$x$};
	    \draw[->] (0,-0.2) -- (0,3.2) node[above] {$y$};

	\draw[domain=-2:2, variable=\x, color=blue, ultra thick] plot({\x}, {floor(\x)+1});
	\stoptikzpicture}
	\eTD
	\bTD
	b
	
	{ \starttikzpicture[scale=0.4]
	    \draw[very thin,color=gray] (-3,0) grid (4,3);
	    \draw[->] (-2.2,0) -- (4.2,0) node[right] {$x$};
	    \draw[->] (0,-0.2) -- (0,3.2) node[above] {$y$};

	\draw[ultra thick] (-2,0) -- (0,2) -- (2,0) --(4,2);
	\stoptikzpicture}
	\eTD
	\bTD
	c
	
	{ \starttikzpicture[scale=0.6]
	    \draw[very thin,color=gray] (-4,-2) grid (4,2);
	    \draw[->] (-4.2,0) -- (4.2,0) node[right] {$x$};
	    \draw[->] (0,-1.2) -- (0,1.2) node[above] {$y$};

	\draw[domain=-4:4, variable=\x, color=orange, ultra thick] plot({\x}, {sin(abs(\x) r)});
	\stoptikzpicture}
	\eTD
	\eTR
	\bTR
	\bTD
	d
	
	{ \starttikzpicture[scale=0.6]
	    \draw[very thin,color=gray] (-4,-1) grid (4,1);
	    \draw[->] (-4.2,0) -- (4.2,0) node[right] {$x$};
	    \draw[->] (0,-1.2) -- (0,1.2) node[above] {$y$};

	\draw[domain=-4:4, variable=\x, color=orange, ultra thick] plot({\x}, {sin(\x r)});
	\stoptikzpicture}
	\eTD
	\bTD
	e
	
	{ \starttikzpicture[scale=0.4]
	    \draw[very thin,color=gray] (-2,0) grid (2,9);
	    \draw[->] (-0.2,0) -- (4.2,0) node[right] {$x$};
	    \draw[->] (0,-0.2) -- (0,3.2) node[above] {$y$};

	\draw[domain=-2:2, variable=\x, color=blue, ultra thick] plot({\x}, {exp(\x)});
	\stoptikzpicture}
	\eTD
	\bTD
	f
	
	{ \starttikzpicture[scale=0.7]
	    \draw[very thin,color=gray] (-2,0) grid (3,4);
	    \draw[->] (-2.2,0) -- (3.2,0) node[right] {$x$};
	    \draw[->] (0,-0.2) -- (0,3.2) node[above] {$y$};

	\draw[color=green!70,ultra thick] (-1,0) parabola[bend pos=0.5] bend +(0,2) + (3,0);
	\stoptikzpicture}
	\eTD
	\eTR
	\bTR
	\bTD
	g
	
	{ \starttikzpicture[scale=0.6]
	    \draw[very thin,color=gray] (-4,-1) grid (4,4);
	    \draw[->] (-4.1,0) -- (4.1,0) node[right] {$x$};
	    \draw[->] (0,-0.2) -- (0,1.2) node[above] {$y$};

	\draw[domain=-4:4, variable=\x, color=red!70, very thick,smooth] plot({\x}, {abs(cos(\x r)) -1});
	\stoptikzpicture}
	\eTD
	\bTD
	h
	
	{ \starttikzpicture[scale=1]
	    \draw[very thin,color=gray] (-1,-1) grid (1,1);
	    \draw[->] (-1.2,0) -- (1.2,0) node[right] {$x$};
	    \draw[->] (0,-1.2) -- (0,1.0) node[above] {$y$};

	\draw[domain=-1:1, variable=\x, color=red!70, very thick] plot({\x}, {- \x/2});
	\stoptikzpicture}
	\eTD
	\bTD
	i
	
	{\starttikzpicture[scale=0.5]
	    \draw[very thin,color=gray] (-3,0) grid (3,3);
	    \draw[->] (-4,0) -- (4,0) node[right] {$x$};
	    \draw[->] (0,-0.2) -- (0,4.0) node[above] {$y$};

	\draw[ultra thick] (-3,1) -- (-2,1);
	\draw[ultra thick] (-2,2) -- (-1,2);
	\draw[ultra thick] (-1,3) -- (1,3);
	\draw[ultra thick] (2,2) -- (3,1);
	\stoptikzpicture}
	\eTD
	\eTR
	\bTR
	\bTD
	j
	
	\externalfigure[figs/funcions-elements-grafic-1.pdf][scale=400]
	\eTD
	\bTD
	k
	
	\externalfigure[figs/funcions-elements-grafic-2.pdf][scale=400] 
	\eTD
	\bTD
	l
	
	\externalfigure[figs/funcions-elements-grafic-3.pdf][scale=400]
	\eTD
	\eTR
	\bTR
	\bTD
	m
	
	\starttikzpicture[scale=0.6]
	    \draw[very thin,color=gray] (-0.1,-0.1) grid (6.2,5);
	    \draw[->] (-0.2,0) -- (6.2,0);
	    \draw (3,-1) node {$\text{Edat d'una ovella (anys)}$};
	    \draw[->] (0,-0.2) -- (0,5.2) node[above] {$\text{Alçada (cm)}$};

	\foreach \x in {1,...,6}
	{
	\draw (\x, -0.1 cm) -- (\x, 0.1 cm);
	\draw (\x, -0.3 cm) node {$\x$};
	}

	\draw (-0.1 cm,1) -- (0.1 cm,1);
	\draw (0,1) node[anchor=east] {$20$};

	\draw (-0.1 cm,2) -- (0.1 cm,2);
	\draw (0,2) node[anchor=east] {$40$};

	\draw (-0.1 cm,3) -- (0.1 cm,3);
	\draw (0,3) node[anchor=east] {$60$};

	\draw (-0.1 cm,4) -- (0.1 cm,4);
	\draw (0,4) node[anchor=east] {$80$};

	\draw (-0.1 cm,5) -- (0.1 cm,5);
	\draw (0,5) node[anchor=east] {$100$};


	\draw[domain=0:6, variable=\x, color=blue, ultra thick] plot({\x}, {2+ sqrt(\x)});

	\stoptikzpicture
	\eTD
	\bTD
	n
	
	\starttikzpicture[scale=0.6]
	    \draw[very thin,color=gray] (-4,0) grid (4,2);
	    \draw[->] (-4.2,0) -- (4.2,0) node[right] {$x$};
	    \draw[->] (0,-0.7) -- (0,1.7) node[above] {$y$};

	\draw[domain=-4:0, variable=\x, color=orange, ultra thick] plot({\x}, {0.5+ sin(\x r)});
	\draw[domain=1:4, variable=\x, color=orange, ultra thick] plot({\x}, {0.5});
	\stoptikzpicture
	\eTD
	\bTD
	o
	
	\starttikzpicture[scale=0.6]
	    \draw[very thin,color=gray] (-0.1,-0.1) grid (6.2,5);
	    \draw[->] (-0.2,0) -- (6.2,0);
	    \draw[->] (0,-0.2) -- (0,5.2);
	    
	\foreach \x in {1,...,6}
	{
	\draw (\x, -0.1 cm) -- (\x, 0.1 cm);
	\draw (\x, -0.3 cm) node {$\x$};
	}

	\draw (-0.1 cm,1) -- (0.1 cm,1);
	\draw (0,1) node[anchor=east] {$20$};

	\draw (-0.1 cm,2) -- (0.1 cm,2);
	\draw (0,2) node[anchor=east] {$40$};

	\draw (-0.1 cm,3) -- (0.1 cm,3);
	\draw (0,3) node[anchor=east] {$60$};

	\draw (-0.1 cm,4) -- (0.1 cm,4);
	\draw (0,4) node[anchor=east] {$80$};

	\draw (-0.1 cm,5) -- (0.1 cm,5);
	\draw (0,5) node[anchor=east] {$100$};


	\draw[domain=-0.2:6, variable=\x, color=blue, ultra thick] plot({\x}, {5- \x});

	\stoptikzpicture
	\eTD
	\eTR
	\eTABLE
\stopplacetable

\stopexercici

\stopsubject

\page[yes]
\startsubject[title={Solucions}]
\stopsubject

\stopsection